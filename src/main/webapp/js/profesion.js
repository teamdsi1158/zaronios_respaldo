/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */ /*autor carlos mata*/

'use strict';

angular.module('ciApp').factory('profesionFactory', ['$q', '$http', function ($q, $http) {
    return{
        listarProfesion: listarProfesion,
        listarProfesionPaciente: listarProfesionPaciente,
        guardarProfesion: guardarProfesion,
        obtenerProfesion: obtenerProfesion,
        eliminarProfesion: eliminarProfesion
    }; 
    
     function listarProfesion() {
            var deferred = $q.defer();
            $http.get('rest/profesion/profesionsvc/list/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las profesiones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
    function listarProfesionPaciente() {
            var deferred = $q.defer();
            $http.get('rest/profesion/profesionsvc/statistics/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las estadisticas de las profesiones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function guardarProfesion(profesion) {
            
            var deferred = $q.defer();
            $http.post('rest/profesion/profesionsvc/save/', profesion).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba la profesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerProfesion(id) {
            var deferred = $q.defer();
            $http.get('rest/profesion/profesionsvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la profesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function eliminarProfesion(id) {
            var deferred = $q.defer();
            $http.delete('rest/profesion/profesionsvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba la profesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

}]);


angular.module('ciApp').controller('profesionController', ['$scope','profesionFactory','$location','$routeParams','$filter','paginacion','mensaje', function ($scope,profesionFactory,$location,$routeParams,$filter,paginacion,mensaje) {
        $scope.iconReverse="up";
        $scope.order='profId';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        $scope.listaProfesion = [];
       //para mostrar guardado o edicion
        $scope.editar=false;
       
        
       
         $scope.profesion = {
            "profNombre": null
            
        };
         $scope.getData = function () {
         return $filter('filter')($scope.listaProfesion, $scope.searchString);
         };
         
           if($location.path()==='/editProfesion/'+$routeParams.profId){
            $scope.editar=true;
           
            obtenerProfesion($routeParams.profId);
        }else{
            $scope.editar=false;
        }
        //Listar las profesiones disponibles
        $scope.listarProfesion=function() {
            profesionFactory.listarProfesion().then(function (lista) {
                
                $scope.listaProfesion= lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        }
        
        $scope.listarProfesionPacientes=function() {
            
            profesionFactory.listarProfesionPaciente().then(function (lista) {
                
                $scope.listaProfesionEstadistica = lista;
                  
            }).catch(function (error) {
                console.error(error);
            });
        };
        //Guardar la profesion resien creada
         $scope.guardarProfesion = function () {
            $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            
            profesionFactory.guardarProfesion($scope.profesion).then(function (msj) {
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                if($location.path()=='/addPaciente'|| $location.path()=='/editPaciente/'+$routeParams.pacId){
                   $('#modalIngProfesion').closeModal();
                    $('#preload1').removeClass("active");
                    $('#preload2').removeClass("active");
                   $scope.findAllProfesion();
               }else{
                 $location.path('/atributos');  
               }
                //Materialize.toast('' + msj, 10000);
                 mensaje.resultado("Profesión",msj);
               
                
            });
        };
        
         $scope.eliminarProfesion = function (id) {
            swal({
                title: "\u00BF Desea eliminar esta profesion?",
                text: "\u00A1 Este registro sera Eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo eliminar!",
                closeOnConfirm: false
                },
                function(){
                profesionFactory.eliminarProfesion(id).then(function (msj) {
                   mensaje.resultado(" Profesion",msj);
                    listarProfesion();
                }).catch(function (error) {
                    console.error(error);
                });
                
                    });
        };
        
         // Obtiene la profesion para poder editarlo
        function obtenerProfesion(id) {
            profesionFactory.obtenerProfesion(id).then(function (data) {
                
                $scope.profesion = data; 
               }).catch(function (error) {
                console.error(error);
            });
        };
        
        $scope.mostrarGrafico=function(b){
            $scope.grafico=b;
            $scope.label="N de personas por profesion";
             var tabla=ordenarTabla();
             
        // Get the context of the canvas element we want to select
        var countries= document.getElementById("profesiones").getContext("2d");
      $scope.myChart = new Chart(countries, {
    type: $scope.tipoGrafico,
    data: {
        datasets: [{
            label: $scope.label,
            data: tabla.data,
            backgroundColor: tabla.rgbabackgroundColor,
            //borderColor: tabla.rgbaBorderColor,
            borderWidth: 1
        }],
    labels: tabla.label
    },
    options: {
            responsive: true,
             legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        },
         title: {
            display: true,
            text: 'Generos'
        }
       
        }
});
        };
        
        function ordenarTabla(){
            var tabla ={
                data: [],
                label: [],
                rgbabackgroundColor: [],
                rgbaBorderColor: []
            };
            for(var i=0;i<$scope.listaProfesionEstadistica.length;i++){
                tabla.label[i]=($scope.listaProfesionEstadistica[i])[1];
                tabla.data[i]=($scope.listaProfesionEstadistica[i])[2];
                tabla.rgbabackgroundColor[i]=generatorRgba();
                tabla.rgbaBorderColor[i]=generatorRgba();
            }
            return tabla;
        }
        
         function generatorRgba(){
            var valor="rgba(";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=0.5+")";
            return valor;
        }
        
        $scope.mostrarProfesion = function(b){
            $scope.mostrarProfesion=b;
        };
        
        $scope.cambiarGrafico=function(){
        if($scope.tipoGrafico==="bar"){
           $scope.tipoGrafico="pie"; 
        }else{
            $scope.tipoGrafico="bar";
        }
        
         delete $scope.myChart;
         $scope.mostrarGrafico(false);
        
        $("#profesiones").show();
        };
    
    }]);
