'use strict';



angular.module('ciApp').service('paginacion',['$q',function($q){
}
]).controller('paginacionController',['$scope','paginacion','$filter',function($scope,paginacion,$filter){
              //pagina a mostrar
        $scope.currentPage = 0;
    $scope.getNumber=function(num){  //Sirve para Mostrar iterar la vista de la paginacion
        return new Array(num);
        };
        //para saltarse de pagina 
         $scope.saltarPagina=function(valor){  
            $scope.currentPage=valor-1;
            $('li').removeClass("active").addClass("waves-effect");
            $('#pag'+valor).removeClass("waves-effect").addClass("active");
            
            $scope.checkchevron();  
        };
                //verifica los chevron de la pacinacion
        $scope.checkchevron=function(){
          
          if($scope.currentPage!=0){
                $('#chevron_left').removeClass("disabled").addClass("waves-effect");
            }else
            {
                $('#chevron_left').removeClass("waves-effect").addClass("disabled");
            }
            if($scope.currentPage >= $scope.getData().length/$scope.numItems - 1){
                $('#chevron_ringht').removeClass("waves-effect").addClass("disabled");
            }else
            {
                $('#chevron_ringht').removeClass("disabled").addClass("waves-effect");
            }    
        };      
         //accion del boton de avance en paginacion
         $scope.add=function(){
             if($scope.currentPage < $scope.getData().length/$scope.numItems - 1){
            $('li').removeClass("active").addClass("waves-effect");
            $('#pag'+($scope.currentPage+2)).removeClass("waves-effect").addClass("active");
             $scope.currentPage=$scope.currentPage+1;
              $scope.checkchevron(); 
         }
         };
         //accion del boton de retroceso de paginacion
         $scope.reset=function(){              
           if($scope.currentPage > 0){
               $('li').removeClass("active").addClass("waves-effect");
            $('#pag'+$scope.currentPage).removeClass("waves-effect").addClass("active");
               $scope.currentPage=$scope.currentPage-1 ;
                $scope.checkchevron(); 
           }  
         };
         
          $scope.orderBy=function(order){
          $scope.order=order;
          $scope.reverse=!$scope.reverse;
          if($scope.reverse){
              $scope.iconReverse="down";
          }else{
              $scope.iconReverse="up";
          }
        };
}]);