/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

angular.module('ciApp').factory('pagoFactory', ['$q', '$http','$rootScope', function ($q, $http,$rootScope) {
         return{
          listarPago: listarPago,
          obtenerPago: obtenerPago,
          guardarPago: guardarPago,
          eliminarPago: eliminarPago
        };
         function listarPago(asitId) {
            var deferred = $q.defer();
            $http.get('rest/pago/pagosvc/list/'+asitId+'/').then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los pagos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarPago(pago) {
            pago.usrNombre=$rootScope.user.name;
            pago.pagModificacion=new Date();
            var deferred = $q.defer();
            
            $http.post('rest/pago/pagosvc/save/', pago).then(function (response) {
                deferred.resolve(response.data);
            $('#modalConfirmacionPago').closeModal();
            }, function (errResponse) {
                console.error('Error mientras guardaba el pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function obtenerPago(id) {
            var deferred = $q.defer();
            $http.get('rest/pago/pagosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras cargaba el pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function eliminarPago(id) {
            var deferred = $q.defer();
            $http.delete('rest/pago/pagosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
}]);

angular.module('ciApp').controller('pagoController', ['$scope', 'pagoFactory','expedienteFactory','$location','$routeParams','mensaje', function ($scope, pagoFactory,expedienteFactory, $location,$routeParams,mensaje) {
         $scope.iconReverse="up";
        $scope.order='pagId';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.total=0;            //Total pagado por el tratamiento
       
        $scope.CurrentDate = new Date(); //Para capturar la fecha actual
        $scope.pago={tpagId: 1};
        $scope.openModalPago=function(){
          $('#modalConfirmacionPago').openModal();
        };

     
     function obtenerAsignacion(){
         expedienteFactory.obtenerAsignacion($routeParams.asitId).then(function(lista){
            
             $scope.asignacion=lista;
            calcularPagado();
         });
     }
     
      if($location.path()=='/verPago/'+$routeParams.pagId+'/'+$routeParams.asitId+'/'+$routeParams.pacId){
          obtenerPago($routeParams.pagId);
           obtenerAsignacion();
        obtenerPaciente();
      }else{
           obtenerAsignacion();
        obtenerPaciente();
      }
     
     function obtenerPaciente(){
         expedienteFactory.obtenerExpediente($routeParams.pacId).then(function(expediente){
             
            $scope.paciente=expediente; 
         });
        }
        
     function obtenerPago(id){
         pagoFactory.obtenerPago(id).then(function(pago){
             
            $scope.pago=pago; 
         });
        }
        
        $scope.guardarPago=function(){
          $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            $scope.pago.asitId=$scope.asignacion.asitId;
            $scope.pago.pagFecha=new Date();
            pagoFactory.guardarPago($scope.pago).then(function (msj) {
                $('#preload1').removeClass("active");
                $('#preload2').removeClass("active");
               // Materialize.toast('' + msj, 5000);

                 listarPago(); 
                 $scope.pago={tpagId: 1};
                mensaje.resultado("Pago",msj);
            });  
        };
     //Lista los pagos segun la asignacion del tratamiento
      function listarPago() {
            pagoFactory.listarPago($routeParams.asitId).then(function (lista) {
                
                $scope.asignacion.pagoTratamientoList = lista;
                 calcularPagado();
            }).catch(function (error) {
                console.error(error);
            });
        }
        
        
        function calcularPagado(){
            $scope.total=0;
            for(var i=0; i<$scope.asignacion.pagoTratamientoList.length;i++){
                $scope.total+=($scope.asignacion.pagoTratamientoList)[i].pagMonto;
            }
        }


    }]);


