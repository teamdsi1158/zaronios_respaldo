/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

angular.module('ciApp').service('mensaje',function(){
  this.resultado=function(entidad,tipo) {
      var title;
      var text;
      var type;
      switch(tipo){
          case "ACTUALIZADO":
          title=tipo;
          text="se he actualizado con exito el:"+entidad;
          type="success";
          break;
          
          case "GUARDADO":
          title=tipo;
          text="Se ha guardado con exito el :"+entidad;
          type="success";
          break;
          
          case "ACTIVADO":
          title=tipo;
          text="Se ha activador con exito el:"+entidad;
          type="success";
          break;
      
          case "ELIMINADO":
          title=tipo;
          text="Se ha eliminado con exito el :"+entidad;
          type="success";
          break;
          
          case "GENERADO":
          title=tipo;
          text="Se ha generado con exito el reporte de:"+entidad;
          type="success";
          break;
            
          case "DADO DE BAJA":
          title=tipo;
          text="Se ha dado de baja:"+entidad;
          type="success";
            break;
          default:
           title="Error";
           text="Ha ocurrido un error durante la accion";
           type="error";
      }
        
                swal({
                     title: title,
                     text: text,
                     type: type,
                     html: true 
                });
    };
    
});
