
angular.module('ciApp', ['ngRoute', 'ngCookies', 'ciApp.services', 'angularUtils.directives.dirPagination', 'wt.responsive', 'ngLoadingSpinner']).config(['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {

        $routeProvider.otherwise({
            templateUrl: 'partials/index.html',
            controller: IndexController
        });
       
//------------------------------------------------------------------------------------------------------
//MODULO: PACIENTE
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/pacientes', {
            templateUrl: 'partials/modulos/crud_paciente.html'
        });

        $routeProvider.when('/pacientesPagos', {
            templateUrl: 'partials/modulos/crud_pagos.html'
        });

        $routeProvider.when('/addPaciente', {
            templateUrl: 'partials/modulos/formularios/paciente.html'
        });

        $routeProvider.when('/editPaciente/:pacId', {
            templateUrl: 'partials/modulos/formularios/paciente.html'
        });


//------------------------------------------------------------------------------------------------------
//MODULO: EMPLEADO
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/empleados', {
            templateUrl: 'partials/modulos/crud_empleado.html'
        });

        $routeProvider.when('/addEmpleado', {
            templateUrl: 'partials/modulos/formularios/empleado.html'
        });

        $routeProvider.when('/editEmpleado/:empId', {
            templateUrl: 'partials/modulos/formularios/empleado.html'
        });

        $routeProvider.when('/expedienteEmpleado/:empId', {
            templateUrl: 'partials/modulos/formularios/expedienteEmpleado.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: TRATAMIENTO
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/tratamientos', {
            templateUrl: 'partials/modulos/crud_tratamiento.html'
        });

        $routeProvider.when('/addTratamiento', {
            templateUrl: 'partials/modulos/formularios/tratamiento.html'
        });

        $routeProvider.when('/editTratamiento/:tratId', {
            templateUrl: 'partials/modulos/formularios/tratamiento.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: TIPO DE PUBLICIDAD
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/publicidades', {
            templateUrl: 'partials/modulos/crud_publicidad.html'
        });

        $routeProvider.when('/addPublicidad', {
            templateUrl: 'partials/modulos/formularios/publicidad.html'
        });

        $routeProvider.when('/editPublicidad/:pubId', {
            templateUrl: 'partials/modulos/formularios/publicidad.html'
        });


//------------------------------------------------------------------------------------------------------
//MODULO: USUARIOS
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/usuarios', {
            templateUrl: 'partials/modulos/crud_usuarios.html'
        });

        $routeProvider.when('/addUsuario', {
            templateUrl: 'partials/modulos/formularios/usuario.html'
        });

        $routeProvider.when('/editUsuario/:usrId', {
            templateUrl: 'partials/modulos/formularios/usuario.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: ROL
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/roles', {
            templateUrl: 'partials/modulos/crud_rol.html'
        });

        $routeProvider.when('/addRol', {
            templateUrl: 'partials/modulos/formularios/rol.html'
        });

        $routeProvider.when('/editRol/:rol', {
            templateUrl: 'partials/modulos/formularios/rol.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: EXPEDIENTE
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/expediente/:pacId', {
            templateUrl: 'partials/modulos/crud_expediente.html'
        });

        $routeProvider.when('/pacientesExpedientes', {
            templateUrl: 'partials/modulos/pacientesExpedientes.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: PAGO
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/pagos/:asitId/:pacId', {
            templateUrl: 'partials/modulos/formularios/pago.html'
        });

        $routeProvider.when('/verPago/:pagId/:asitId/:pacId', {
            templateUrl: 'partials/modulos/formularios/pagoDetalle.html'
        });

        $routeProvider.when('/tratamientosPagos/:pacId', {
            templateUrl: 'partials/modulos/tratamientosPagos.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: TIPO PAGO
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addTipoPago', {
            templateUrl: 'partials/modulos/formularios/tipoPago.html'
        });

        $routeProvider.when('/editTipoPago/:tpagId', {
            templateUrl: 'partials/modulos/formularios/tipoPago.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: SESION
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addSesion/:asitId/:pacId', {
            templateUrl: 'partials/modulos/formularios/sesion.html'
        });

        $routeProvider.when('/editSesion/:asitId/:pacId/:sesiId', {
            templateUrl: 'partials/modulos/formularios/sesion.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: SEXO
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addSexo', {
            templateUrl: 'partials/modulos/formularios/sexo.html'
        });

        $routeProvider.when('/editSexo/:sexId', {
            templateUrl: 'partials/modulos/formularios/sexo.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: ESTADO CIVIL
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addEstadoCivil', {
            templateUrl: 'partials/modulos/formularios/estadoCivil.html'
        });

        $routeProvider.when('/editEstadoCivil/:estId', {
            templateUrl: 'partials/modulos/formularios/estadoCivil.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: PROFESION
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addProfesion', {
            templateUrl: 'partials/modulos/formularios/profesion.html'
        });

        $routeProvider.when('/editProfesion/:profId', {
            templateUrl: 'partials/modulos/formularios/profesion.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: TIPO DE SIGNO VITAL
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addTipoSignoVital', {
            templateUrl: 'partials/modulos/formularios/tipoSignoVital.html'
        });

        $routeProvider.when('/editTipoSignoVital/:dsivId', {
            templateUrl: 'partials/modulos/formularios/tipoSignoVital.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: ESTADO DE CITAS
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/addEstado', {
            templateUrl: 'partials/modulos/formularios/estado.html'
        });

        $routeProvider.when('/editEstado/:estId', {
            templateUrl: 'partials/modulos/formularios/estado.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: ACCESSTOKEN
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/sesiones', {
            templateUrl: 'partials/modulos/tablas/crud_accessToken.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: CONSULTAS
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/consultas', {
            templateUrl: 'partials/modulos/consulta.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: PAPELERA DE RECICLAJE DEL SISTEMA
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/configuracion', {
            templateUrl: 'partials/modulos/configuracionSistema.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: OTROS ATRIBUTOS DEL SISTEMA
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/atributos', {
            templateUrl: 'partials/modulos/crud_atributos.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: ESTADISTICAS DE EL SISTEMA
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/estadisticas', {
            templateUrl: 'partials/modulos/estadisticas.html'
        });

//------------------------------------------------------------------------------------------------------
//MODULO: BITACORAS
//------------------------------------------------------------------------------------------------------
        $routeProvider.when('/bitacoraPaciente/:pacId', {
            templateUrl: 'partials/modulos/tablas/tablaBitacoraPaciente.html'
        });

        $routeProvider.when('/bitacoraPago/:pagId', {
            templateUrl: 'partials/modulos/tablas/tablaBitacoraPagoTratamiento.html'
        });
        

        $locationProvider.hashPrefix('!');

        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
            return {'responseError': function (rejection) {
                    var status = rejection.status;
                    var config = rejection.config;
                    var method = config.method;
                    var url = config.url;
                    if (status === 401) {//=
                        window.location.href = "http://localhost:8080/zaronios/#!/login";
                    } else {
                        $rootScope.error = method + " on " + url + " failed with status " + status;
                    }
                    return $q.reject(rejection);
                }
            };
        });

        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
            return {
                'request': function (config) {
                    var isRestCall = true; //config.url.indexOf('rest') == 0;
                    if (isRestCall && angular.isDefined($rootScope.accessToken)) {
                        var accessToken = $rootScope.accessToken;
                        if (exampleAppConfig.useAccessTokenHeader) {
                            config.headers['X-Access-Token'] = accessToken;
                        } else {
                            config.url = config.url + "?token=" + accessToken;
                        }
                    }
                    return config || $q.when(config);
                }
            };
        });
    }
]).run(function ($rootScope, $location, $cookieStore, UserService) {
    $rootScope.permisos=[];
    $rootScope.days = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'];
    $rootScope.month = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];

    $rootScope.addDate = function (n) {
        $rootScope.fecha = new Date();
        $rootScope.fecha = $rootScope.fecha.setDate($rootScope.fecha.getDate() + n);
        return new Date($rootScope.fecha);
    };
    
    $rootScope.$on('$routeChangeStart',  function (event, next, current) {
        
        if($rootScope.permisos.find(esPath)==undefined && next.$$route.originalPath!="/index"){
            $location.path("/index");
            Materialize.toast('\u00A1 No tiene los permisos necesarios!', 4000);
        }
        
        function esPath(permiso) { 
    return permiso.perNombre === next.$$route.originalPath;
    }
       
    });

    $rootScope.$on('$viewContentLoaded', function () {
        delete $rootScope.error;
    });

    $rootScope.hasRole = function (role) {
        sessionStorage.setItem("user", $rootScope.user.name);
        if ($rootScope.user === undefined) {
            return false;
        }
        if ($rootScope.user.roles[role] === undefined) {
            return false;
        }
        return $rootScope.user.roles[role];
    };

    $rootScope.logout = function () {
        delete $rootScope.user;
        delete $rootScope.accessToken;
        $cookieStore.remove('accessToken');
        sessionStorage.removeItem("user");
        window.location.href = "/zaronios";
    };

    var originalPath = $location.path();

    $location.path("/index");

    var accessToken = $cookieStore.get('accessToken');

    if (accessToken !== undefined) {
        $rootScope.accessToken = accessToken;
        UserService.get(function (user) {
            $rootScope.user = user;
            sessionStorage.setItem("user", user.name);
            $location.path(originalPath);
        });
    } else {
        $rootScope.initialized = false;
        return false;
    }
    $rootScope.initialized = true;
});

function IndexController($scope, GroupService) {
    $scope.groups = GroupService.query();
    $scope.deletePost = function (group) {
        group.$remove(function () {
            $scope.groups = GroupService.query();
        });
    };
}

function LoginController($scope, $rootScope, $location, $cookieStore, UserService) {
    $scope.rememberMe = false;
    $scope.login = function () {
        UserService.authenticate($.param({username: $scope.username, password: $scope.password}), function (authenticationResult) {
            var accessToken = authenticationResult.token;
            $rootScope.accessToken = accessToken;
            if ($scope.rememberMe) {
                $cookieStore.put('accessToken', accessToken);
            }
            UserService.get(function (user) {
                $rootScope.user = user;
                sessionStorage.setItem("user", user.name);
                $location.path("/");
            });
        }, function (err) {
            $rootScope.error = err.data;
        });
    };
}

var services = angular.module('ciApp.services', ['ngResource']);

services.factory('UserService', function ($resource) {

    return $resource('rest/user/:action', {}, {
        authenticate: {
            method: 'POST',
            params: {'action': 'authenticate'},
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        }
    });

});

services.factory('GroupService', function ($resource) {
    return $resource('rest/groups/:id', {id: '@id'});
});
