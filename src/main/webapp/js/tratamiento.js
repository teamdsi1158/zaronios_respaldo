'use strict';

angular.module('ciApp').factory('tratamientoFactory', ['$q', '$http', function ($q, $http) {
        return {
            listarTratamiento: listarTratamiento,
            listarTratamientoInactivos: listarTratamientoInactivos,
            guardarTratamiento: guardarTratamiento,
            guardarProcedimiento: guardarProcedimiento,
            darBajaProcedimiento: darBajaProcedimiento,
            obtenerTratamiento: obtenerTratamiento,
            activarTratamiento: activarTratamiento,
            darBajaTratamiento: darBajaTratamiento,
            eliminarTratamiento: eliminarTratamiento,
            listarTratamientoPaciente: listarTratamientoPaciente,

        };

        function listarTratamiento() {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/list/').then(function (response) {
                deferred.resolve(response.data);
               
            }, function (errResponse) {
                console.error('Error mientras carga los tratamientos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarTratamientoInactivos() {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/listDesactivated/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los tratamientos inactivos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        


        function guardarTratamiento(tratamiento) {
            var deferred = $q.defer();
            $http.post('rest/tratamiento/tratamientosvc/addAndUp/', tratamiento).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el tratamiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarProcedimiento(procedimiento){   
            var deferred=$q.defer();
            $http.post('rest/procedimiento/procedimientosvc/addAndUp/', procedimiento).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el procedimiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
            
        }
        function darBajaProcedimiento(id){
            var deferred= $q.defer();
            $http.delete('rest/procedimiento/procedimientosvc/del/'+id+'/').then(function(response){
                deferred.resolve(response.data);
            }, function(errResponse){
                console.error('Error mientras borraba Procedimiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }


        function obtenerTratamiento(id) {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/get/' + id + '/').then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras obtenia el tratamiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function darBajaTratamiento(id) {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja el tratamiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function activarTratamiento(id) {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/up/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se activaba el tratamiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function eliminarTratamiento(id) {
            var deferred = $q.defer();
            $http.delete('rest/tratamiento/tratamientosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el tratamiento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function listarTratamientoPaciente() {
            var deferred = $q.defer();
            $http.get('rest/tratamiento/tratamientosvc/statistics/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las estadisticas de los tratamientos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    }
]);

angular.module('ciApp').controller('tratamientoController', ['$scope', 'tratamientoFactory','$location','$routeParams','$filter', function ($scope, tratamientoFactory, $location,$routeParams,$filter) {
        $scope.iconReverse="up";
        $scope.order='persona.perPrimerNombre';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        var modificarTratamiento=false;
        $scope.editar = false;
        $scope.listaTratamiento = [];
        $scope.editar=false;
         ////////////////////////////////////////
        $scope.numero=0;        //para asignarle el numero de procedimiento al tratamiento
        
        $scope.tratamiento = {
            "tratNombre": null,
            "tratDescripcion": null
            
        };
         
         
            //Agrega un procedimiento a la lista de procedimientos a agregar o modificar
        $scope.agregarProcedimiento=function(){
            $scope.procedimientoList.push({"procNumero": ++$scope.numero,"procId":null});
        };
        
         //sentencia para que no cargue innecesariamente lista de Tratamientos
      
        if($location.path()=='/editTratamiento/'+$routeParams.tratId){  
            $scope.editar=true;
            obtenerTratamiento($routeParams.tratId);
        }else{
            $scope.editar=false;
        }
        
            //Retira el procedimiento seleccionado
        $scope.quitarProcedimiento=function(){
            swal({
                title: "\u00BFDesea eliminar el ultimo procedimiento de la lista?",
                text: "\u00A1El Procedimiento ser&aacute eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo eliminarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
            if($scope.procedimientoList[$scope.procedimientoList.length-1].procId!==null){
            var proc=$scope.procedimientoList.pop();
            tratamientoFactory.darBajaProcedimiento(proc.procId).then(function (msj) {
                    Materialize.toast('' + msj, 1000);
                }).catch(function (error) {
                    console.error(error);
                });
            $scope.numero--;
            }
            else{
                 $scope.$apply(function () {
             var proc=$scope.procedimientoList.pop();
        });
        $scope.numero--;
                 Materialize.toast('' + "eliminado", 1000);
            }
            swal("\u00A1Deshabilitado!", "El Procedimiento seleccionado fue eliminado.", "success");
                });
        };
        
            //Inicializa la lista de tratamientos
        $scope.procedimientoList = [];
        //Extrae la lista de tratamientos activos
        $scope.listarTratamiento=function() {
            tratamientoFactory.listarTratamiento().then(function (lista) {
                $scope.listaTratamiento = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems); 
                 $scope.loaderVar=false; //Variable para el loader
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        }
        
         //Extrae la lista de tratamientos desactivos
        $scope.listarTratamientoInactivos=function() {
            $scope.tablaTratamientosInactivos=true;
            tratamientoFactory.listarTratamientoInactivos().then(function (lista) {
                $scope.listaTratamiento = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
                 $scope.loaderVar=false; //Variable para el loader
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        }

        //Guarda un tratamiento, primero guarda el tratamiento, luego itera los procedimientos
        $scope.guardarTratamiento = function () {
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            tratamientoFactory.guardarTratamiento($scope.tratamiento).then(function (tratamiento) {
                
                for (var i=0;i<$scope.procedimientoList.length;i++){
                                     
                    $scope.procedimientoList[i].trataId=tratamiento.tratId;
                    tratamientoFactory.guardarProcedimiento($scope.procedimientoList[i]).then(function(procedimientoi){
                        $('#preload1').remove("active");
                        $('#preload2').remove("active");
                         //Materialize.toast('' + procedimientoi, 1000);
                    });
                
                }

                swal({
                     title: "\u00A1Guardado!",
                     text: "Tratamiento guardado con &eacutexito",
                     type: "success",
                     html: true, 
                });

                $location.path('/tratamientos');
            }).catch(function (error) {
                        $('#preload1').remove("active");
                        $('#preload2').remove("active");
                console.error(error);
            });
        };
        
            //Obtiene el tratamiento para su edicion
        function obtenerTratamiento(id) {
            modificarTratamiento = true;
            tratamientoFactory.obtenerTratamiento(id).then(function (data) {
                
                $scope.tratamiento = data;
                $scope.procedimientoList=data.procedimientoList;
                $scope.numero=$scope.tratamiento.procedimientoList.length;
                $('#tipo').focus();
                $('#ldescripcion').addClass('active');
                $('#lprocedimiento').addClass('active');
            }).catch(function (error) {
                console.error(error);
            });

        };

            //Deshabilita un tratamiento 
        $scope.darBajaTratamiento = function (id) {
            swal({
                title: "\u00BFDesea deshabilitar este tratamiento?",
                text: "\u00A1El tratamiento ser&aacute deshabilitado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo deshabilitarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
                tratamientoFactory.darBajaTratamiento(id).then(function (msj) {
                    Materialize.toast('' + msj, 1000);
                    $scope.listarTratamiento();
                }).catch(function (error) {
                    console.error(error);
                });
                swal("\u00A1Deshabilitado!", "El tratamiento seleccionado fue deshabilitado.", "success");
                    });
        };
        
        //Vuelve a activar el Tratamiento seleccionado
        $scope.activarTratamiento = function (id) {
           swal({
                title: "\u00BFDesea activar este tratamiento?",
                text: "\u00A1 Este registro sera activado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Si, deseo activar!",
                closeOnConfirm: false
                },
                function(){
            tratamientoFactory.activarTratamiento(id).then(function (msj) {
                Materialize.toast('' + msj, 2000);
                $scope.listarTratamientoInactivos();
            }).catch(function (error) {
                console.error(error);
            });
            swal("Activado!", "El tratamiento seleccionado fue activado.", "success");
                    });
        
        };
        
         //Eliminar un tratamiento 
         $scope.eliminarTratamiento = function (id) {
            swal({
                title: "\u00BFDesea eliminar definitivamente este tipo de  tratamiento?",
                text: "\u00A1El tratamiento ser&aacute eliminao,<br>la operaci&oacuten no se puede revertir!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo eliminarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
                tratamientoFactory.eliminarTratamiento(id).then(function (msj) {
                    Materialize.toast('' + msj, 1000);
                    $scope.listarTratamientoInactivos();
                }).catch(function (error) {
                    console.error(error);
                });
                swal("\u00A1Eliminado!", "Su registro seleccionado fue eliminado del sistema.", "success");
                    });
        };
//      Listar las estadisticas de los tratamientos        
         $scope.listarTratamientoPacientes=function() {
            tratamientoFactory.listarTratamientoPaciente().then(function (lista) {
                $scope.listaTratamientoEstadistica = lista;
            }).catch(function (error) {
                console.error(error);
            });
        };
        
        
        $scope.getData = function () {
         return $filter('filter')($scope.listaTratamiento, $scope.searchString);
         };
         
         $scope.cancelar=function(){
          if($scope.tratamiento.tratEstado==true){
              $location.path('/tratamientos'); 
          }else{
              $location.path('/configuracion'); 
          }  
        };
         //Para remover la tabla de inactivos en el menu configuracion
         $scope.MoverTablaInactivos=function(){
            $scope.tablaTratamientosInactivos=false;
        };
        
        $scope.mostrarGrafico=function(b){
            $scope.grafico=b;
            $scope.label="N de personas por Tratamientos";
             var tabla=ordenarTabla();
        // Get the context of the canvas element we want to select
        var countries= document.getElementById("tratamientos").getContext("2d");
        
      $scope.myChart = new Chart(countries, {
    type: $scope.tipoGrafico,
    data: {
        datasets: [{
            label: $scope.label,
            data: tabla.data,
            backgroundColor: tabla.rgbabackgroundColor,
            //borderColor: tabla.rgbaBorderColor,
            borderWidth: 1
        }],
    labels: tabla.label
    },
    options: {
            responsive: true,
             legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        },
         title: {
            display: true,
            text: 'Tratamientos'
        }
       
        }
});
        };
        
        function ordenarTabla(){
            var tabla ={
                data: [],
                label: [],
                rgbabackgroundColor: [],
                rgbaBorderColor: []
            };
            for(var i=0;i<$scope.listaTratamientoEstadistica.length;i++){
                tabla.label[i]=($scope.listaTratamientoEstadistica[i])[1];
                tabla.data[i]=($scope.listaTratamientoEstadistica[i])[2];
                tabla.rgbabackgroundColor[i]=generatorRgba();
                tabla.rgbaBorderColor[i]=generatorRgba();
            }
            return tabla;
        }
        
        $scope.mostrarTratamiento = function(b){
            $scope.mostrarTratamientos=b;
        };
        
        $scope.cambiarGrafico=function(){
        
       if($scope.tipoGrafico==="pie"){
            $scope.$apply(
           $scope.tipoGrafico="bar"    
           );
           
       }else{
           $scope.$apply(
           $scope.tipoGrafico="pie"  );
           
       }
       $scope.$apply(
           $scope.myChart
           );
         
      
        };
        
        
        function generatorRgba(){
            var valor="rgba(";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=0.5+")";
            return valor;
        }
        
    }
]);