'use strict';

angular.module('ciApp').factory('tipoSignoVitalFactory', ['$q', '$http', function ($q, $http) {
    return{
     listarTipoSignoVital: listarTipoSignoVital,
     guardarTipoSignoVital: guardarTipoSignoVital,
     guardarSignoVital: guardarSignoVital,
     obtenerTipoSignoVital: obtenerTipoSignoVital,
     eliminarTipoSignoVital: eliminarTipoSignoVital
     
    }; 
    function listarTipoSignoVital() {
            var deferred = $q.defer();
            $http.get('rest/tipoSigno/tipoSignosvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los tipos de signos vitales');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function guardarTipoSignoVital(tipoSignoVital) {
            var deferred = $q.defer();
            $http.post('rest/tipoSigno/tipoSignosvc/save/', tipoSignoVital).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el tipo de signo vital');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarSignoVital(tipoSignoVital) {
            var deferred = $q.defer();
            tipoSignoVital.sivValor=parseFloat(tipoSignoVital.sivValor);
            $http.post('rest/signoVital/signoVitalsvc/save/', tipoSignoVital).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el signo vital'+errResponse);
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerTipoSignoVital(id) {
            var deferred = $q.defer();
            $http.get('rest/tipoSigno/tipoSignosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el signo vital');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function eliminarTipoSignoVital(id) {
            var deferred = $q.defer();
            $http.delete('rest/tipoSigno/tipoSignosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el signo vital');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
}]);

angular.module('ciApp').controller('tipoSignoVitalController', ['$scope', 'tipoSignoVitalFactory','$location','$routeParams','$filter','paginacion','mensaje', function ($scope, tipoSignoVitalFactory, $location,$routeParams,$filter,paginacion,mensaje) {
 $scope.iconReverse="up";
        $scope.order='dsivNombre';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
         $scope.editar = false;
         listarSignoVital();
         $scope.listaTipoSignoVital={};
         $scope.regDecimales="(\u005C.[0-9]{1,";   
         $scope.decimales=0;
         
         
          $scope.getData = function () {
         return $filter('filter')($scope.listaTipoSignoVital, $scope.searchString);
         };
         
          $scope.orderBy=function(order){
          $scope.order=order;
          $scope.reverse=!$scope.reverse;
          if($scope.reverse){
              $scope.iconReverse="down";
          }else{
              $scope.iconReverse="up";
          }
        };
            //obtiene el signo vital seleccionado
          function obtenerSignoVital(){
         tipoSignoVitalFactory.obtenerTipoSignoVital($routeParams.dsivId).then(function(tipoSignoVital){
            $scope.tipoSignoVital=tipoSignoVital; 
            validarRestriccion();
         });
        }
        
         if($location.path()==='/editTipoSignoVital/'+$routeParams.dsivId){
            $scope.editar=true;
           
            obtenerSignoVital($routeParams.dsivId);
        }else{
            $scope.editar=false;
        }
            //Guarda el signo vital
         $scope.guardarSignoVital=function(){
          $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            
            if($scope.decimal==true){
            $scope.tipoSignoVital.dsivRestriccion="[0-9]{1,3}"+$scope.regDecimales+$scope.decimales+"})?";
            }else{
                $scope.tipoSignoVital.dsivRestriccion="[0-9]{1,3}";
            }
            if($scope.negativo==true){
                $scope.tipoSignoVital.dsivRestriccion="[-]?"+$scope.tipoSignoVital.dsivRestriccion;
            }
            
                 tipoSignoVitalFactory.guardarTipoSignoVital($scope.tipoSignoVital).then(function (msj) {
                $('#preload1').removeClass("active");
                $('#preload2').removeClass("active");
                //Materialize.toast('' + msj, 10000);
                 mensaje.resultado("Signo vital",msj);
                  $location.path('/atributos');  
                 listarSignoVital(); 
            });  
        };
        
        //lista la publicidad disponible
        function listarSignoVital() {
            tipoSignoVitalFactory.listarTipoSignoVital().then(function (lista) {
                $scope.listaTipoSignoVital = lista;
                $scope.paginas=Math.ceil(lista.length/$scope.numItems);
            }).catch(function (error) {
                console.error(error);
            });
        }
        
         $scope.eliminarTipoSignoVital = function (id) {
            swal({
                title: "\u00BF Desea eliminar este signo vital?",
                text: "\u00A1 Este registro sera Eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo eliminar!",
                closeOnConfirm: false
                },
                function(){
                tipoSignoVitalFactory.eliminarTipoSignoVital(id).then(function (msj) {
                    mensaje.resultado(" Signo Vital",msj);
                    listarProfesion();
                }).catch(function (error) {
                    console.error(error);
                });
               
                    });
                      listarSignoVital(); 
        };
        $scope.validaDecimal=function(){
            if($scope.decimal!=true){
                $scope.decimales=0;
            }
        }
        function validarRestriccion(){
            var decimal=$scope.tipoSignoVital.dsivRestriccion.indexOf("[-]");
            if(decimal!=-1){
                $scope.negativo=true;

            }
            if($scope.tipoSignoVital.dsivRestriccion.indexOf(".[0-9]")!=-1){
                  $scope.decimal=true;
                  if(decimal!=-1){
                $scope.decimales=$scope.tipoSignoVital.dsivRestriccion.substring(25,26);
            }else{
                $scope.decimales=$scope.tipoSignoVital.dsivRestriccion.substring(21,22);
            }
        }
    }
 }]);