//charset: utf-8
angular.module('ciApp', ['ngRoute', 'ngCookies', 'ciApp.services'])
        .config(
                ['$routeProvider', '$locationProvider', '$httpProvider', function ($routeProvider, $locationProvider, $httpProvider) {

                        $routeProvider.when('/login', {
                            templateUrl: 'partials/login.html',
                            controller: LoginController
                        });

                        $routeProvider.otherwise({
                            templateUrl: 'partials/login.html',
                            controller: LoginController
                        });

                        $locationProvider.hashPrefix('!');

                        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
                            return {
                                'responseError': function (rejection) {
                                    var status = rejection.status;
                                    var config = rejection.config;
                                    var method = config.method;
                                    var url = config.url;

                                    if (status == 401) {
                                        $location.path("/login");
                                    } else {
                                        $rootScope.error = method + " on " + url + " failed with status " + status;
                                    }

                                    return $q.reject(rejection);
                                }
                            };
                        });

                        $httpProvider.interceptors.push(function ($q, $rootScope, $location) {
                            return {
                                'request': function (config) {
                                    //var isRestCall = config.url.indexOf('rest') == 0;
                                    var isRestCall = true;
                                    if (isRestCall && angular.isDefined($rootScope.accessToken)) {
                                        var accessToken = $rootScope.accessToken;
                                        if (exampleAppConfig.useAccessTokenHeader) {
                                            config.headers['X-Access-Token'] = accessToken;
                                        } else {
                                            config.url = config.url + "?token=" + accessToken;
                                        }
                                    }
                                    return config || $q.when(config);
                                }
                            };
                        }
                        );

                    }]

                ).run(function ($rootScope, $location, $cookieStore, UserService) {

    $rootScope.$on('$viewContentLoaded', function () {
        delete $rootScope.error;
    });

    $rootScope.hasRole = function (role) {

        if ($rootScope.user === undefined) {
            return false;
        }

        if ($rootScope.user.roles[role] === undefined) {
            return false;
        }

        return $rootScope.user.roles[role];
    };

    $rootScope.logout = function () {
        delete $rootScope.user;
        delete $rootScope.accessToken;
        $cookieStore.remove('accessToken');
        window.location.href = "/zaronios";
    };

    var originalPath = $location.path();
    $location.path("/login");
    var accessToken = $cookieStore.get('accessToken');
    if (accessToken !== undefined) {
        $rootScope.accessToken = accessToken;
        UserService.get(function (user) {
            $rootScope.user = user;
            $location.path(originalPath);
        });
    }

    $rootScope.initialized = true;

});

function LoginController($scope, $rootScope, $location, $cookieStore, UserService) {

    $scope.rememberMe = true;

    $scope.login = function () {
        UserService.authenticate($.param({username: $scope.username, password: $scope.password}), function (authenticationResult) {
            var accessToken = authenticationResult.token;
            $rootScope.accessToken = accessToken;
            if ($scope.rememberMe) {
                $cookieStore.put('accessToken', accessToken);
            }
            sessionStorage.accessToken = accessToken;
            UserService.get(function (user) {
                $rootScope.LoginError = false;
                $rootScope.user = user;
                window.location.href = "main.jsp";
            });
        }, function (err) {
            $rootScope.LoginError = true;
            $rootScope.LoginErrorDetails = err.data;
        });
    };
}

var services = angular.module('ciApp.services', ['ngResource']);

services.factory('UserService', function ($resource) {

    return $resource('rest/user/:action', {},
            {
                authenticate: {
                    method: 'POST',
                    params: {'action': 'authenticate'},
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }
            }
    );
});