/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

angular.module('ciApp').factory('expedienteFactory', ['$q', '$http', function ($q, $http) {
        return {
            listarAsignacion: listarAsignacion,
            guardarExpediente: guardarExpediente,
            guardarAsignacion: guardarAsignacion,
            finalizarAsignacion: finalizarAsignacion,
            obtenerExpediente: obtenerExpediente,
            obtenerAsignacion: obtenerAsignacion
            
            
        };
        
         function listarAsignacion(expId) {
            var deferred = $q.defer();
            $http.get('rest/asignacion/asignacionsvc/list/'+expId+'/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga la lista de asignaciones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarExpediente(tratamiento) {
            
            var deferred = $q.defer();
            $http.post('rest/tratamiento/tratamientosvc/addAndUp/', JSON.stringify(tratamiento)).then(function (response) {   
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los cantratantes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarAsignacion(asignacion) {
            
            var deferred = $q.defer();
            $http.post('rest/asignacion/asignacionsvc/save/', asignacion).then(function (response) {   
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras Guardaba la asignacion ');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }


        function obtenerExpediente(id) {
            var deferred = $q.defer();
            $http.get('rest/paciente/pacientesvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los cantratantes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function obtenerAsignacion(asitId) {
            var deferred = $q.defer();
            $http.get('rest/asignacion/asignacionsvc/get/' + asitId + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la asignacion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function finalizarAsignacion(id) {
            var deferred = $q.defer();
            $http.get('rest/asignacion/asignacionsvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja la asignacion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
    }
]);

//Permite la comunicación con otros Factorys, Ej: Se podrán usar las funciones de tratamiento.js al incluir tratamientoFactory
angular.module('ciApp').controller('expedienteController', ['$scope', 'expedienteFactory', 'tratamientoFactory','citaFactory','sesionFactory','$routeParams','mensaje','$http', function ($scope, expedienteFactory,tratamientoFactory,citaFactory,sesionFactory,$routeParams,mensaje,$http) {
    $scope.activarDescripcion=false;
    $scope.sesion={};
    $scope.cita={};
    $scope.paciente={};
    findAllTratamientos();
    $scope.asignacion={expId: null, asitFinalizado: null};
    obtenerExpediente($routeParams.pacId);
    $scope.listarTratamientos= [];
    $scope.listaAsignaciones= [];
    
//  Guarda una nueva asignacion al paciente    
    $scope.guardarAsignacion=function(){
        $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            $scope.asignacion.expId=$scope.paciente.expId;
            $scope.asignacion.asitFinalizado=false;
      expedienteFactory.guardarAsignacion($scope.asignacion).then(function(msj){
           $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                mensaje.resultado(" Asignacion",msj);
                findAllAsignaciones();
                 $('#nuevoTratamientoModal').closeModal();
                 swal({
                    title: "\u00A1Guardado!",
                    text: "Tratamiento creado con &eacutexito",
                    type: "success",
                    html: true, 
                });
      });  
    };
//  Obtener expediente    
    function obtenerExpediente(id){
        expedienteFactory.obtenerExpediente(id).then(function (data) {
                $scope.paciente = data;
                findAllAsignaciones();
                
        }).catch(function (error) {
                console.error(error);
            });
    }

//Para capturar tofos los Tratamientos 
    function findAllTratamientos(){
        
        tratamientoFactory.listarTratamiento().then(function (tratamientos) {
            
            $scope.listaTratamientos = tratamientos;
        }).catch(function (error) {
                
        });
    }
//  Lista todas las asignaciones de tratamientos    
    function findAllAsignaciones(){
        
        expedienteFactory.listarAsignacion($scope.paciente.expId).then(function (lista) {
            
            $scope.listaAsignaciones = lista;
            
             setTimeout(function () {
                  $scope.$apply(function () {
 $(document).ready(function(){
      $('.slider').slider();
    });
                  });
   }, 3000);
            $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }

            configuration();
             
        }).catch(function (error) {
                
        });
    }
    
    function configuration(){
        $(document).ready(function(){
    $('.collapsible').collapsible();
  });
    }
//  Guardar Cita    
    $scope.guardarCita=function(){
        $('#preload1').addClass("active");
        $('#preload2').addClass("active");
        $scope.cita.estId=1;
      citaFactory.guardarCita($scope.cita).then(function(idCita){
          $scope.sesion.asitId=parseInt($scope.sesion.asitId);
         $scope.sesion.citaId=idCita;
          sesionFactory.guardarSesion($scope.sesion);
          $("#modalCita").closeModal();
          findAllAsignaciones()
            $('#preload1').removeClass("active");
            $('#preload2').removeClass("active");
      });
    };
//  Finalizar Tratamiento    
    $scope.finalizar=function(id){
        swal({
                title: "\u00BFDesea finalizar este tratamiento?",
                text: "\u00A1El tratamiento ser&aacute finalizado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo finalizarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
            expedienteFactory.finalizarAsignacion(id).then(function (msj) {
                mensaje.resultado("Tratamiento",msj);
               findAllAsignaciones();
            }).catch(function (error) {
                console.error(error);
            });
            
                    });
    };
//  Iniciar Sesion de tratamiento        
    $scope.iniciarSesion=function(id){
            $scope.sesion.asitId=id;
            $scope.cita.citaFecha=new Date();
        $scope.cita.estId=5;
        citaFactory.guardarCita($scope.cita).then(function(idCita){
           $scope.sesion.citaId=idCita;
           sesionFactory.guardarSesion($scope.sesion);
            $("#modalSesion").closeModal();
           findAllAsignaciones();
        });
        };
//  Abre para agregar nuevo tratamiento        
    $scope.nuevoTratamiento=function(){
            $('#nuevoTratamientoModal').openModal({
                dismissible: false
            });
        };
//  Abre para agregar nueva cita        
    $scope.nuevaCita=function(){
            $("#modalCita").openModal({
                dismissible: false
                
            });
        };
        
//  Cancelar una cita cambiando el estado de esta        
    $scope.cancelar=function(sesion){
        
        var cita=sesion.cita;
         swal({
                title: "\u00BFDesea cancelar esta sesion?",
                text: "\u00A1La sesion ser&aacute cancelada!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo cancelarla",
                closeOnConfirm: false,
                html: true
                },
                function(){
                    cita.estId=2;
                    delete cita.citaDia;
                    delete cita.citaHora;
                    citaFactory.guardarCita(cita);
          findAllAsignaciones();
            });
        };
//  Abre para agregar nieva sesion        
    $scope.nuevaSesion=function(){
            $("#modalSesion").openModal({
                dismissible: false
                
            });
        };
//  Cambiar Modo de descripcion
    $scope.cambiarDescripcion=function(){
        $scope.activarDescripcion=!$scope.activarDescripcion;
        console.log($scope.activarDescripcion);
    };
        //Generar Reporte de todos los tratamientos
        $scope.generarReporteExpediente = function () {
            $http.get('rest/reportsvc/report_expediente/' + $scope.paciente.expId + '/', {responseType: 'arraybuffer'}).then(function (response) {
                var archivo = new Blob([response.data], {type: 'application/pdf'});
                var isChrome = !!window.chrome && !!window.chrome.webstore;
                if (isChrome) {
                    var url = window.URL || window.webkitURL;
                    var fileURLi = URL.createObjectURL(archivo);
                    window.open(fileURLi, "_blank");
                } else {
                    var fileURL = URL.createObjectURL(archivo);
                    window.open(fileURL);
                }
                mensaje.resultado($scope.empleado.persona.perPrimerNombre+" "+$scope.empleado.persona.perPrimerApellido, "GENERADO");
            }).catch(function (error) {
                console.error(error);
            });
        };
        
         //Generar Reporte de la informacion de el paciente
        $scope.generarReportepaciente = function () {
            $http.get('rest/reportsvc/report_paciente/' + $scope.paciente.pacId + '/', {responseType: 'arraybuffer'}).then(function (response) {
                var archivo = new Blob([response.data], {type: 'application/pdf'});
                var isChrome = !!window.chrome && !!window.chrome.webstore;
                if (isChrome) {
                    var url = window.URL || window.webkitURL;
                    var fileURLi = URL.createObjectURL(archivo);
                    window.open(fileURLi, "_blank");
                } else {
                    var fileURL = URL.createObjectURL(archivo);
                    window.open(fileURL);
                }
                mensaje.resultado($scope.empleado.persona.perPrimerNombre+" "+$scope.empleado.persona.perPrimerApellido, "GENERADO");
            }).catch(function (error) {
                console.error(error);
            });
        };
    
    }
]);

