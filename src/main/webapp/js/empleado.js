'use strict';

angular.module('ciApp').factory('empleadoFactory', ['$q', '$http', function ($q, $http) {

        return {
            listarEmpleado: listarEmpleado,
            listarEmpleadosInactivo: listarEmpleadosInactivo,
            listarEmpleadoSinUsuario: listarEmpleadoSinUsuario,
            guardarEmpleado: guardarEmpleado,
            guardarImagen: guardarImagen,
            guardarPerfil: guardarPerfil,
            obtenerEmpleado: obtenerEmpleado,
            activarEmpleado: activarEmpleado,
            darBajaEmpleado: darBajaEmpleado,
            eliminarEmpleado: eliminarEmpleado,
            listarRoles: listarRoles
        };

        function listarEmpleado() {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/list/').then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los Empleados');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listarEmpleadosInactivo() {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/listDesactivated/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los empleados inactivos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listarEmpleadoSinUsuario() {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/listNotUser/').then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los Empleados');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listarRoles() {
            var deferred = $q.defer();
            $http.get('rest/rol/rolsvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los cantratantes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarEmpleado(empleado) {
            var deferred = $q.defer();
            
            $http.post('rest/empleado/empleadosvc/save/', empleado).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el empleado');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        function guardarImagen(imagen) {
            var deferred = $q.defer();
            
            $http.post('rest/image/imagensvc/save/', imagen).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se guardaba la imagen');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarPerfil(perfil) {
            var deferred = $q.defer();

            $http.post('rest/image/imagensvc/savePerfil/', perfil).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se guardaba la imagen de perfil');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }



        function obtenerEmpleado(id) {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras obtenia el empleado');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function activarEmpleado(id) {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/up/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras en la activacion del empleado');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function darBajaEmpleado(id) {
            var deferred = $q.defer();
            $http.get('rest/empleado/empleadosvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja el empleado');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function eliminarEmpleado(id) {
            var deferred = $q.defer();
            $http.delete('rest/empleado/empleadosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el empleado');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
    }
]);

angular.module('ciApp').controller('empleadoController', ['$scope', 'empleadoFactory', 'pacienteFactory', '$location', '$routeParams', 'paginacion', '$http','mensaje', function ($scope, empleadoFactory, pacienteFactory, $location, $routeParams, paginacion, $http,mensaje) {
        $scope.iconReverse = "up";
        $scope.order = 'persona.perPrimerNombre';           //para orden de la lista de publicidad
        $scope.reverse = false;
        //Cantidad de Items por pagina
        $scope.numItems = 10;
        //paginas a mostrar, se realiza un calculo de este
        $scope.paginas = 1;
        $scope.searchString = '';
        //para mostrar guardado o edicion
        $scope.perfil = {

        };
        $scope.imagen = {
            "data": null
        };
        $scope.editar = false;

        $scope.getData = function () {
            return $filter('filter')($scope.listaEmpleado, $scope.searchString);
        };

        $scope.previewFile = function () {         //funcion para mostrar el preview de imagen

            var preview = document.querySelector('#image');
            var file = document.querySelector('input[type=file]').files[0];
            var reader = new FileReader();

            reader.onloadend = function () {
                preview.src = reader.result;
                $scope.imagen.data = preview.src;
            }
            if (file) {
                reader.readAsDataURL(file);
            } else {
                preview.src = "";
            }
        };

        if($location.path() == '/addEmpleado' || $location.path() == '/editEmpleado/' + $routeParams.empId) {
             findAllEstadoCivil();
            findAllSexo();
            findAllDepartamento();
            findAllPais();
            
            setTimeout(function () {
            $scope.$apply(function () {
             $(document).ready(function() {
    $('select').material_select();
                    });
             }); 
              }, 6000);
        }

        if ($location.path() == '/editEmpleado/' + $routeParams.empId || $location.path() == '/expedienteEmpleado/' + $routeParams.empId) {
            obtenerEmpleado($routeParams.empId);
            $scope.editar = true;
        } else {
            $scope.editar = false;
        }

        $scope.empleado = {
            "empId": null,
            "empIsss": "",
            "empEstado": true,
            "perId": null
        };

        $scope.departamento = {
            depCodigo: null
        };

        $scope.contacto = {
            "conId": null,
            "contNombre": "",
            "contApellido": "",
            "contTelefono": null
        };

        $scope.documento = {
            "docId": null,
            "docNombre": "ISSS",
            "docCodigo": ""
        };

        $scope.persona = {
            "perId": null,
            "perTelCelular": null,
            "perTelFijo": null,
            "perFechaNacimiento": "",
            "perSegundoNombre": "",
            "perPrimerNombre": "",
            "perPrimerApellido": "",
            "perSegundoApellido": null,
            "perApellidoCasada": null,
            "perCorreoElectronico": "",
            "perDireccion": "",
            "perColonia": "",
            "conId": null,
            "docId": null,
            "estId": null,
            "munCodigo": null,
            "paiCodigo": 503,
            "sexId": null
        };

        $scope.listaEmpleados = [];
        $scope.listaEmpleado = [];
        $scope.listaRoles = [];
        $scope.listaDepartamento = [];
        $scope.listaMunicipio = [];
        $scope.listaSexo = [];
        $scope.listaEstadosCivil = [];
        $scope.listaPais = [];




        function findAllEstadoCivil() {
            pacienteFactory.listaEstadosCivil().then(function (data) {
                $scope.listaEstadosCivil = data;
                $(document).ready(function() {
    $('select').material_select();
                    });
            }).catch(function (error) {
                
            });
        }

        function findAllPais() {
            pacienteFactory.listarPais().then(function (data) {
                $scope.listaPais = data;
            }).catch(function (error) {
                
            });
        }

        $scope.findAllMunicipio = function () {
            $scope.listaMunicipio = [];
            pacienteFactory.listaMunicipios($scope.departamento.depCodigo).then(function (muns) {
                $scope.listaMunicipio = muns;
                
                $(document).ready(function () {
                    $('select').material_select();
                });

            }).catch(function (error) {
                console.error(error);
            });
        };

        function findAllDepartamento() {
            pacienteFactory.listaDepartamentos().then(function (deps) {
                $scope.listaDepartamento = deps;
            }).catch(function (error) {
                console.error(error);
            });
        }

        function findAllSexo() {
            pacienteFactory.listaSexos().then(function (data) {
                $scope.listaSexo = data;
                 $(document).ready(function() {
    $('select').material_select();
                    });
            }).catch(function (error) {
                
            });
        }

        $scope.findAllEmpleado=function() {
            empleadoFactory.listarEmpleado().then(function (lista) {
                $scope.listaEmpleados = lista;
                 $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                
            });
        }

        //lista la publicidad Inactiva
        $scope.listarEmpleadoInactivos = function () {
            $scope.tablaEmpleadosInactivos = true;
            empleadoFactory.listarEmpleadosInactivo().then(function (lista) {
                
                $scope.listaEmpleado = lista;
                
                $scope.paginas = Math.ceil(lista.length / $scope.numItems);
                $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        };

        $scope.print = function () {
            $("#expediente").print({
                globalStyles: true,
                mediaPrint: false,
                stylesheet: "css/materialize.min.css",
                noPrintSelector: ".no-print",
                iframe: true,
                append: null,
                prepend: null,
                manuallyCopyFormValues: true,
                deferred: $.Deferred(),
                timeout: 5000,
                title: "Expediente de Empleado " + $scope.empleado.persona.perPrimerNombre + " " + $scope.empleado.persona.perPrimerApellido,
                doctype: '<!doctype html>'
            });

        };



        $scope.limpiarEmpleado = function () {
            $scope.empleado = {
                "empId": null,
                "empIsss": "",
                "empEstado": true,
                "perId": null
            };

            $scope.departamento = {
                depCodigo: null
            };

            $scope.contacto = {
                "conId": null,
                "contNombre": "",
                "contApellido": "",
                "contTelefono": null
            };

            $scope.documento = {
                "docId": null,
                "docNombre": "ISSS",
                "docCodigo": ""
            };

            $scope.persona = {
                "perId": null,
                "perTelCelular": null,
                "perTelFijo": null,
                "perFechaNacimiento": "",
                "perSegundoNombre": "",
                "perPrimerNombre": "",
                "perPrimerApellido": "",
                "perSegundoApellido": null,
                "perApellidoCasada": null,
                "perCorreoElectronico": "",
                "perDireccion": "",
                "perColonia": "",
                "conId": null,
                "docId": null,
                "estId": null,
                "munCodigo": null,
                "paiCodigo": 503,
                "sexId": null
            };
            $('#modalIngEmpleado').openModal();
        };
        // Para REGISTRAR O MODIFICAR la informacion de un empleado
        $scope.guardarEmpleado = function () {
            
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            pacienteFactory.guardarDocumento($scope.documento).then(function (docu) {
                
                pacienteFactory.guardarContacto($scope.contacto).then(function (cont) {
                    
                    $scope.persona.conId = cont.conId;


                    $scope.persona.docId = docu.docId;
                    if ($scope.persona.conId !== null && $scope.persona.docId !== null && $scope.persona.estId !== null) {
                        
                        delete $scope.persona.sexo;
                        delete $scope.persona.estadoCivil;
                        pacienteFactory.guardarPersona($scope.persona).then(function (per) {

                            $scope.empleado.perId = per.perId;
                            
                            if ($scope.empleado.perId !== null) {
                                $scope.imagen.imgRuta = "C//";
                                if ($scope.imagen.data != null) {
                                    $scope.imagen.imgNombre = per.perId + $scope.persona.perPrimerApellido + $scope.persona.perSegundoApellido;
                                }

                                empleadoFactory.guardarImagen($scope.imagen).then(function (img) {
                                    
                                    $scope.perfil.imgId = img;
                                    // empleadoFactory.guardarPerfil($scope.perfil).then(function(ime){
                                    //  
                                    $scope.empleado.imagen = $scope.perfil;
                                    empleadoFactory.guardarEmpleado($scope.empleado).then(function (msj) {

                                        Materialize.toast('' + msj, 1000);
                                        $('#preload1').remove("active");
                                        $('#preload2').remove("active");
                                        $location.path('/empleados');
                                    });
                                    // });
                                }).catch(function (error) {
                                    $('#preload1').remove("active");
                                    $('#preload2').remove("active");
                                    console.error(error);
                                });

                            }
                        }).catch(function (error) {
                            console.error(error);
                            $('#preload1').remove("active");
                            $('#preload2').remove("active");

                        });
                    }
                }).catch(function (error) {
                    $('#preload1').remove("active");
                    $('#preload2').remove("active");
                    console.error(error);
                });

            }).catch(function (error) {
                $('#preload1').remove("active");
                $('#preload2').remove("active");
                console.error(error);
            });
        };

        //Vuelve a activar el empleado seleccionado
        $scope.activarEmpleado = function (id) {
            swal({
                title: "\u00BFDesea activar este empleado?",
                text: "\u00A1La empleado seleccionado ser&aacute activado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Si, deseo activarlo",
                closeOnConfirm: false,
                html: true
            },
                    function () {
                        empleadoFactory.activarEmpleado(id).then(function (msj) {
                            Materialize.toast('' + msj, 2000);
                            $scope.listarEmpleadoInactivos();
                        }).catch(function (error) {
                            console.error(error);
                        });
                        mensaje.resultado("\u00A1Activada!", "El empleado seleccionado fue activado", "success");
                    });

        };

        $scope.darBajaEmpleado = function (id) {
            swal({
                title: "\u00BF Desea deshabilitar este empleado?",
                text: "\u00A1 Este registro sera deshabilitado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo deshabilitar!",
                closeOnConfirm: false
            },
                    function () {
                        empleadoFactory.darBajaEmpleado(id).then(function (msj) {
                           mensaje.resultado("Empleado", msj);
                            $scope.findAllEmpleado();
                        }).catch(function (error) {
                            console.error(error);
                        });
                        
                    });
        };

        //eliminar la publicidad 
        $scope.eliminarEmpleado = function (id) {

            swal({
                title: "\u00BFDesea eliminar definitivamente este empleado?",
                text: "\u00A1La empleado ser&aacute eliminado, <br>la operaci&oacuten no se puede revertir!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo eliminarla",
                closeOnConfirm: false,
                html: true
            },
                    function () {
                        empleadoFactory.eliminarEmpleado(id).then(function (msj) {
                            mensaje.resultado("Empleado", msj);
                            $scope.listarEmpleadoInactivos();

                        }).catch(function (error) {
                            console.error(error);
                        });
                        mensaje.resultado("\u00A1Eliminada!", "El empleado seleccionado fue eliminado", "success");
                    });
        };

        function obtenerEmpleado(id) {
            empleadoFactory.obtenerEmpleado(id).then(function (data) {
                
                $scope.persona = data.persona;
                $scope.contacto = data.persona.contacto;
                $scope.departamento = data.persona.municipio.departamento;
                $scope.documento = data.persona.documento;
                $scope.empleado = data;
                $scope.imagen = data.imagen.imgId;
                $scope.sexo = $scope.persona.sexo;
                $scope.pais = $scope.persona.pais;
                $scope.estadoCivil = $scope.persona.estadoCivil;
                $scope.municipioAux = data.persona.municipio;
                $scope.departamentoAux = data.persona.municipio.departamento.depCodigo;
                $scope.departamentoNombre = data.persona.municipio.departamento.depNombre;
                 $scope.findAllMunicipio();
                
                 $(document).ready(function() {
            $('select').material_select();
                });
                $('label').addClass('active');
            }).catch(function (error) {
                console.error(error);
            });
        }
        ;


        //Para remover la tabla de inactivos en el menu configuracion
        $scope.MoverTablaInactivos = function () {
            $scope.tablaEmpleadosInactivos = false;
        };

            //Generar Reporte de empleado
       $scope.generarReporteEmpleado = function () {
            $http.get('rest/reportsvc/report_empleado/' + $routeParams.empId + '/', {responseType: 'arraybuffer'}).then(function (response) {
                var archivo = new Blob([response.data], {type: 'application/pdf'});
                var isChrome = !!window.chrome && !!window.chrome.webstore;
                if (isChrome) {
                    var url = window.URL || window.webkitURL;
                    var fileURLi = URL.createObjectURL(archivo);
                    window.open(fileURLi, "_blank");
                } else {
                    var fileURL = URL.createObjectURL(archivo);
                    window.open(fileURL);
                }
                mensaje.resultado($scope.empleado.persona.perPrimerNombre+" "+$scope.empleado.persona.perPrimerApellido, "GENERADO");
            }).catch(function (error) {
                console.error(error);
            });
        };
    }
]);
