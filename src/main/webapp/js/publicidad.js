'use strict';

angular.module('ciApp').factory('publicidadFactory', ['$q', '$http', function ($q, $http) {

        return {
            listarPublicidad: listarPublicidad,
            listarPublicidadInactiva: listarPublicidadInactiva,
            listarPublicidadPaciente: listarPublicidadPaciente,
            guardarPublicidad: guardarPublicidad,
            obtenerPublicidad: obtenerPublicidad,
            activarPublicidad: activarPublicidad,
            darBajaPublicidad: darBajaPublicidad,
            eliminarPublicidad: eliminarPublicidad
        };

        function listarPublicidad() {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga las publicaciones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarPublicidadInactiva() {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/listDesactivated/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga las publicidades inactivas');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function listarPublicidadPaciente() {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/statistics/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las estadisticas de las publididades');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarPublicidad(publicidad) {
            
            var deferred = $q.defer();
            $http.post('rest/publicidad/publicidadsvc/save/', publicidad).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba la publicidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }


        function obtenerPublicidad(id) {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras cargaba la publicidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function activarPublicidad(id) {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/up/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras en la activacion de la publicidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function darBajaPublicidad(id) {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja la publicidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function eliminarPublicidad(id) {
            var deferred = $q.defer();
            $http.delete('rest/publicidad/publicidadsvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba la publicidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    }
]);

angular.module('ciApp').controller('publicidadController', ['$scope', 'publicidadFactory','$location','$routeParams','$filter','paginacion','mensaje', function ($scope, publicidadFactory, $location,$routeParams,$filter,paginacion,mensaje) {
        
        $scope.iconReverse="up";
        $scope.order='pubNombre';           //para orden de la lista de publicidad
        $scope.reverse=false;
        //Cantidad de Items por pagina
        $scope.numItems=10;  
        //paginas a mostrar, se realiza un calculo de este
        $scope.paginas=1;
        $scope.searchString='';
        //para mostrar guardado o edicion
        $scope.editar=false;
        //Lista de publicidad 
        $scope.listaPublicidad = [];
         
        $scope.getData = function () {
         return $filter('filter')($scope.listaPublicidad, $scope.searchString);
         };
      
       
        $scope.publicidad = {
            "pubNombre": null,
            "pubDescripcion": null
        };
        
       
        if($location.path()=='/editPublicidad/'+$routeParams.pubId){
            $scope.editar=true;
            obtenerPublicidad($routeParams.pubId);
        }else{
            $scope.editar=false;
        }
        
        //lista la publicidad disponible
        $scope.listarPublicidad=function() {
            publicidadFactory.listarPublicidad().then(function (lista) {
                
                $scope.listaPublicidad = lista;
                $scope.paginas=Math.ceil(lista.length/$scope.numItems);
                $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        }
        
        //lista la publicidad Inactiva
        $scope.listarPublicidadInactiva=function() {
            $scope.tablaPublicidadesInactivas=true;
            publicidadFactory.listarPublicidadInactiva().then(function (lista) {
                
                $scope.listaPublicidad = lista;
                $scope.paginas=Math.ceil(lista.length/$scope.numItems);
                $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        };
        
         $scope.listarPublicidadPacientes=function() {
            
            publicidadFactory.listarPublicidadPaciente().then(function (lista) {
                
                $scope.listaPublicidadEstadistica = lista;
                  
            }).catch(function (error) {
                console.error(error);
            });
        };
        // Guarda la publicidad o la modifica
        $scope.guardarPublicidad = function () {
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            publicidadFactory.guardarPublicidad($scope.publicidad).then(function (msj) {
                $('#preload1').removeClass("active");
                $('#preload2').removeClass("active");
                //Materialize.toast('' + msj, 1000);
                mensaje.resultado(" Publicidad",msj);
               
                if($location.path()=='/addPaciente'|| $location.path()=='/editPaciente/'+$routeParams.pacId){
                   $('#modalIngPublicidad').closeModal();
                    $('#preload1').removeClass("active");
                    $('#preload2').removeClass("active");
                   $scope.findAllPublicidad();
                   
               }else{
                 $location.path('/publicidades');  
               }
            }).catch(function (error) {
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                console.error(error);
            });
        };
           // Obtiene la publicidad para poder editarla
        function obtenerPublicidad(id) {
            publicidadFactory.obtenerPublicidad(id).then(function (data) {
                
                $scope.publicidad = data;
                $('label').addClass('active');
               
            }).catch(function (error) {
                console.error(error);
            });
        };
        
             //Vuelve a activar el paciente seleccionado
        $scope.activarPublicidad = function (id) {
           swal({
                title: "\u00BFDesea activar esta publicidad?",
                text: "\u00A1La publicidad seleccionada ser&aacute activada!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Si, deseo activarla",
                closeOnConfirm: false,
                html: true
                },
                function(){
            publicidadFactory.activarPublicidad(id).then(function (msj) {
                Materialize.toast('' + msj, 2000);
                $scope.listarPublicidadInactiva();
            }).catch(function (error) {
                console.error(error);
            });
            mensaje.resultado("\u00A1Activada!", "La publicidad seleccionada fue activada", "success");

                    });
        
        };
        
            //deshabilita la publicidad 
        $scope.darBajaPublicidad = function (id) {
            swal({
                title: "\u00BFDesea deshabilitar esta publicidad?",
                text: "\u00A1La publicidad ser&aacute deshabilita!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo deshabilitarla",
                closeOnConfirm: false,
                html: true
                },
                function(){
                             publicidadFactory.darBajaPublicidad(id).then(function (msj) {
                             mensaje.resultado("Publicidad",msj);
                             listarPublicidad();
                            
            }).catch(function (error) {
                console.error(error);
            });
           
});
        };
        
         //eliminar la publicidad 
        $scope.eliminarPublicidad = function (id) {
            
            swal({
                title: "\u00BFDesea eliminar definitivamente esta publicidad?",
                text: "\u00A1La publicidad ser&aacute eliminada, <br>la operaci&oacuten no se puede revertir!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo eliminarla",
                closeOnConfirm: false,
                html: true
                },
                function(){
                             publicidadFactory.eliminarPublicidad(id).then(function (msj) {
                             mensaje.resultado(" Publicidad",msj);
                             $scope.listarPublicidadInactiva();
                            
            }).catch(function (error) {
                console.error(error);
            });
           
});
        };
         
        //Para el boton de cancelar
        $scope.cancelar=function(){
          if($scope.publicidad.pubEstado==true){
              $location.path('/publicidades'); 
          }else{
              $location.path('/configuracion'); 
          }  
        };
        //Para remover la tabla de inactivos en el menu configuracion
         $scope.MoverTablaInactivos=function(){
            $scope.tablaPublicidadesInactivas=false;
        };
        
        //Realizar configuracion de los graficos de publicidad
        $scope.mostrarGrafico=function(b){
            $scope.grafico=b;
            $scope.label="N de pacientes por publicidad";
             var tabla=ordenarTabla();
        // Get the context of the canvas element we want to select
        var countries= document.getElementById("publicidad").getContext("2d");
      $scope.myChart = new Chart(countries, {
    type: 'pie',
    data: {
        
        datasets: [{
            label: $scope.label,
            data: tabla.data,
            backgroundColor: tabla.rgbabackgroundColor,
           //borderColor: tabla.rgbaBorderColor,
            borderWidth: 1
        }],
    labels: tabla.label
    },
    options: {
            responsive: true
        }
});
        };
        
        //Para ordenar tabla de estadisticas
        function ordenarTabla(){
            var tabla ={
                data: [],
                label: [],
                rgbabackgroundColor: [],
                rgbaBorderColor: []
            };
            for(var i=0;i<$scope.listaPublicidadEstadistica.length;i++){
                tabla.label[i]=($scope.listaPublicidadEstadistica[i])[1];
                tabla.data[i]=($scope.listaPublicidadEstadistica[i])[2];
                tabla.rgbabackgroundColor[i]=generatorRgba();
                tabla.rgbaBorderColor[i]=generatorRgba();
            }
            return tabla;
        }
        
         function generatorRgba(){
            var valor="rgba(";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=0.5+")";
            return valor;
        }
        
    }
]);