/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

angular.module('ciApp').factory('tipoPagoFactory', ['$q', '$http', function ($q, $http) {
        return{
          listarTipoPago: listarTipoPago,
          guardarTipoPago: guardarTipoPago,
          eliminarTipoPago: eliminarTipoPago,
          obtenerTipoPago: obtenerTipoPago
        };
         function listarTipoPago() {
            var deferred = $q.defer();
            $http.get('rest/tipoPago/tipoPagosvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los tipos de pagos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarTipoPago(pago) {
            var deferred = $q.defer();
            $http.post('rest/tipoPago/tipoPagosvc/save/', pago).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el tipo de pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
          function eliminarTipoPago(id) {
            var deferred = $q.defer();
            $http.delete('rest/tipoPago/tipoPagosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el tipo de  pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerTipoPago(id) {
            var deferred = $q.defer();
            $http.get('rest/tipoPago/tipoPagosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el tipo de pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
}]);

angular.module('ciApp').controller('tipoPagoController', ['$scope', 'tipoPagoFactory','$location','$routeParams', function ($scope, tipoPagoFactory,$location,$routeParams) {
        $scope.iconReverse="up";
        $scope.order='tpagId';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        listarTipoPago();
 
        if($location.path()==='/editTipoPago/'+$routeParams.tpagId){
            $scope.editar=true;
           
            obtenerTipoPago($routeParams.tpagId);
        }else{
            $scope.editar=false;
        }
 
 
        function listarTipoPago(){
            tipoPagoFactory.listarTipoPago().then(function (lista) {
                $scope.listaTipoPago= lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
            }
            
              $scope.guardarTipoPago=function(){
          $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            tipoPagoFactory.guardarTipoPago($scope.tipoPago).then(function (msj) {
                $('#preload1').removeClass("active");
                $('#preload2').removeClass("active");
                //Materialize.toast('' + msj, 10000);
                swal({
                     title: "\u00A1Guardado!",
                     text: "El tipo de pago " +msj+" con &eacutexito",
                     type: "success",
                     html: true 
                });
                  $location.path('/atributos');  
                 listarTipoPago(); 
            });  
        };
        
            
              $scope.eliminarTipoPago = function (id) {
            swal({
                title: "\u00BF Desea eliminar este tipo de pago?",
                text: "\u00A1 Este registro sera Eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo eliminar!",
                closeOnConfirm: false
                },
                function(){
                tipoPagoFactory.eliminarTipoPago(id).then(function (msj) {
                    Materialize.toast('' + msj, 1000);
                    listarTipoPago();
                }).catch(function (error) {
                    console.error(error);
                });
                swal("Eliminado!", "Su registro seleccionado fue eliminado.", "success");
                    });
        };
        
         function obtenerTipoPago(id) {
            tipoPagoFactory.obtenerTipoPago(id).then(function (data) {
                $scope.tipoPago = data; 
               }).catch(function (error) {
                console.error(error);
            });
        };
 
 
 }]);