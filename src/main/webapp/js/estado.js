/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

angular.module('ciApp').factory('estadoFactory', ['$q', '$http', function ($q, $http) {
        return{
            guardarEstado: guardarEstado,
           obtenerEstado: obtenerEstado,
            listarEstado: listarEstado,
           eliminarEstado: eliminarEstado
            };
            
            
        function guardarEstado(estado) {
            
            var deferred = $q.defer();
            $http.post('rest/estado/estadosvc/save/', estado).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el estado de cita');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerEstado(id) {
            var deferred = $q.defer();
            $http.get('rest/estado/estadosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el estado de la cita');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarEstado() {
            var deferred = $q.defer();
            $http.get('rest/estado/estadosvc/list/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga los estados de citas');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function eliminarEstado(id) {
            var deferred = $q.defer();
            $http.delete('rest/estadoc/estadosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el estado de cita');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
            
}]);

angular.module('ciApp').controller('estadoController', ['$scope','estadoFactory','$location','$routeParams','paginacion','mensaje', function ($scope,estadoFactory,$location,$routeParams,paginacion,mensaje) {
        $scope.iconReverse="up";
        $scope.order='estId';           //para orden de la lista de Estado Civil
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        $scope.listaEstado = [];
        //para mostrar guardado o edicion
        $scope.editar=false;
      
        
         $scope.getData = function () {
         return $filter('filter')($scope.listaEstado, $scope.searchString);
         };
         
          if($location.path()==='/editEstado/'+$routeParams.estId){
            $scope.editar=true;
           
            obtenerEstado($routeParams.estId);
        }else{
            $scope.editar=false;
        }
        
        $scope.listarEstado=function() {
            estadoFactory.listarEstado().then(function (lista) {
                $scope.mostrarEstados=true;
                
                $scope.listaEstado = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        };
        
          $scope.guardarEstado = function () {
             
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            estadoFactory.guardarEstado($scope.estado).then(function (msj) {
                $('#preload1').remove("active");
                $('#preload2').remove("active");
                //Materialize.toast('' + msj, 10000);
                mensaje.resultado("Estado de cita",msj);
                 $location.path('/atributos');  
            }).catch(function (error){
                mensaje.resultado("\u00A1Error!","error"+error,error);
            });
        };
        
         function obtenerEstado(id) {
            estadoFactory.obtenerEstado(id).then(function (data) {
                
                $scope.estado = data; 
               }).catch(function (error) {
                console.error(error);
            });
        };
        
 }]);

