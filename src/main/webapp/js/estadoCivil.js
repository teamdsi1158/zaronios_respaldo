'use strict';

angular.module('ciApp').factory('estadoCivilFactory', ['$q', '$http', function ($q, $http) {
        return{
             guardarEstadoCivil: guardarEstadoCivil,
           obtenerEstadoCivil: obtenerEstadoCivil,
            listarEstadoPaciente: listarEstadoPaciente,
           eliminarEstadoCivil: eliminarEstadoCivil
        };
        
        function guardarEstadoCivil(estadoCivil) {
            
            var deferred = $q.defer();
            $http.post('rest/estadoc/estadocsvc/save/', estadoCivil).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el estado civil');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerEstadoCivil(id) {
            var deferred = $q.defer();
            $http.get('rest/estadoc/estadocsvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el estado civil');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarEstadoPaciente() {
            var deferred = $q.defer();
            $http.get('rest/estadoc/estadocsvc/statistics/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las estadisticas de los pacientes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function eliminarEstadoCivil(id) {
            var deferred = $q.defer();
            $http.delete('rest/estadoc/estadocsvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el estado civil');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
}]);

angular.module('ciApp').controller('estadoCivilController', ['$scope','estadoCivilFactory','pacienteFactory','$location','$routeParams','$filter','mensaje', function ($scope,estadoCivilFactory,pacienteFactory,$location,$routeParams,$filter,mensaje) {
        $scope.iconReverse="up";
        $scope.order='estId';           //para orden de la lista de Estado Civil
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        $scope.listaEstadoCivil = [];
       //para mostrar guardado o edicion
        $scope.editar=false;
       
        
        
        $scope.getData = function () {
         return $filter('filter')($scope.listaEstadoCivil, $scope.searchString);
         };
        
         
         if($location.path()==='/editEstadoCivil/'+$routeParams.estId){
            $scope.editar=true;
           
            obtenerEstadoCivil($routeParams.estId);
        }else{
            $scope.editar=false;
        }
        
        $scope.listarEstadoCivil=function() {
            pacienteFactory.listaEstadosCivil().then(function (lista) {
                $scope.mostrarEstados=true;
                
                $scope.listaEstadoCivil = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        };
        
        
         $scope.guardarEstadoCivil = function () {
             
            $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            estadoCivilFactory.guardarEstadoCivil($scope.estadoCivil).then(function (msj) {
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                //Materialize.toast('' + msj, 10000);
                 mensaje.resultado("Estado Civil",msj);
                 $location.path('/atributos');  
            });
        };
        
        $scope.eliminarEstadoCivil = function (id) {
            swal({
                title: "\u00BF Desea eliminar este estado civil?",
                text: "\u00A1 Este registro sera Eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo eliminar!",
                closeOnConfirm: false
                },
                function(){
                estadoCivilFactory. eliminarEstadoCivil(id).then(function (msj) {
                    mensaje.resultado(" Estado Civil",msj);
                    $scope.listarEstadoCivil();
                }).catch(function (error) {
                    console.error(error);
                });
                
                    });
        };
        
        function obtenerEstadoCivil(id) {
            estadoCivilFactory.obtenerEstadoCivil(id).then(function (data) {
                
                $scope.estadoCivil = data; 
               }).catch(function (error) {
                console.error(error);
            });
        };
        
         $scope.listarEstadoPacientes=function() {
            
            estadoCivilFactory.listarEstadoPaciente().then(function (lista) {
                
                $scope.listaEstadoEstadistica = lista;
                  
            }).catch(function (error) {
                console.error(error);
            });
        }
       
        $scope.mostrarGrafico=function(b){
         
            $scope.grafico=b;
            $scope.label="N de personas por estado civil";
             var tabla=ordenarTabla();
             
        // Get the context of the canvas element we want to select
        var countries= document.getElementById("estCiviles").getContext("2d");
      $scope.myChart = new Chart(countries, {
    type: 'pie',
    data: {
        
        datasets: [{
            label: $scope.label,
            data: tabla.data,
            backgroundColor: tabla.rgbabackgroundColor,
            //borderColor: tabla.rgbabackgroundColor,
            //rgbaBorderColor
            borderWidth: 1
        }],
    labels: tabla.label
    },
    options: {
            responsive: true
        }
});
        };
        function ordenarTabla(){
            
           
            var tabla ={
                data: [],
                label: [],
                rgbabackgroundColor: [],
                rgbaBorderColor: []
            };
            for(var i=0;i<$scope.listaEstadoEstadistica.length;i++){
                tabla.label[i]=($scope.listaEstadoEstadistica[i])[1];
                tabla.data[i]=($scope.listaEstadoEstadistica[i])[2];
                tabla.rgbabackgroundColor[i]=generatorRgba();
                tabla.rgbaBorderColor[i]=generatorRgba();
            }
            return tabla;
        }
        
        $scope.moverTablaEstados=function(){
            $scope.mostrarEstados=false;
            
        };
        
         function generatorRgba(){
            var valor="rgba(";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=0.5+")";
            return valor;
        }

}]);