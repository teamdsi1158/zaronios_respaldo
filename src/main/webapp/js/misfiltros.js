angular.module('ciApp').filter('filtro_cambio', function () {
    return function (input) {
        return input ? 'si' : 'no';
    };
});

angular.module('ciApp').filter('filtro_entrada_cat', function () {
    return function (input) {
        return input ? '1' : '0';
    };
});

angular.module('ciApp').filter('filtro_limitada', function () {
    return function (input) {
        var nombre = "";
        if (input === 1) {
            nombre = "LIMITADA";
        } else {
            nombre = "NO LIMITADA";
        }
        return nombre;
    };
});
