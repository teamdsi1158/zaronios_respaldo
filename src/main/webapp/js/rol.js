'use strict';

angular.module('ciApp').factory('rolFactory', ['$q', '$http', function ($q, $http) {
        return {
           listarRol: listarRol,
           guardarRol: guardarRol,
           darBajaRol: darBajaRol,
           obtenerRol: obtenerRol
        };
        
        function listarRol() {
            var deferred = $q.defer();
            $http.get('rest/rol/rolsvc/list/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga los roles');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function guardarRol(rol) {
            var deferred = $q.defer();
            
            $http.post('rest/rol/rolsvc/save/', rol).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el rol');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function darBajaRol(id){
            var deferred= $q.defer();
            $http.delete('rest/rol/rolsvc/del/'+id+'/').then(function(response){
                deferred.resolve(response.data);
            }, function(errResponse){
                console.error('Error mientras daba de baja el rol');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        function obtenerRol(id) {
            var deferred = $q.defer();
            $http.get('rest/rol/rolsvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el rol');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
}]);

angular.module('ciApp').controller('rolController', ['$scope', 'rolFactory','menuFactory','$location','$routeParams','$filter','mensaje', function ($scope, rolFactory,menuFactory,$location,$routeParams,$filter,mensaje) {
        $scope.iconReverse="up";
        $scope.order='rolId';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        $scope.listarol = [];
        $scope.menus=[];
        $scope.listaMenu=[];
       //para mostrar guardado o edicion
        $scope.editar=false;
 
        $scope.getData = function () {
         return $filter('filter')($scope.listaRol, $scope.searchString);
         };
         
    
         //Fucion para ordenar tabla dando click en el titulo de la columna de la tabla
          $scope.orderBy=function(order){
          $scope.order=order;
          $scope.reverse=!$scope.reverse;
          if($scope.reverse){
              $scope.iconReverse="down";
          }else{
              $scope.iconReverse="up";
          }
        };
        
         if($location.path()!=='/addRol' && $location.path()!=='/editRol/'+$routeParams.rol){ 
             
        listarRol();
            }else{
             listarMenuConRol($routeParams.rol); 
            }
        if($location.path()==='/editRol/'+$routeParams.rol){
            $scope.editar=true;
           
           
        }else{
            listarMenu();
            $scope.editar=false;
        }
        
        $scope.mostrarMenu=function(it){
         
            var j;
             for(j=0;j<$scope.menus.length;j++){
                        if(it.menId===$scope.menus[j].menId){
                             
                            
                        it.checked=true;
                    j=$scope.menus.length;
                }else{
                    it.checked=false;
                }}
              
        }
        
        function listarMenuConRol(id) {
            menuFactory.listarMenu().then(function (lista) {
                
                 rolFactory.obtenerRol(id).then(function (data) {
                
                $scope.rol = data;
                $scope.menus=$scope.rol.menuSet;
                var i;
                var j;
                  $scope.listaMenu = lista;
                 
               }).catch(function (error) {
                console.error(error);
            });
        
              
            }).catch(function (error) {
                console.error(error);
            });
            
        }
        function listarMenu() {
            menuFactory.listarMenu().then(function (lista) {
                
                $scope.listaMenu = lista;
            }).catch(function (error) {
                console.error(error);
            });
            
        }
        
        function listarRol() {
            rolFactory.listarRol().then(function (lista) {
                
                $scope.listaRol = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);
                 $scope.loaderVar=false; //Variable para el loader
            
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        }
        
         $scope.eliminarRol = function (id) {
            swal({
                title: "\u00BF Desea deshabilitar este rol?",
                text: "\u00A1 Este registro sera deshabilitado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo deshabilitar!",
                closeOnConfirm: false
                },
                function(){
                rolFactory.darBajaRol(id).then(function (msj) {
                    mensaje.resultado("Rol",msj);
                    listarRol();
                }).catch(function (error) {
                    console.error(error);
                });
               
                    });
        };
        
         $scope.guardarRol = function(){
            $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            $scope.rol.menuSet=$scope.menus;
             rolFactory.guardarRol($scope.rol).then(function (msj) {
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                 mensaje.resultado(" Rol",msj);
                  $location.path('/roles'); 
             }).catch(function(error){
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                console.error(error);
                $location.path('/roles'); 
             });
        };
        
        $scope.addMenu=function(menu){
       
            var idx = $scope.menus.indexOf(menu);
            
            if (idx > -1 || menu.checked==true) {
                $scope.menus.splice(idx, 1);
                menu.checked=false;
                }else{
                    
            $scope.menus.push(menu);
        
            }
        
        };
        
     
}])
.filter('searchFor', function(){
         return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});