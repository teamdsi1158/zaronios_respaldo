/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';

angular.module('ciApp').factory('bitacoraFactory', ['$q', '$http', function ($q, $http) {
        return{
            listarBitacoraPaciente: listarBitacoraPaciente,
            listarBitacoraPagoTratamiento: listarBitacoraPagoTratamiento
        };
        function listarBitacoraPaciente(idPac) {
            var deferred = $q.defer();
            $http.get('rest/bitacora/bitacorasvc/pacientes/'+idPac+"/").then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la bitacora de el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarBitacoraPagoTratamiento(idpag) {
            var deferred = $q.defer();
            $http.get('rest/bitacora/bitacorasvc/pago/'+idpag+"/").then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la bitacora de el ´pago');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
}
]);

angular.module('ciApp').controller('bitacoraController', ['$scope','bitacoraFactory','$location','$routeParams','paginacion','mensaje',function ($scope,bitacoraFactory,$location,$routeParams,paginacion,mensaje) {
        $scope.iconReverse="up";
        $scope.order='segDia';           //para orden de la lista de Estado Civil
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=20;   //Cantidad de Items por pagina
        $scope.paginas=1; 
        $scope.listaSeguimiento=[];
        $scope.bitacora;
       
        
        $scope.getData = function () {
         return $filter('filter')($scope.listaSeguimiento, $scope.searchString);
         };
        
        
        $scope.listarBitacoraPacientes=function() {
            bitacoraFactory.listarBitacoraPaciente($routeParams.pacId).then(function (bitacora) {
                
                $scope.bitacora=bitacora;
                $scope.listaSeguimiento = bitacora[0].seguimientoSet;
                 $scope.paginas=Math.ceil(bitacora.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        };
        
         $scope.listarBitacoraPagoTratamiento=function() {
            bitacoraFactory.listarBitacoraPagoTratamiento($routeParams.pagId).then(function (bitacora) {
                
                $scope.bitacora=bitacora;
                $scope.listaSeguimiento = bitacora[0].seguimientoSet;
                 $scope.paginas=Math.ceil(bitacora.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        };
        
        $scope.orderBy=function(order){
          $scope.order=order;
          $scope.reverse=!$scope.reverse;
          if($scope.reverse){
              $scope.iconReverse="down";
          }else{
              $scope.iconReverse="up";
          }
        };
}
]);

