/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

'use strict';

angular.module('ciApp').factory('sesionFactory', ['$q', '$http', function ($q, $http) {
        return{
          listarSesion: listarSesion,
          guardarSesion: guardarSesion,
          obtenerSesion: obtenerSesion,
          obtenerFotos: obtenerFotos,
          obtenerExamen: obtenerExamen,
          darBajaSesion: darBajaSesion,
          guardarImagen: guardarImagen,
          guardarExamen: guardarExamen,
          guardarImagenPaciente: guardarImagenPaciente
          
        };
         function listarSesion(asitId) {
            var deferred = $q.defer();
            $http.get('rest/sesion/sesionsvc/list/'+asitId+'/').then(function (response) {
                deferred.resolve(response.data);
                 
            }, function (errResponse) {
                console.error('Error mientras carga las sesiones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarSesion(sesion) {
            var deferred = $q.defer();
            
            $http.post('rest/sesion/sesionsvc/save/', sesion).then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras guardaba la sesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function obtenerSesion(id) {
            var deferred = $q.defer();
            $http.get('rest/sesion/sesionsvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la sesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerFotos(sesiId) {
            var deferred = $q.defer();
            $http.get('rest/imagePaciente/imagenPacientesvc/list/' + sesiId + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga las fotos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function obtenerExamen(sesiId) {
            var deferred = $q.defer();
            $http.get('rest/examen/examensvc/' + sesiId + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los examenes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function darBajaSesion(id) {
            var deferred = $q.defer();
            $http.get('rest/sesion/sesionsvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja la sesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarImagen(imagen){
             var deferred = $q.defer();
            
            $http.post('rest/image/imagensvc/save/', imagen).then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se guardaba la imagen');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarExamen(examen){
             var deferred = $q.defer();
            
            $http.post('rest/examen/examensvc/save/', examen).then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se guardaba el examen');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function guardarImagenPaciente(imagenP){
             var deferred = $q.defer();
            guardarImagen(imagenP.imagen).then(function(imagen){
                
                imagenP.imgId=imagen.imgId;
                
                delete imagenP.imagen;
                 $http.post('rest/imagePaciente/imagenPacientesvc/save/', imagenP).then(function (response) {
                    deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se guardaba la imagen');
                deferred.reject(errResponse);
            });
            return deferred.promise;
            });
           
               
        }
        
}]);

angular.module('ciApp').controller('sesionController', ['$scope', 'sesionFactory','expedienteFactory','tipoSignoVitalFactory','citaFactory','estadoFactory','$location','$routeParams','$filter','paginacion','mensaje', function ($scope, sesionFactory,expedienteFactory,tipoSignoVitalFactory,citaFactory,estadoFactory,$location,$routeParams,$filter,paginacion,mensaje) {
 $scope.CurrentDate = new Date(); //Para capturar la fecha actual
 $scope.restriccion='\u005Cd{1,5}\u005C.\u005Cd{1,2}';
 $scope.asignacion={};
 obtenerTipoSignoVital();
 obtenerPaciente();
 listarEstado();
 obtenerAsignacion();
        $scope.pilaSignoVital=[];
 $scope.signoVital={
     sesiId: null,
      dSivId:null
 };
$scope.cita={
    
};
$scope.sesion={
    sesiId: null
}

 $scope.pilaImagenes=[];
 $scope.pilaExamenes=[];
        
       if($location.path()=='/editSesion/'+$routeParams.asitId+"/"+$routeParams.pacId+"/"+$routeParams.sesiId ){  
       
            obtenerSesion($routeParams.sesiId);
    } 
 
 //Para guardar una nueva sesion
 $scope.guardarSesion = function(){
     if($scope.cita.estId==null){
         Materialize.toast('' + "Debe ingresar el estado de la sesion", 10000);
         return 0;
     }
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            delete $scope.cita.citaDia;
            delete $scope.cita.citaHora;
            $scope.cita.estId=4;
            
            
     citaFactory.guardarCita($scope.cita).then(function (idCita){
     if($scope.sesion.sesiId==null){
            $scope.sesion.sesiFechaFin=new Date();
            $scope.sesion.empId=14; /////////////////////Registrarlo con el usuario
            $scope.sesion.asitId=$scope.asignacion.asitId;
            $scope.sesion.citaId=idCita;
     }
     
           $scope.sesion.citaId=idCita;
           $scope.sesion.empId=14;
             sesionFactory.guardarSesion($scope.sesion).then(function (id) {
                
                for(var i=0; i<$scope.pilaSignoVital.length; i++){
                    ($scope.pilaSignoVital)[i].sesiId=id;
                tipoSignoVitalFactory.guardarSignoVital($scope.pilaSignoVital[i]).then(function(msj){
                     mensaje.resultado("Signo Vital:",msj);
                     
                });
            }
              $scope.obtenerImagenes(id);
              $scope.obtenerExamenes(id);
            
              for(var j=0;j<$scope.pilaImagenes.length;j++){
                  sesionFactory.guardarImagenPaciente($scope.pilaImagenes[j]);
              }
              
              for(var j=0;j<$scope.pilaExamenes.length;j++){
                  sesionFactory.guardarExamen($scope.pilaExamenes[j]).then(function(msj){
                       mensaje.resultado("Examen:",msj);
                  });
              }
                  $location.path('/expediente/'+$routeParams.pacId); 
             }).catch(function(error){
                $('#preload1').removeClass("active");
                $('#preload2').removeClass("active");
                console.error(error);
                $location.path('/expediente/'+$routeParams.pacId);
             });
             });
        };
        
        
        function obtenerAsignacion(){
         expedienteFactory.obtenerAsignacion($routeParams.asitId).then(function(lista){
             
            $scope.asignacion=lista; 
            
         });
        }
        
         function obtenerPaciente(){
         expedienteFactory.obtenerExpediente($routeParams.pacId).then(function(expediente){
             
            $scope.paciente=expediente; 
            
         });
        }
        
        function obtenerTipoSignoVital(){
         tipoSignoVitalFactory.listarTipoSignoVital().then(function(lista){
             
            $scope.listaTipoSignoVital=lista; 
             setTimeout(function () {
            $scope.$apply(function () {
             $(document).ready(function() {
    $('select').material_select();
                    });
             }); 
              }, 2000);
         });
        }
        
         function obtenerSesion(id){
         sesionFactory.obtenerSesion(id).then(function(data){
             
            $scope.sesion=data; 
             setTimeout(function () {
                  $scope.$apply(function () {
 $(document).ready(function(){
      $('.slider').slider();
    });
                  });
   }, 3000);
   
            $scope.pilaSignoVital=$scope.sesion.signoVitalList;
            $scope.cita=data.cita;
             setTimeout(function () {
            $scope.$apply(function () {
             $(document).ready(function() {
    $('select').material_select();
                    });
                     $(document).ready(function(){
      $('.least-gallery').least();
      
     });
     $('label').addClass("active");
             }); 
              }, 2000);
         });
        }
        
        function listarEstado(){
         estadoFactory.listarEstado().then(function(lista){
             
            $scope.listaEstado=lista; 
             $(document).ready(function() {
    $('select').material_select();
  }); 
         });
        }
        
        // Para agregar un nuevo signo vital a la sesión
        $scope.agregarSignoVital=function(){
            if($scope.signoVital==null){
                 Materialize.toast('Debe seleccionar al menos un signo vital' , 1000);
                return 0;
            }
            $scope.pilaSignoVital.push({"tipoSignoVital":$scope.signoVital,dsivId: $scope.signoVital.dsivId});
            $scope.signoVital={
                sesiId:null,
                dSivId:null
            };
           
            
            
           $(document).ready(function() {
    $('input#valor,input#descripcion,textarea#procedimientos').characterCounter();
  });
        };
        
        $scope.quitarSignoVital=function(it){
            if($scope.pilaSignoVital.length==0 || it.siviValor=="" || it.siviValor!=null){
                 Materialize.toast('Escribió un valor , debe borrarlo si desea eliminar' , 3000);
                return 0;
            }
         var index=$scope.pilaSignoVital.indexOf(it);
         if(index>=0){
             $scope.pilaSignoVital.splice(index,1);
         }
        };
        
//  Obtiene la lista de Fotos agregadas a la sesión        
      $scope.obtenerImagenes=function(id){
          var imagenes=$("#imagenesPre li").length;
          for (var i=$scope.pilaImagenes.length;i<imagenes;i++){
          var img=$("[qq-file-id="+i+"] img").attr("src");
          var descripcion=$("[qq-file-id="+i+"] #descripcion input").val();
          var imagen={data: img};
          $scope.pilaImagenes.push({"imagen":imagen,impDescripcion: descripcion,sesiId: id,imgId: null});
          }
      };
//  Obtiene la lista de Examenes agregadas a la sesión      
      $scope.obtenerExamenes=function(id){
          var examenes=$("#examenesPre li").length;
          for (var i=$scope.pilaExamenes.length;i<examenes;i++){
          var img=$("#examenesPre [qq-file-id="+i+"] img").attr("src");
          var descripcion=$("#examenesPre [qq-file-id="+i+"] #descripcion input").val();
          
          
          $scope.pilaExamenes.push({"exaObservacion": descripcion,"sesiId": id,"data": img});
        }
      };
//  Itera para el guardado de Fotografias agregadas      
      $scope.guardarFotos=function(){
           $scope.obtenerImagenes($scope.sesion.sesiId);
          if($scope.pilaImagenes.length==0){
              return 0;
          }
          for(var j=0;j<$scope.pilaImagenes.length;j++){
               var data=$scope.pilaImagenes[j];
                  sesionFactory.guardarImagenPaciente(data);
              }
          sesionFactory.obtenerFotos($scope.sesion.sesiId).then(function(lista){
              
                      
              $scope.sesion.imagenPacienteList=lista;
                     
          });
      };
      
 //  Itera para el guardado de Examenes agregadas      
     $scope.guardarExamenes=function(){
           $scope.obtenerExamenes($scope.sesion.sesiId);
           
          if($scope.pilaExamenes.length==0){
              return 0;
          }
          for(var j=0;j<$scope.pilaExamenes.length;j++){
               var data=$scope.pilaExamenes[j];
                  sesionFactory.guardarExamen(data);
              }
          sesionFactory.obtenerExamen($scope.sesion.sesiId).then(function(lista){
              
                      
              $scope.sesion.examenPacienteList=lista;
                     
          });
      };
    
      
 }]);