/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';


angular.module('ciApp').factory('accessTokenFactory', ['$q', '$http', function ($q, $http) {
        return{
            listarAccessToken: listarAccessToken
        };

        function listarAccessToken() {
            var deferred = $q.defer();
            $http.get('rest/accessToken/accessTokensvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga las sesiones');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    }
]);

angular.module('ciApp').controller('accessTokenController', ['$scope', 'accessTokenFactory', '$location', '$routeParams', '$filter', '$http', function ($scope, accessTokenFactory, $location, $routeParams, $filter, $http) {
        $scope.iconReverse = "up";
        $scope.order = '';           //para orden de la lista de publicidad
        $scope.reverse = false;
        $scope.searchString = '';
        $scope.numItems = 50;   //Cantidad de Items por pagina
        $scope.paginas = 1;
        //para mostrar guardado o edicion
        $scope.editar = false;
        $scope.listaAccessToken = [];

        $scope.getData = function () {
            return $filter('filter')($scope.listaAccessToken, $scope.searchString);
        };

        listarAccessToken();
        function listarAccessToken() {
            accessTokenFactory.listarAccessToken().then(function (data) {
                console.log(data);
                $scope.listaAccessToken = data;
                $scope.paginas = Math.ceil(data.length / $scope.numItems);
            }).catch(function (error) {
                console.log(error);
            });
        }

        $scope.orderBy = function (order) {
            $scope.order = order;
            $scope.reverse = !$scope.reverse;
            if ($scope.reverse) {
                $scope.iconReverse = "down";
            } else {
                $scope.iconReverse = "up";
            }
        };

        $scope.generarReporteAccess = function () {
            $http.get('rest/reportsvc/report_accesos/', {responseType: 'arraybuffer'}).then(function (response) {
                var archivo = new Blob([response.data], {type: 'application/pdf'});
                var isChrome = !!window.chrome && !!window.chrome.webstore;
                if (isChrome) {
                    var url = window.URL || window.webkitURL;
                    var fileURLi = URL.createObjectURL(archivo);
                    window.open(fileURLi, "_blank");
                } else {
                    var fileURL = URL.createObjectURL(archivo);
                    window.open(fileURL);
                }
                msjAviso("Documento generado con Exito", "success");
            }).catch(function (error) {
                console.error(error);
            });
        };

        function msjAviso(msj, tipo) {
            swal({
                title: "Aviso",
                text: msj,
                type: tipo,
                showConfirmButton: false,
                html: true,
                timer: 2000
            });
        }

    }
]);

