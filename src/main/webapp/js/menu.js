'use strict';

angular.module('ciApp').factory('menuFactory', ['$q', '$http', function ($q, $http) {

        return {
            listarMenu: listarMenu,
            obtenerMenu: obtenerMenu,
            listarMenuConfig: listarMenuConfig
        };

        function listarMenu() {
            var deferred = $q.defer();
            $http.get('rest/menu/menusvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los menus');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function obtenerMenu(id) {
            var deferred = $q.defer();
            $http.get('rest/menu/menusvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el menú');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listarMenuConfig() {
            var deferred = $q.defer();
            $http.get('rest/menuconf/menusvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el menú');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    }
]);

angular.module('ciApp').controller('MenuController', ['$scope', '$rootScope', '$http', 'menuFactory', '$location', '$routeParams', '$filter', function ($scope, $rootScope, $http, menuFactory, $location, $routeParams, $filter) {

        $scope.listaMenu = [];
        $scope.bh = false;
        $scope.userData = {};

        $scope.menuitem = {menId: null, menNombre: null, menRuta: null, menIcono: null, orden: null, nivel: null, menIdPadre: null, hijos: []};

        setTimeout(function(){traerUsuario();},1000);

        function traerUsuario() {
            var usuario = sessionStorage.getItem("user");
            if (usuario !== null) {
                var usuarioJSON = {"username": usuario};
                $http.post('rest/menuconf/menusvc/usuarios/', usuarioJSON).then(function (user) {
                    $scope.userData = user.data;
                    listarMenuConfig();
                }).catch(function (error) {
                    console.error(error);
                });
            }
        }

        function listarMenuConfig() {
            menuFactory.listarMenuConfig().then(function (d) {
                var data = d;
                var padres = [];
                var hijos = [];
                var usuarioRoles = [];

                usuarioRoles = $scope.userData.rol.menuSet;

                for (var t in data) {
                    
                    for (var w in usuarioRoles) {
                        if (data[t].nivel === 1) {
                            if (data[t].menId === usuarioRoles[w].menId) {
                                padres.push(data[t]);
                                $rootScope.permisos.push(data[t].permisoList);
                            }
                        } else if (data[t].nivel === 2) {
                            if (data[t].menId === usuarioRoles[w].menId) {
                                hijos.push(data[t]);
                                $rootScope.permisos.push(data[t].permisoList);
                            }
                        }
                    }
                }

                for (var it in padres) {
                    $scope.menuitem = {
                        menId: padres[it].menId,
                        menNombre: padres[it].menNombre,
                        menRuta: padres[it].menRuta,
                        menIcono: padres[it].menIcono,
                        orden: padres[it].orden,
                        nivel: padres[it].nivel,
                        menIdPadre: padres[it].menIdPadre,
                        hijos: []
                    };
                    $scope.listaMenu.push($scope.menuitem);
                }

                for (var xv in $scope.listaMenu) {
                    for (var wx in hijos) {
                        if ($scope.listaMenu[xv].menId === hijos[wx].menIdPadre) {
                            if($scope.bh===false){
                                $scope.bh=true;
                            }
                            console.log("hijo");
                            $scope.menuitem = {
                                menId: hijos[wx].menId,
                                menNombre: hijos[wx].menNombre,
                                menRuta: hijos[wx].menRuta,
                                menIcono: hijos[wx].menIcono,
                                orden: hijos[wx].orden,
                                nivel: hijos[wx].nivel,
                                menIdPadre: hijos[wx].menIdPadre,
                                hijos: []
                            };
                            $scope.listaMenu[xv].hijos.push($scope.menuitem);
                        }
                    }
                }
                padres.forEach(function(menu){
                    menu.permisoList.forEach(function(permiso){
                        $rootScope.permisos.push(permiso);
                    });
                });
                hijos.forEach(function(menu){
                    menu.permisoList.forEach(function(permiso){
                        $rootScope.permisos.push(permiso); 
                    });
                });
                
                
            }).catch(function (error) {
                console.error(error);
            });
        }

    }
]);
