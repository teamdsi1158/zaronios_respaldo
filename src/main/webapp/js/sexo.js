'use strict';

angular.module('ciApp').factory('sexoFactory', ['$q', '$http', function ($q, $http) {
    return {
           listarSexo: listarSexo,
           listarSexoPaciente: listarSexoPaciente,
           guardarSexo: guardarSexo,
           obtenerSexo: obtenerSexo,
           eliminarSexo: eliminarSexo
    };
    
    function listarSexo() {
            var deferred = $q.defer();
            $http.get('rest/sexo/sexosvc/list/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga los sexos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
    function listarSexoPaciente() {
            var deferred = $q.defer();
            $http.get('rest/sexo/sexosvc/statistics/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras carga las estadisticas de los pacientes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function guardarSexo(sexo) {
             $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            var deferred = $q.defer();
            $http.post('rest/sexo/sexosvc/save/', sexo).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el genero');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function obtenerSexo(id) {
            var deferred = $q.defer();
            $http.get('rest/sexo/sexosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el sexo');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function eliminarSexo(id) {
            var deferred = $q.defer();
            $http.delete('rest/sexo/sexosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba el sexo');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    
}]);

angular.module('ciApp').controller('sexoController', ['$scope','sexoFactory','$location','$routeParams','$filter','mensaje', function ($scope,sexoFactory,$location,$routeParams,$filter,mensaje) {
        $scope.iconReverse="up";
        $scope.order='sexId';           //para orden de la lista de publicidad
        $scope.reverse=false;
        $scope.searchString='';
        $scope.numItems=5;   //Cantidad de Items por pagina
        $scope.paginas=1;
        $scope.listaSexo = [];
       //para mostrar guardado o edicion
        $scope.editar=false;
        
        
        
        
         $scope.getData = function () {
         return $filter('filter')($scope.listaSexo, $scope.searchString);
         };
         
           if($location.path()==='/editSexo/'+$routeParams.sexId){
            $scope.editar=true;
           
            obtenerSexo($routeParams.sexId);
        }else{
            $scope.editar=false;
        }
        
        $scope.listarSexo=function() {
        
            sexoFactory.listarSexo().then(function (lista) {
                $scope.listaSexo = lista;
                 $scope.paginas=Math.ceil(lista.length/$scope.numItems);  
            }).catch(function (error) {
                console.error(error);
            });
        };
        
        $scope.listarSexoPacientes=function() {
            sexoFactory.listarSexoPaciente().then(function (lista) {
                $scope.listaSexoEstadistica = lista;
            }).catch(function (error) {
                console.error(error);
            });
        };
        
         $scope.guardarSexo = function () {
            $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            sexoFactory.guardarSexo($scope.sexo).then(function (msj) {
                $('#preloadPubli1').remove("active");
                $('#preloadPubli2').remove("active");
                mensaje.resultado("Género",msj);
                 $location.path('/atributos');  
            });
        };
        
         $scope.eliminarSexo = function (id) {
            swal({
                title: "\u00BF Desea eliminar este genero?",
                text: "\u00A1 Este registro sera Eliminado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo eliminar!",
                closeOnConfirm: false
                },
                function(){
                sexoFactory. eliminarSexo(id).then(function (msj) {
                   mensaje.resultado(" Genero",msj);
                    $scope.listarSexo();
                }).catch(function (error) {
                    console.error(error);
                });
               
                    });
        };
        
         // Obtiene el sexo para poder editarlo
        function obtenerSexo(id) {
            sexoFactory.obtenerSexo(id).then(function (data) {
                $scope.sexo = data; 
               }).catch(function (error) {
                console.error(error);
            });
        };
        
        $scope.mostrarGrafico=function(b){
            $scope.grafico=b;
            $scope.label="N de personas por genero";
             var tabla=ordenarTabla();
        // Get the context of the canvas element we want to select
        var countries= document.getElementById("sexos").getContext("2d");
        
      $scope.myChart = new Chart(countries, {
    type: $scope.tipoGrafico,
    data: {
        datasets: [{
            label: $scope.label,
            data: tabla.data,
            backgroundColor: tabla.rgbabackgroundColor,
            //borderColor: tabla.rgbaBorderColor,
            borderWidth: 1
        }],
    labels: tabla.label
    },
    options: {
            responsive: true,
             legend: {
            display: true,
            labels: {
                fontColor: 'rgb(255, 99, 132)'
            }
        },
         title: {
            display: true,
            text: 'Generos'
        }
       
        }
});
        };
        
        function ordenarTabla(){
            var tabla ={
                data: [],
                label: [],
                rgbabackgroundColor: [],
                rgbaBorderColor: []
            };
            for(var i=0;i<$scope.listaSexoEstadistica.length;i++){
                tabla.label[i]=($scope.listaSexoEstadistica[i])[1];
                tabla.data[i]=($scope.listaSexoEstadistica[i])[2];
                tabla.rgbabackgroundColor[i]=generatorRgba();
                tabla.rgbaBorderColor[i]=generatorRgba();
            }
            return tabla;
        }
        
        $scope.mostrarSexo = function(b){
            $scope.mostrarSexos=b;
        };
        
        $scope.cambiarGrafico=function(){
        
       if($scope.tipoGrafico==="pie"){
            $scope.$apply(
           $scope.tipoGrafico="bar"    
           );
           
       }else{
           $scope.$apply(
           $scope.tipoGrafico="pie"  );
           
       }
       $scope.$apply(
           $scope.myChart
           );
         
      
        };
        
        
        function generatorRgba(){
            var valor="rgba(";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=Math.ceil(Math.random() * (255 - 1) + 1)+",";
            valor+=0.5+")";
            return valor;
        }
    
    }]);