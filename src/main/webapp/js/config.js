var exampleAppConfig = {
    /* When set to false a query parameter is used to pass on the access token.
     * This might be desirable if headers don't work correctly in some
     * environments and is still secure when using https. */
    useAccessTokenHeader: true,
    debug: true
};

$(document).ready(function () {

    $(".button-collapse").sideNav({
        menuWidth: 300
    });

    $('.modal-trigger').leanModal();

    $('.datepicker').pickadate({
        selectMonths: true,
        selectYears: 15
    });

    $('select').material_select();
});