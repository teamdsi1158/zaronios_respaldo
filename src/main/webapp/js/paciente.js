'use strict';

angular.module('ciApp').factory('pacienteFactory', ['$q', '$http', function ($q, $http) {

        return {
            listaPacientes: listaPacientes,
            listaPacientesInactivos: listaPacientesInactivos,
            guardarPaciente: guardarPaciente,
            darBajaPaciente: darBajaPaciente,
            eliminarPaciente: eliminarPaciente,
            activarPaciente: activarPaciente,
            obtenerPaciente: obtenerPaciente,
            listarPais: listarPais,
            listaMunicipios: listaMunicipios,
            listaDepartamentos: listaDepartamentos,
            listaPublicidad: listaPublicidad,
            listaSangre: listaSangre,
            listaEstadosCivil: listaEstadosCivil,
            listaSexos: listaSexos,
            guardarDocumento: guardarDocumento,
            guardarProfesion: guardarProfesion,
            guardarPersona: guardarPersona,
            guardarContacto: guardarContacto
        };

        function listaPacientes() {
            var deferred = $q.defer();
            $http.get('rest/paciente/pacientesvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los pacientes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

                function listaPacientesInactivos() {
            var deferred = $q.defer();
            
            $http.get('rest/paciente/pacientesvc/listDesactivated/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los pacientes');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaSexos() {
            var deferred = $q.defer();
            $http.get('rest/sexo/sexosvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los sexos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaMunicipios(depCodigo) {
            var deferred = $q.defer();
            $http.get('rest/municipio/municipiosvc/list/' + depCodigo + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los municipios');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaEstadosCivil() {
            var deferred = $q.defer();
            $http.get('rest/estadoc/estadocsvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los estados civiles');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function listarPais() {
            var deferred = $q.defer();
            $http.get('rest/pais/paissvc/list/').then(function (response) {
                
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los paises');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaDepartamentos() {
            var deferred = $q.defer();
            $http.get('rest/departamento/departamentosvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los departamentos');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaPublicidad() {
            var deferred = $q.defer();
            $http.get('rest/publicidad/publicidadsvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los publibidad');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function listaSangre() {
            var deferred = $q.defer();
            $http.get('rest/tiposan/tiposansvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga los tipos de sangre');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarPaciente(paciente) {
              
            var deferred = $q.defer();
            $http.post('rest/paciente/pacientesvc/save/', paciente).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarProfesion(profesion) {
            var deferred = $q.defer();
            $http.post('rest/profesion/profesionsvc/save/', profesion).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba la profesion');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarPersona(persona) {
          
            var deferred = $q.defer();
            
            $http.post('rest/persona/personasvc/save/', persona).then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras guardaba la informacion de la persona');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarDocumento(documento) {
            var deferred = $q.defer();
            $http.post('rest/documento/documentosvc/save/', documento).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el Documento');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarContacto(contacto) {
            var deferred = $q.defer();
            $http.post('rest/contacto/contactosvc/save/', contacto).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el contacto');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }


        function darBajaPaciente(id) {
            var deferred = $q.defer();
            $http.get('rest/paciente/pacientesvc/down/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se daba de baja a el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        function eliminarPaciente(id) {
            var deferred = $q.defer();
            $http.delete('rest/paciente/pacientesvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se eliminaba el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
         function activarPaciente(id) {
            var deferred = $q.defer();
            $http.get('rest/paciente/pacientesvc/up/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras se eliminaba el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function obtenerPaciente(id) {
            var deferred = $q.defer();
            $http.get('rest/paciente/pacientesvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
                
            }, function (errResponse) {
                console.error('Error mientras se obtenia el paciente');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
    }
]);

angular.module('ciApp').controller('pacienteController', ['$scope','$rootScope', 'pacienteFactory','profesionFactory','$location','$routeParams','$filter','mensaje','$http', function ($scope,$rootScope, pacienteFactory,profesionFactory,$location,$routeParams,$filter,mensaje,$http) {
        $scope.iconReverse="up";
        $scope.order='persona.perId';           //para orden de la lista de pacientes
        $scope.reverse=true;
        $scope.editar=false;        //para verificar si esta editando
        ///////////////Variables para paginacion/////////////////////////// 
        $scope.numItems=5;
        $scope.paginas=1;
        $scope.searchString='';
        $scope.listaPacientes = [];
       
          //Obtiene el paciente a editar
        function obtenerPaciente(id) {
               setTimeout(function () {
            $scope.$apply(function () {
            
            pacienteFactory.obtenerPaciente(id).then(function (data) {
                //1
                $scope.expediente = data.expediente;
                //2
                $scope.persona = data.persona;
                //3
                $scope.contacto = data.persona.contacto;
                //4
                $scope.profesion = data.profesion;
                //5
                $scope.publicidad = data.publicidad;
                //6
                $scope.documento = data.persona.documento;
                //7
                $scope.paciente = data;
                //8
                 $scope.pais = $scope.persona.pais;
                if(data.persona.municipio!=null){
                $scope.departamento = data.persona.municipio.departamento;
                 $scope.municipioAux=data.persona.municipio;
                $scope.departamentoAux=data.persona.municipio.departamento.depCodigo;
                $scope.departamentoNombre=data.persona.municipio.departamento.depNombre;
                    //9
                   
                  
            }
                $scope.sexo = data.persona.sexo;
                $scope.estadoCivil=data.persona.estadoCivil;
              
                $scope.sangreAux=data.sangre;
                $scope.publicidadAux=data.publicidad;
                $scope.profesion=data.profesion;

                $scope.findAllMunicipio();
                $(document).ready(function() {
            $('select').material_select();
                });
                
                $('label').addClass("active");
                $('tipo1').removeClass("active");
                $('descripcion1').removeClass("active");
            }).catch(function (error) {
                console.error(error);
            });
             $(document).ready(function() {
    $('select').material_select();
                    });
             }); 
              }, 2000);
        };
        
        $scope.departamentoAux = {
           
        };
        
        $scope.departamento={
            
        };
        
        $scope.sexo = {
           
        };

        $scope.expediente = {
          
        };

        $scope.contacto = {
          
        };

        $scope.documento = {
        
        };

        $scope.persona = {
        
        };
        $scope.paciente = {
        
        };
        $scope.profesion = {
            "profNombre": null
          
        };

        //Inicializacion de las listas de la informacion necesaria para el registro o edicion
        $scope.listaDepartamento = [];
        $scope.listaMunicipio = [];
        $scope.listaTipoSangre = [];
        $scope.listaEstadosCivil = [];
        $scope.listaDocumento = [];
        $scope.listaSexo = [];
        $scope.listaTipoPublicidad = [];
        $scope.listaProfesion=[];
        $scope.listaPais = [];
        //Para pasar de una pagina a cualquiera
         $scope.findAllPublicidad=function() {
            pacienteFactory.listaPublicidad().then(function (data) {
                $scope.listaTipoPublicidad = data;
                $(document).ready(function() {
            $('select').material_select();
                });
            }).catch(function (error) {
                
            });
        };
        $scope.findAllProfesion=function() {
            profesionFactory.listarProfesion().then(function (data) {
                $scope.listaProfesion = data;
                $(document).ready(function() {
            $('select').material_select();
                });
            }).catch(function (error) {
                
            });
        };
        
        //Para no traer la lista de pacientes innecesariamente
        if($location.path()!='/addPaciente' && $location.path()!='/editPaciente/'+$routeParams.pacId && $location.path()!='/configuracion'){ 
        listarPaciente();
         //En caso de editar o registrar, se debe extraer la informacion necesaria
        }else if($location.path()=='/addPaciente' || $location.path()=='/editPaciente/'+$routeParams.pacId){
            
            findAllTipoSangre();
            findAllEstadoCivil();
            findAllSexo();
            $scope.findAllPublicidad();
            $scope.findAllProfesion();
            findAllPais();
            findAllDepartamento();
            setTimeout(function () {
            $scope.$apply(function () {
             $(document).ready(function() {
    $('select').material_select();
                    });
             }); 
              }, 4000);
        }
          //En caso de editar el paciente se cambia el texto a editar, y se extrae el paciente a peticion
        if($location.path()=='/editPaciente/'+$routeParams.pacId){  
            $scope.editar=true;
            obtenerPaciente($routeParams.pacId);
        }else{
            $scope.editar=false;
        }
             
             //Extrae todos los pacientes disponibles
        function listarPaciente() {
            pacienteFactory.listaPacientes().then(function (data) {
                $scope.listaPacientes = data;
                $scope.paginas=Math.ceil(data.length/$scope.numItems); //Deacuerdo a la cantidad de pacientes               
                $scope.loaderVar=false; //Variable para el loader
                setTimeout(function () {
            $scope.$apply(function () {
                 $('#example').DataTable({
                        pageLength: 10,
                        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                        pagingType: 'simple_numbers',
                        order: [[0, "des"]],
                        language: {
                        "emptyTable": "Sin registros disponibles en la tabla",
                        "info": "Mostrando _START_ a _END_ de _TOTAL_ registros",
                        "infoEmpty": "",
                        "infoFiltered": "(filtrados de _MAX_ totales )",
                        "lengthMenu": " ",
                        "search": "Buscar:",
                        "zeroRecords": "Búsqueda sin resultados",
                        "paginate": {
                            "first": "Primero",
                            "last": "Último",
                            "next": "Siguiente",
                            "previous": "Anterior"
                        }
                            }
                    });        
            }); 
              }, 2000);
            
            if(data.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                
            });
        }
        
        $scope.listarPacienteDesactivated= function() {
            $scope.tablaPacientesInactivos=true;
            pacienteFactory.listaPacientesInactivos().then(function (data) {
                $scope.listaPacientes = data;
                $scope.paginas=Math.ceil(data.length/$scope.numItems); //Deacuerdo a la cantidad de pacientes               
                $scope.loaderVar=false; //Variable para el loader
            
            if(data.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                
            });
        };
        
        if($location.path()=='/configuracion'){
            $scope.listarPacienteDesactivated();
        }
        
        $scope.agregarProfesion=function(){
            $('#modalIngProfesion').openModal();
        };


        $scope.limpiarCodDoc = function () {
            $scope.documento.docCodigo = "";
        };

            //Extrae los sexos 
        function findAllSexo() {
            pacienteFactory.listaSexos().then(function (data) {
                $scope.listaSexo = data;
                $(document).ready(function() {
            $('select').material_select();
                });
            }).catch(function (error) {
                
            });
        }
            //Extrae todos los tipos de sangre disponibles
        function findAllTipoSangre() {
            pacienteFactory.listaSangre().then(function (data) {
                $scope.listaTipoSangre = data;
                $(document).ready(function() {
            $('select').material_select();
                    });
            }).catch(function (error) {
                
            });
        }
            //Estrae todos los estados civiles a mostrar
        function findAllEstadoCivil() {
            pacienteFactory.listaEstadosCivil().then(function (data) {
                $scope.listaEstadosCivil = data;
                 $(document).ready(function() {
                $('select').material_select();
                });
            }).catch(function (error) {
                
            });
        }
        
        //Extrae todos los Paises disponibles
        function findAllPais() {
            pacienteFactory.listarPais().then(function (deps) {
                $scope.listaPais = deps;
                 $(document).ready(function() {
                    $('select').material_select();
                });
                
                }).catch(function (error) {
                console.error(error);
            });
        }
        
            //Extrae todos los municipios segun el codigo de departamento seleccionado
        $scope.findAllMunicipio = function () {
            
            $scope.listaMunicipio = [];
            pacienteFactory.listaMunicipios($scope.departamento.depCodigo).then(function (muns) {
                $scope.listaMunicipio = muns;
                 
                $(document).ready(function() {
            $('select').material_select();
                });
            }).catch(function (error) {
                console.error(error);
            });
        };
        //activa los departamentos
        $scope.elegirPais=function(){
              findAllDepartamento();
        };
            //Extrae todos los Departamentos
        function findAllDepartamento() {
            pacienteFactory.listaDepartamentos().then(function (deps) {
                $scope.listaDepartamento = deps;
                
                
            }).catch(function (error) {
                console.error(error);
            });
        }
        //Guargar un nuevo paciente, a la vez de una persona,profecion
        $scope.guardarPaciente = function () {
            if($scope.persona.sexId==undefined || $scope.persona.estId==undefined){
                      Materialize.toast("No ha seleccionado un sexo o estado civil",1000);
                      return 0;
                  }
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            
            $scope.persona.docId=null;
            $scope.persona.conId=null;
            pacienteFactory.guardarContacto($scope.contacto).then(function(cont){
                $scope.persona.conId = cont.conId;
                if($scope.persona.docId!=null ){
                     delete $scope.persona.sexo;
                     delete $scope.persona.estadoCivil;
                     console.log($scope.persona);
                      pacienteFactory.guardarPersona($scope.persona).then(function (per) {
                          
                          $scope.paciente.perId = per.perId;
                           $scope.paciente.usrNombre=$rootScope.user.name;
                           $scope.paciente.pacModificacion=new Date();
                           
                           pacienteFactory.guardarPaciente($scope.paciente).then(function (msj) {
                                $('#preload1').remove("active");
                                        $('#preload2').remove("active");
                                         mensaje.resultado("Paciente",msj);
                                         $location.path('/pacientes');
                           });
                      });
                }
            });
            pacienteFactory.guardarDocumento($scope.documento).then(function (docu) {
                  $scope.persona.docId = docu.docId;
                  
                  if($scope.persona.conId!=null){
                     delete $scope.persona.sexo;
                     delete $scope.persona.estadoCivil;
                     console.log($scope.persona);
                      pacienteFactory.guardarPersona($scope.persona).then(function (per) {
                          
                          $scope.paciente.perId = per.perId;
                          $scope.paciente.usrNombre=$rootScope.user.name;
                           $scope.paciente.pacModificacion=new Date();
                           console.log($scope.paciente);
                           pacienteFactory.guardarPaciente($scope.paciente).then(function (msj) {
                                $('#preload1').remove("active");
                                        $('#preload2').remove("active");
                                        mensaje.resultado("Paciente",msj);
                                         $location.path('/pacientes');
                           });
                      });
                }
            });    
            
        };
            //Deshabilita el paciente seleccionado
        $scope.deshabilitarPaciente = function (id) {
           swal({
                title: "\u00BFDesea deshabilitar este paciente?",
                text: "\u00A1El paciente ser&aacute deshabilitado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo deshabilitarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
            pacienteFactory.darBajaPaciente(id).then(function (msj) {
                mensaje.resultado("Paciente",msj);
                listarPaciente();
            }).catch(function (error) {
                console.error(error);
            });
           
                    });
        
        };
        
         //Deshabilita el paciente seleccionado
        $scope.eliminarPaciente = function (id) {
           swal({
                title: "\u00BFDesea eliminar definitivamente este paciente?",
                text: "\u00A1El paciente ser&aacute eliminado, <br>la operaci&oacuten no se puede revertir!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#ff5252",
                confirmButtonText: "Si, deseo eliminarlo",
                closeOnConfirm: false,
                html: true
                },
                function(){
            pacienteFactory.eliminarPaciente(id).then(function (msj) {
                mensaje.resultado("Paciente",msj);
                $scope.listarPacienteDesactivated();
            }).catch(function (error) {
                console.error(error);
            });
            
                    });
        
        };
        
                    //Vuelve a activar el paciente seleccionado
        $scope.activarPaciente = function (id) {
           swal({
                title: "\u00BFDesea activar este paciente?",
                text: "\u00A1El paciente seleccionado ser&aacute activado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#4caf50",
                confirmButtonText: "Si, deseo activarlo.",
                closeOnConfirm: false,
                html: true
                },
                function(){
            pacienteFactory.activarPaciente(id).then(function (msj) {
                mensaje.resultado("Paciente",msj);
                $scope.listarPacienteDesactivated();
            }).catch(function (error) {
                console.error(error);
            });
            
                    });
        
        };
        
        
           
           
           //en caso de que la publicidad a agregar no se encuentre
        $scope.agregarPublicidad=function(){
            $('#modalIngPublicidad').openModal();
        };
        
         $scope.getData = function () {
         return $filter('filter')($scope.listaPacientes, $scope.searchString);
         };
        
          //Fucion para ordenar tabla dando click en el titulo de la columna de la tabla
         $scope.orderBy=function(order){
          $scope.order=order;
          $scope.reverse=!$scope.reverse;
          if($scope.reverse){
              $scope.iconReverse="down";
          }else{
              $scope.iconReverse="up";
          }
        };
        
        $scope.cancelar=function(){
          if($scope.paciente.pacEstado==true || $location.path()=='/addPaciente'){
              $location.path('/pacientes'); 
          }else{
              $location.path('/configuracion'); 
          }  
        };
        
        $scope.MoverTablaInactivos=function(){
            $scope.tablaPacientesInactivos=false;
        };
        $scope.mun=function(){
            $(document).ready(function() {
    $('select').material_select();
  }); 
  return "jj";
        };
        
        $scope.generarReportePacientes=function(){
            $http.get('rest/reportsvc/report_pacientes/', {responseType: 'arraybuffer'}).then(function (response) {
                var archivo = new Blob([response.data], {type: 'application/pdf'});
                var isChrome = !!window.chrome && !!window.chrome.webstore;
                if (isChrome) {
                    var url = window.URL || window.webkitURL;
                    var fileURLi = URL.createObjectURL(archivo);
                    window.open(fileURLi, "_blank");
                } else {
                    var fileURL = URL.createObjectURL(archivo);
                    window.open(fileURL);
                }
                msjAviso("Documento generado con Exito", "success");
            }).catch(function (error) {
                console.error(error);
            });
        };
    }
]).filter('busqueda',function(){
   return function(arr,searchString){
       if(!searchString){
           return arr;
       }
       var result = [];
        searchString = searchString.toLowerCase();
        angular.forEach(arr, function(paciente){
            var primerN=paciente.persona.perPrimerNombre.toLowerCase();
           
            var primerA=paciente.persona.perPrimerApellido.toLowerCase();
            var expediente=paciente.expediente.expCodigo.toLowerCase();
            var sexo=paciente.persona.sexo.sexNombre.toLowerCase();
            if(paciente.persona.perSegundoNombre!=null){
                 var segundoN=paciente.persona.perSegundoNombre.toLowerCase();
            }
            if(paciente.persona.perSegundoApellido!=null){
            var segundoA=paciente.persona.perSegundoApellido.toLowerCase();
        }
            
            if((primerN+" "+primerA+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((segundoN+" "+primerA+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((primerN+" "+segundoN+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((primerN+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((segundoN+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((expediente).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((sexo).indexOf(searchString)!= -1){
                result.push(paciente);
            }else if((primerN+" "+segundoN+" "+primerA+" "+segundoA).indexOf(searchString)!= -1){
                result.push(paciente);
            }
        });
        return result;
    };

});
 