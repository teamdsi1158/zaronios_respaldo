/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
'use strict';



angular.module('ciApp').factory('citaFactory', ['$q', '$http', function ($q, $http) {
         return{
             guardarCita: guardarCita,
           obtenerCita: obtenerCita,
            listarCita: listarCita,
           eliminarCita: eliminarCita
        };
        
         function listarCita() {
            var deferred = $q.defer();
            $http.get('rest/cita/citasvc/list/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga las citas');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        
          function guardarCita(cita) {
            
            var deferred = $q.defer();
            $http.post('rest/cita/citasvc/save/', cita).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba la cita');
            deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        
        function obtenerCita(id) {
            var deferred = $q.defer();
            $http.get('rest/cita/citasvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga la cita');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
        
        
         function eliminarCita(id) {
            var deferred = $q.defer();
            $http.delete('rest/cita/citasvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras eliminaba la cita');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }
}]);

