'use strict';

angular.module('ciApp').factory('usuarioFactory', ['$q', '$http', function ($q, $http) {
        return {
            listarUsuario: listarUsuario,
            guardarUsuario: guardarUsuario,
            darBajaUsuario: darBajaUsuario,
            obtenerUsuario: obtenerUsuario
        };

        function listarUsuario() {
            var deferred = $q.defer();
            $http.get('rest/usuario/usuariosvc/list/').then(function (response) {
                deferred.resolve(response.data);

            }, function (errResponse) {
                console.error('Error mientras carga los usuarios');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function guardarUsuario(usuario) {
            var deferred = $q.defer();
            $http.post('rest/usuario/usuariosvc/save/', usuario).then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras guardaba el usuario');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function darBajaUsuario(id) {
            var deferred = $q.defer();
            $http.delete('rest/usuario/usuariosvc/del/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras daba de baja el usuario');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

        function obtenerUsuario(id) {
            var deferred = $q.defer();
            $http.get('rest/usuario/usuariosvc/get/' + id + '/').then(function (response) {
                deferred.resolve(response.data);
            }, function (errResponse) {
                console.error('Error mientras carga el usuario');
                deferred.reject(errResponse);
            });
            return deferred.promise;
        }

    }]);

angular.module('ciApp').controller('usuarioController', ['$scope', 'usuarioFactory', 'empleadoFactory', 'rolFactory', '$location', '$routeParams', '$filter','mensaje', function ($scope, usuarioFactory, empleadoFactory, rolFactory, $location, $routeParams, $filter,mensaje) {
        $scope.iconReverse = "up";
        $scope.order = 'usrUsername';           //para orden de la lista de publicidad
        $scope.reverse = false;
        $scope.searchString = '';
        $scope.numItems = 5;   //Cantidad de Items por pagina
        $scope.paginas = 1;
        $scope.listaUsuario = [];
        //para mostrar guardado o edicion
        $scope.editar = false;
        $scope.empleado = {
            
        };

        $scope.usuario = {usrId: null, usrUsername: null, usrPassword: null, rolId: null, empId: null, empleado: {}};

        $scope.c = {};

        $scope.getData = function () {
            return $filter('filter')($scope.listaUsuario, $scope.searchString);
        };

        //Fucion para ordenar tabla dando click en el titulo de la columna de la tabla
        $scope.orderBy = function (order) {
            $scope.order = order;
            $scope.reverse = !$scope.reverse;
            if ($scope.reverse) {
                $scope.iconReverse = "down";
            } else {
                $scope.iconReverse = "up";
            }
        };

        if ($location.path() !== '/addUsuario' && $location.path() !== '/editUsuario/' + $routeParams.usrId) {
            listarUsuario();
        }
        if ($location.path() === '/editUsuario/' + $routeParams.usrId) {
            $scope.editar = true;
            obtenerUsuario($routeParams.usrId);
        } else {
            listarEmpleado();
            listarRol();
            $scope.editar = false;
        }

        function listarUsuario() {
            usuarioFactory.listarUsuario().then(function (lista) {
                $scope.listaUsuario = lista;
                $scope.paginas = Math.ceil(lista.length / $scope.numItems);
                $scope.loaderVar=false; //Variable para el loader
            if(lista.length==0){
                $scope.vacio=true; //variable para poner un titulo en la vista
            }else{
                $scope.vacio=false;
            }
            }).catch(function (error) {
                console.error(error);
            });
        }

        function listarEmpleado() {
            empleadoFactory.listarEmpleadoSinUsuario().then(function (lista) {
                $scope.listaEmpleado = lista;
            }).catch(function (error) {
                console.error(error);
            });
        }

        function listarRol() {
            rolFactory.listarRol().then(function (lista) {

                $scope.listaRol = lista;
            }).catch(function (error) {
                console.error(error);
            });
        }

        function obtenerUsuario(id) {
            usuarioFactory.obtenerUsuario(id).then(function (data) {
                $scope.usuario = data;
                $scope.usuario = {usrId: data.usrId, usrUsername: data.usrUsername, usrPassword: data.usrPassword, rolId: data.rolId, empId: data.empId,empleado: data.empleado};
                $scope.empId=data.empId;
                $('label').addClass('active');

            }).catch(function (error) {
                console.error(error);
            });
        }
        ;

        $scope.guardarUsuario = function () {
            $('#preloadPubli1').addClass("active");
            $('#preloadPubli2').addClass("active");
            if ($scope.contra1 != $scope.contra2) {
                Materialize.toast('' + "Ambas contrase&ntilde;as deben ser iguales", 1000);
                return 0;
            }
            $scope.usuario.usrPassword = $scope.contra1;
             $scope.usuario.rolId=parseInt($scope.usuario.rolId);
             $scope.usuario.empId=parseInt($scope.usuario.empId);
             delete $scope.usuario.empleado;
            $('#preload1').addClass("active");
            $('#preload2').addClass("active");
            usuarioFactory.guardarUsuario($scope.usuario).then(function (usuario) {
                var i = 0;
                // para iterar la lista de empleados 
                for (i = 0; i < $scope.listaEmpleado.length; i++) {
                    if ($scope.listaEmpleado[i].empId === parseInt($scope.usuario.empId)) {
                        $scope.listaEmpleado[i].usrId = usuario.usrId;
                        $scope.empleado = $scope.listaEmpleado[i];
                    }
                }
                empleadoFactory.guardarEmpleado($scope.empleado).then(function (msj) {
                    mensaje.resultado("Empleado",msj);
                }).catch(function (error) {
                    console.error(error);
                });
                $('#preload1').remove("active");
                $('#preload2').remove("active");
                // Materialize.toast('' + msj, 1000);
                $location.path('/usuarios');
            }).catch(function (error) {
                $('#preload1').remove("active");
                $('#preload2').remove("active");
                console.error(error);
                $location.path('/usuarios');
            });
        };

        $scope.selectEmpleado = function (data) {
            
        };

        $scope.eliminarUsuario = function (id) {

            var i = 0;
            for (i = 0; i < $scope.listaUsuario.length; i++) {
                
                
                if ($scope.listaUsuario[i].usrId === id) {
                    $scope.empleado = $scope.listaUsuario[i].empleado;
                    $scope.empleado.usuario = null;
                }
            }
            swal({
                title: "\u00BF Desea deshabilitar este usuario?",
                text: "\u00A1 Este registro sera deshabilitado!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, deseo deshabilitar!",
                closeOnConfirm: false
            },
                    function () {
                      
                        empleadoFactory.guardarEmpleado($scope.empleado).then(function (msj) {


                            usuarioFactory.darBajaUsuario(id).then(function (msj) {
                                 mensaje.resultado("Usuario",msj);
                                listarUsuario();
                            }).catch(function (error) {
                                console.error(error);
                            });
                            
                        }).catch(function (error) {
                            console.error(error);
                        });
                    });
        };

    }]);