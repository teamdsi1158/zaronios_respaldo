<!DOCTYPE html>
<html ng-app="ciApp">
    <head>
        <title>Zaronios</title>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <link href="css/sweetalert.css" rel="stylesheet">
        <meta name="msapplication-TileColor" content="#00bcd4">
        <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
        <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="css/login.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="css/least.min.css" type="text/css" rel="stylesheet" /> <!--Librer�a para las im�genes-->
        <style type="text/css">
            .ng-hide {
                display: none;
            }
        </style>
        <link rel="shortcut icon" href="images/logo.ico" type="image/x-icon">
    </head>
    <body>

        <div class="loader" ng-hide="initialized">
            <p>Loading</p>
            <p><span class="fa fa-spinner fa-spin"></span></p>
        </div>

        <div class="container ng-hide" ng-show="initialized" style="width: 100%">
            <!--<div class="alert alert-danger" ng-show="error">{{error}}</div>-->
            <div ng-view ></div>
        </div>

        <script type="text/javascript" src="js/angular/jquery.min.js"></script>    
        <script type="text/javascript" src="js/propios/materialize.min.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap-datepicker.js"></script>
        <script type="text/javascript" src="js/bootstrap/bootstrap-table.js"></script>
        <!-- ANGULAR LIBS -->

        <script type="text/javascript" src="js/angular/angular.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-resource.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-route.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-cookies.min.js"></script>

        <!-- JS PROPIAS DE APLICACI�N -->
        <script type="text/javascript" src="js/config.js"></script>
        <script type="text/javascript" src="js/login.js"></script>
        <script type="text/javascript" src="js/sweetalert.min.js"></script>
        <script type="text/javascript" src="js/least/least.min.js"></script> <!--Librer�a para im�genes-->
        <script type="text/javascript" src="js/angular/jquery.min.js"></script> <!--Librer�a para im�genes-->
    </body>
</html>
