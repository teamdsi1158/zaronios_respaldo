<!DOCTYPE html>
<html ng-app="ciApp">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Zaronios</title>
        <script type="text/javascript"  src="js/chart/Chart.bundle.js"></script>
        <script type="text/javascript"  src="js/chart/utils.js"></script>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="css/index.css"  media="screen,projection"/>
        <link href="css/prism.css" rel="stylesheet">
        <link href="css/sweetalert.css" rel="stylesheet">
        <link href="css/ghpages-materialize.css" type="text/css" rel="stylesheet" media="screen,projection">
        <link href="css/iconos.css" rel="stylesheet">
        <link href="css/fullcalendar.min.css" rel="stylesheet">
        <link href="css/fullcalendar.print.min.css" rel="stylesheet"  media='print'  >
        <link href="css/least/least.min.css" rel="stylesheet" >
        <link href="css/fine-uploader/fine-uploader-gallery.min.css" rel="stylesheet" >
        <link href="css/fine-uploader/fine-uploader-new.css" rel="stylesheet" >
        <!--  DataTables      -->
        <link href="css/dataTable/datatables.css" rel="stylesheet">
        <link href="css/dataTable/datatables.min.css" rel="stylesheet">
        
        <!--<link href="css/chosen.css" rel="stylesheet"  media='print'  > -->
        <script type="text/javascript" src="js/angular/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/angular/angular.min.js"></script>
        <script type="text/javascript" src="js/app.js"></script> 

        <style type="text/css">
            .ng-hide {
                display: none;
            }
            .modal { width: 80% !important ; max-height: 75% !important; height: 74% !important }
        </style>
        <link rel="shortcut icon" href="images/logo.ico" type="image/x-icon">
    </head>

    <body class="ng-hide" ng-show="initialized">
        <header ng-show="initialized">
            <div class="container ">
                <a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only">
                    <i class="material-icons">menu</i>
                </a>
            </div>
            <ul id="nav-mobile" class="side-nav fixed" ng-controller="MenuController">
                <li><li><img src="images/navBar6.jpg" width="105%" /></li></li>
                <li><a class="waves-effect waves-teal grisito"><i class="material-icons md-light white-text">face</i> <span ng-bind="user.name"></span></a></li>
                <div class="divider"></div>
                <li class="bold" ng-repeat="item in listaMenu">
                    <span ng-if="item.nivel === 1 && item.menRuta !== null">
                        <a href="#!{{item.menRuta}}" class="waves-effect waves-teal grisito">
                            <i class="material-icons md-light white-text">{{item.menIcono}}</i> {{item.menNombre}}
                        </a>
                    </span>
                </li>
                <li class="no-padding">
                    <ul  class="collapsible collapsible-accordion">
                        <li style="background-color: #263238;">
                            <a ng-if="bh==true" style="background-color: #263238;"  class="collapsible-header waves-effect waves-teal grisito">
                                <i class="material-icons md-light white-text"> settings </i> Configuraciones 
                            </a>
                            <div class="collapsible-body" style="background-color: #263238;">
                                <ul ng-repeat="item in listaMenu">
                                    <span ng-show="item.menRuta === null">
                                        <li ng-repeat="hi in item.hijos">
                                            <a href="#!{{hi.menRuta}}" class="waves-effect waves-teal grisito">
                                                <i class="material-icons md-light white-text">{{hi.menIcono}}</i> {{hi.menNombre}}
                                            </a>
                                        </li>
                                    </span>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>  
                <div class="divider"></div>
                <li>
                    <a ng-click="logout()" class="waves-effect waves-red grisito">
                        <i class="material-icons white-text">close</i> Cerrar Sesi&oacute;n
                    </a>
                </li>
            </ul>

        </header>



        <main>         

            <div class="ng-hide" ng-show="initialized">

                <!--<div class="alert alert-danger" ng-show="error">{{error}}</div>-->
                <div ng-view></div>
            </div>
        </main>

        <!-- MATERIALIZE LIBS -->



        <script type="text/javascript" src="js/sweetalert.min.js"></script>
        <script type="text/javascript" src="js/propios/materialize.min.js"></script>
        <script type="text/javascript" src="js/angular/spin.js"></script>
        <!-- ANGULAR LIBS -->

        <script type="text/javascript" src="js/angular/angular-resource.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-route.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-cookies.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-spinner.min.js"></script>
        <script type="text/javascript" src="js/angular/angular-loading-spinner.js"></script>
        <script type="text/javascript" src="js/angular/angular-materialize.min.js"></script>
        <script type="text/javascript" src="js/moment.js"></script>


        <!-- LIB JS PARA CREAR PDF 
        <script type="text/javaScript" src="js/propios/jQuery.print.min.js" ></script>
        <!-- JS PROPIAS DE APLICACI�N -->
        <script  type="text/javascript" src="js/angular/angular-responsive-tables/release/angular-responsive-tables.min.js" ></script>       
        <script  type="text/javascript" src="js/angular/dirPagination.js"></script> 
        <script type="text/javascript" src="js/config.js"></script>
        <!-- JS para mensajes -->
        <script type="text/javascript" src="js/mensajesJs/mensajes.js"></script>

        <script type="text/javascript" src="js/sweetalert.min.js"></script>        
        <script type="text/javascript" src="js/misfiltros.js"></script> 
        <!-- Js para controllers -->
        <script type="text/javascript" src="js/paciente.js"></script> 
        <script type="text/javascript" src="js/empleado.js"></script> 
        <script type="text/javascript" src="js/tratamiento.js"></script> 
        <script type="text/javascript" src="js/publicidad.js"></script> 
        <script type="text/javascript" src="js/expediente.js"></script>
        <script type="text/javascript" src="js/sesion.js"></script>
        <script type="text/javascript" src="js/expediente.js"></script>
        <script type="text/javascript" src="js/usuario.js"></script>
        <script type="text/javascript" src="js/rol.js"></script>
        <script type="text/javascript" src="js/accessToken.js"></script>
        <script type="text/javascript" src="js/paginacion.js"></script>
        <script type="text/javascript" src="js/sexo.js"></script>
        <script type="text/javascript" src="js/estadoCivil.js"></script>
        <script type="text/javascript" src="js/profesion.js"></script>
        <script type="text/javascript" src="js/pago.js"></script>
        <script type="text/javascript" src="js/signoVital.js"></script>
        <script type="text/javascript" src="js/cita.js"></script>
        <script type="text/javascript" src="js/estado.js"></script>
        <script type="text/javascript" src="js/tipoPago.js"></script>
        <script type="text/javascript" src="js/bitacora.js"></script>
        <script type="text/javascript" src="js/menu.js"></script>
        <!--Para las m�scaras en el input-->
        <script type="text/javascript" src="js/jasny/jasny-bootstrap.js"></script>

        <!-- Least js -->
        <script type="text/javascript" src="js/least/least.min.js"></script>

        <!-- fine uploader -->
        <script type="text/javascript" src="js/fine-uploader/fine-uploader.min.js"></script>
        <!-- full Calendar
        <script type="text/javascript" src="js/fullCalendar/moment.min.js"></script>
        <script type="text/javascript" src="js/fullCalendar/fullcalendar.min.js"></script>
        <script type="text/javascript" src="js/fullCalendar/es.js"></script> -->
        <!--  Data Tables      -->
        
        <script type="text/javascript" src="js/dataTable/datatables.min.js"></script>
        <script type="text/javascript" src="js/dataTable/datatables.js"></script>
        <!-- Full Calendar extencion google 
       
        <script type='text/javascript' src='js/fullCalendar/gcal.min.js'></script>-->
        <!-- Para cambiar de idioma de fullCalendar 
         <script type="text/javascript" src="js/jasny-bootstrap.js"></script>  <script type="text/javascript" src="js/chosen.jquery.js" ></script>
           <script type="text/javascript" charset="utf-8" src="js/docsupport/init.js" ></script>-->

    </body>
    <style>
        /* label color */
        .input-field label {
            color: #37474f;
        }

        #obligatorio{
            color: #f44336;
            font-size: 1.5em;
        }

        header, main, footer {
            padding-left: 240px;
        }

        @media only screen and (max-width : 992px) {
            header, main, footer {
                padding-left: 0;
            }
        }
        .side-nav {
            background-color: #263238 !important;
        }

        .grisito{
            color: #e0e0e0 !important;
        }   

        /*li:hover{
            background-color: red
        } */

    </style>

    <script>
                $('.button-collapse').sideNav({
                    menuWidth: 240, // Default is 300
                    edge: 'left', // Choose the horizontal origin
                    closeOnClick: false, // Closes side-nav on <a> clicks, useful for Angular/Meteor
                    draggable: false, // Choose whether you can drag to open on touch screens,
                    onOpen: function (el) { /* Do Stuff*/
                    }, // A function to be called when sideNav is opened
                    onClose: function (el) { /* Do Stuff*/
                    }, // A function to be called when sideNav is closed
                }
                );
    </script>
</html>