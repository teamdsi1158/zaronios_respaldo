package sv.com.zaronios.expediente.dao;



import java.util.List;
import sv.com.zaronios.expediente.security.entity.Entity;

public interface Dao<T extends Entity, I>
{
    List<T> findAll();

    T find(I id);

    T save(T entity);

    void delete(I id);

    void delete(T entity);
}
