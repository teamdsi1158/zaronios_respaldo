/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author sscruz
 */
@Controller
public class IndexController {
 @RequestMapping(value =  "/")
    public String homePage(ModelMap model) {
        
        return "index";
    }
    
}
