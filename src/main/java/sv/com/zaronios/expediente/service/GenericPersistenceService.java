/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.service;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Marlon
 */
@Service("genericService")
public class GenericPersistenceService {

    @PersistenceContext
    private EntityManager entityManager;

    public Object getById(Class clazz, Serializable id) throws Exception {
        return entityManager.find(clazz, id);
    }

    public List select(Class clazz, String order) throws Exception {
        String orderBy = (order == null ? "" : "ORDER BY " + order);
        String clazzName = clazz.getSimpleName();
        Query query = entityManager.createQuery("SELECT c FROM " + clazzName + " c " + orderBy);
        return query.getResultList();
    }

    @Transactional
    public List select(String q) throws Exception {
        Query query = entityManager.createQuery(q);
        System.out.println("Size: " + query.getResultList().size());
        return query.getResultList();
    }

    public List busqueda(String q) throws Exception {
        Query query = entityManager.createQuery(q);
        query.setMaxResults(500);
        return query.getResultList();
    }

    public List selectq(String q) throws Exception {
        Query query = entityManager.createQuery(q);
        query.setMaxResults(20);
        return query.getResultList();
    }

    public List listNativeSQL(String q) throws Exception {
        Query query = entityManager.createNativeQuery(q);
        return query.getResultList();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public Boolean esContenido(Serializable o) {
        return entityManager.contains(o);
    }

    @Transactional
    public Serializable save(Serializable o) throws Exception {
        entityManager.persist(o);
        return o;
    }

    @Transactional
    public Serializable update(Serializable o) throws Exception {
        entityManager.merge(o);
        return o;
    }

    public List select(Class clazz, List<Criteria> filters, String order) throws Exception {
        String clazzName = clazz.getSimpleName();
        String select = "SELECT c FROM " + clazzName + " c ";
        String where = "WHERE ";
        String orderBy = (order == null ? "" : "ORDER BY c." + order + " DESC");

        try {
            for (Criteria criteria : filters) {
                String attribute;

                if (criteria.getParentesis() != null && criteria.getParentesis() == 1) {
                    where += "( ";
                }

                //Atributos 
                String[] elements = criteria.getAttribute().split("\\.");

                if (elements.length > 1) {
                    attribute = elements[elements.length - 1];
                } else {
                    attribute = criteria.getAttribute();
                }

                if (criteria.getRelationalOp().equalsIgnoreCase("LIKE")) {
                    where += " UPPER(TRANSLATE(c." + criteria.getAttribute() + ",'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) ";

                    String s = (String) criteria.getValue();

                    criteria.setValue(removerCaracteresEspeciales(s));

                } else {
                    where += "c." + criteria.getAttribute();
                }

                //Operador 
                if (criteria.getValue() != null) {
                    where += " " + criteria.getRelationalOp() + " :" + attribute + " ";
                } else {
                    where += " " + criteria.getRelationalOp() + " ";
                }

                if (criteria.getParentesis() != null && criteria.getParentesis() == 0) {
                    where += ") ";
                }

                //Operador lógico 
                if (criteria.getLogicalOp().isEmpty()) {
                    where += " OR ";
                } else {
                    where += " " + criteria.getLogicalOp() + " ";
                }

            }

            if (!filters.isEmpty()) {
                where = where.substring(0, where.length() - 4);
            }

            Query query = entityManager.createQuery(select + where + orderBy);

            for (Criteria criteria : filters) {
                if (criteria.getValue() != null) {
                    String attribute;

                    //Atributos 
                    String[] elements = criteria.getAttribute().split("\\.");

                    if (elements.length > 1) {
                        attribute = elements[elements.length - 1];
                    } else {
                        attribute = criteria.getAttribute();
                    }

                    query.setParameter(attribute, criteria.getValue());
                }
            }

            return query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
    }

    public List select(Class clazz, List<String> attributes, List<Criteria> filters, String order) throws Exception {
        String clazzName = clazz.getSimpleName();
        String select = "SELECT ";
        String where = "WHERE ";
        String orderBy = (order == null ? "" : "ORDER BY c." + order);

        try {
            for (String attr : attributes) {
                select += "c." + attr + ",";
            }

            select = select.substring(0, select.length() - 1);

            select += " FROM " + clazzName + " c ";

            for (Criteria criteria : filters) {
                String attribute;

                //Atributos 
                String[] elements = criteria.getAttribute().split("\\.");

                if (elements.length > 1) {
                    attribute = elements[elements.length - 1];
                } else {
                    attribute = criteria.getAttribute();
                }

                if (criteria.getRelationalOp().equalsIgnoreCase("LIKE")) {
                    where += " UPPER(TRANSLATE(c." + criteria.getAttribute() + ",'áéíóúÁÉÍÓÚ', 'aeiouAEIOU')) ";

                    String s = (String) criteria.getValue();

                    criteria.setValue(removerCaracteresEspeciales(s));

                } else {
                    where += "c." + criteria.getAttribute();
                }

                //Operador 
                where += " " + criteria.getRelationalOp() + " :" + attribute + " ";

                //Operador l�gico 
                if (criteria.getLogicalOp().isEmpty()) {
                    where += " OR ";
                } else {
                    where += " " + criteria.getLogicalOp() + " ";
                }

            }

            if (!filters.isEmpty()) {
                where = where.substring(0, where.length() - 4);
            }

            Query query = entityManager.createQuery(select + where + orderBy);

            for (Criteria criteria : filters) {
                String attribute;

                //Atributos 
                String[] elements = criteria.getAttribute().split("\\.");

                if (elements.length > 1) {
                    attribute = elements[elements.length - 1];
                } else {
                    attribute = criteria.getAttribute();
                }

                query.setParameter(attribute, criteria.getValue());
            }

            return query.getResultList();
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Transactional
    public boolean delete(Class clazz, Serializable id) throws Exception {
        boolean deleted = false;

        Object o = entityManager.find(clazz, id);

        if (o != null) {
            entityManager.remove(o);
            deleted = true;
        }

        return deleted;
    }

    @Transactional
    public boolean delete(String query) throws Exception {
        boolean deleted = true;
        try {
            Object o = entityManager.createQuery(query).executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            deleted = false;
        }
        return deleted;
    }

    @Transactional
    public boolean deleteSQL(String query) throws Exception {
        boolean deleted = true;
        try {
            Object o = entityManager.createNativeQuery(query).executeUpdate();
        } catch (Exception ex) {
            ex.printStackTrace();
            deleted = false;
        }
        return deleted;
    }

    public static String removerCaracteresEspeciales(String input) {
        // Cadena de caracteres original a sustituir.
        String original = "áàäéèëíìïóòöúùuñÁÀÄÉÈËÍÌÏÓÒÖÚÙÜÑçÇ";
        // Cadena de caracteres ASCII que reemplazarán los originales.
        String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
        String output = input;
        for (int i = 0; i < original.length(); i++) {
            // Reemplazamos los caracteres especiales.
            output = output.replace(original.charAt(i), ascii.charAt(i));
        }//for i
        return output;
    }

    /**
     * @param entityManager the entityManager to set
     */
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
