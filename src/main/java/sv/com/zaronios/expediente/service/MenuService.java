/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.service;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sv.com.zaronios.expediente.entity.Menu;

/**
 *
 * @author Marlon
 */
@Service("menuService")
public class MenuService {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public List getMenu() {
        String hql = "select m from Menu m order by m.orden asc";
        Query q = entityManager.createQuery(hql);
        List<Menu> menus = q.getResultList();
        return menus;
    }
}
