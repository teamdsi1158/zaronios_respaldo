/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.service;

/**
 *
 * @author rchicas
 */
public class Criteria {

    public static final int ABRIR_PARENTESIS = 1;
    public static final int CERRAR_PARENTESIS = 0;

    private String attribute;
    private String relationalOp;
    private Object value;
    private String logicalOp;
    private Integer parentesis; //null: sin paréntesis, 1: Abrir paréntesis, 0: Cerrar paréntesis 

    public Criteria(String attribute, String relationalOp, Object value, String logicalOp) {
        this.attribute = attribute;
        this.relationalOp = relationalOp;
        this.value = value;
        this.logicalOp = logicalOp;
    }

    public Criteria(String attribute, String relationalOp, Object value, String logicalOp, Integer parentesis) {
        this.attribute = attribute;
        this.relationalOp = relationalOp;
        this.value = value;
        this.logicalOp = logicalOp;
        this.parentesis = parentesis;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    public String getRelationalOp() {
        return relationalOp;
    }

    public void setRelationalOp(String relationalOp) {
        this.relationalOp = relationalOp;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getLogicalOp() {
        return logicalOp;
    }

    public void setLogicalOp(String logicalOp) {
        this.logicalOp = logicalOp;
    }

    public Integer getParentesis() {
        return parentesis;
    }

    public void setParentesis(Integer parentesis) {
        this.parentesis = parentesis;
    }

    @Override
    public String toString() {
        return "Criteria{" + "attribute=" + attribute + ", relationalOp=" + relationalOp + ", value=" + value + ", logicalOp=" + logicalOp + ", parentesis=" + parentesis + '}';
    }

}
