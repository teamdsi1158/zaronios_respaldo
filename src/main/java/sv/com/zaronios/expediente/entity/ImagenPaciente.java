/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "imagen_paciente", catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"imp_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImagenPaciente.findAll", query = "SELECT i FROM ImagenPaciente i")
    , @NamedQuery(name = "ImagenPaciente.findByImpId", query = "SELECT i FROM ImagenPaciente i WHERE i.impId = :impId")
    , @NamedQuery(name = "ImagenPaciente.findByImpDescripcion", query = "SELECT i FROM ImagenPaciente i WHERE i.impDescripcion = :impDescripcion")})
public class ImagenPaciente implements Serializable {

   
   

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "imp_id", nullable = false)
    private Integer impId;
    
     @Basic(optional = false)
    @NotNull
    @Column(name = "img_id", nullable = false)
    private int imgId;
     
     @JoinColumn(name = "img_id", referencedColumnName = "img_id", nullable = false,  insertable = false, updatable = false)
    @ManyToOne
    private Imagen imagen;
     
    @Column(name = "sesi_id")
    private Integer sesiId;
    
     @JsonBackReference
    @JoinColumn(name = "sesi_id", referencedColumnName = "sesi_id", nullable = false,  insertable = false, updatable = false)
    @ManyToOne
    private Sesion sesion;

    
    @Column(name = "imp_descripcion", length = 500)
    private String impDescripcion;
    
  
    public ImagenPaciente() {
    }

    public ImagenPaciente(Integer impId) {
        this.impId = impId;
    }

    public Integer getImpId() {
        return impId;
    }

    public void setImpId(Integer impId) {
        this.impId = impId;
    }

    public String getImpDescripcion() {
        return impDescripcion;
    }

    public void setImpDescripcion(String impDescripcion) {
        this.impDescripcion = impDescripcion;
    }
    
    public int getImgId() {
        return imgId;
    }

    public void setImgId(int imgId) {
        this.imgId = imgId;
    }
    
    
     public Imagen getImagen() {
        return imagen;
    }

    public void setImagen(Imagen imagen) {
        this.imagen = imagen;
    }

    public Integer getSesiId() {
        return sesiId;
    }

    public void setSesiId(Integer sesiId) {
        this.sesiId = sesiId;
    }
    
    public Sesion getSesion() {
        return sesion;
    }

    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (impId != null ? impId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenPaciente)) {
            return false;
        }
        ImagenPaciente other = (ImagenPaciente) object;
        if ((this.impId == null && other.impId != null) || (this.impId != null && !this.impId.equals(other.impId))) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.ImagenPaciente[ impId=" + impId + " ]";
    }
}
