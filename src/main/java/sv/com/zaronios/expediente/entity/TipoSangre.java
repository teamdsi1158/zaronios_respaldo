/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marlon
 */
@Entity
@Table(name = "tipo_sangre", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoSangre.findAll", query = "SELECT t FROM TipoSangre t")
    , @NamedQuery(name = "TipoSangre.findBySanId", query = "SELECT t FROM TipoSangre t WHERE t.sanId = :sanId")
    , @NamedQuery(name = "TipoSangre.findBySanDescripcion", query = "SELECT t FROM TipoSangre t WHERE t.sanDescripcion = :sanDescripcion")})
public class TipoSangre implements Serializable {
    
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "san_id", nullable = false)
    private Integer sanId;

    @Column(name = "san_descripcion", length = 10)
    private String sanDescripcion;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "sangre")
    private List<Paciente> pacienteList;

    public TipoSangre() {
    }

    public TipoSangre(Integer sanId) {
        this.sanId = sanId;
    }

    public Integer getSanId() {
        return sanId;
    }

    public void setSanId(Integer sanId) {
        this.sanId = sanId;
    }

    public String getSanDescripcion() {
        return sanDescripcion;
    }

    public void setSanDescripcion(String sanDescripcion) {
        this.sanDescripcion = sanDescripcion;
    }

    @XmlTransient
    @JsonIgnore
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sanId != null ? sanId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoSangre)) {
            return false;
        }
        TipoSangre other = (TipoSangre) object;
        if ((this.sanId == null && other.sanId != null) || (this.sanId != null && !this.sanId.equals(other.sanId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.TipoSangre[ sanId=" + sanId + " ]";
    }
    
}
