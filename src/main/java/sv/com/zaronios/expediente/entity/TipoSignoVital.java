/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "tipo_signo_vital", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoSignoVital.findAll", query = "SELECT t FROM TipoSignoVital t")
    , @NamedQuery(name = "TipoSignoVital.findByDsivId", query = "SELECT t FROM TipoSignoVital t WHERE t.dsivId = :dsivId")
    , @NamedQuery(name = "TipoSignoVital.findByDsivDescripcion", query = "SELECT t FROM TipoSignoVital t WHERE t.dsivDescripcion = :dsivDescripcion")
    , @NamedQuery(name = "TipoSignoVital.findByDsivRestriccion", query = "SELECT t FROM TipoSignoVital t WHERE t.dsivRestriccion = :dsivRestriccion")})
public class TipoSignoVital implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dsiv_id", nullable = false)
    private Integer dsivId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "dsiv_nombre", nullable = false)
    private String dsivNombre;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "dsiv_unidad", nullable = false)
    private String dsivUnidad;

    
    @Size(max = 500)
    @Column(name = "dsiv_descripcion", length = 500)
    private String dsivDescripcion;
    
    @Size(max = 100)
    @Column(name = "dsiv_restriccion", length = 100)
    private String dsivRestriccion;
    
    @JsonBackReference
    @OneToMany(mappedBy = "tipoSignoVital",fetch=FetchType.EAGER)
    private List<SignoVital> signoVitalList;

    public TipoSignoVital() {
    }

    public TipoSignoVital(Integer dsivId) {
        this.dsivId = dsivId;
    }

    public Integer getDsivId() {
        return dsivId;
    }

    public void setDsivId(Integer dsivId) {
        this.dsivId = dsivId;
    }
    
     public String getDsivNombre() {
        return dsivNombre;
    }

    public void setDsivNombre(String dsivNombre) {
        this.dsivNombre = dsivNombre;
    }

    public String getDsivDescripcion() {
        return dsivDescripcion;
    }

    public void setDsivDescripcion(String dsivDescripcion) {
        this.dsivDescripcion = dsivDescripcion;
    }

    public String getDsivRestriccion() {
        return dsivRestriccion;
    }

    public void setDsivRestriccion(String dsivRestriccion) {
        this.dsivRestriccion = dsivRestriccion;
    }
    
    
    public String getDsivUnidad() {
        return dsivUnidad;
    }

    public void setDsivUnidad(String dsivUnidad) {
        this.dsivUnidad = dsivUnidad;
    }
    
   

    @XmlTransient
    @JsonIgnore
    public List<SignoVital> getSignoVitalList() {
        return signoVitalList;
    }

    public void setSignoVitalList(List<SignoVital> signoVitalList) {
        this.signoVitalList = signoVitalList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dsivId != null ? dsivId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoSignoVital)) {
            return false;
        }
        TipoSignoVital other = (TipoSignoVital) object;
        if ((this.dsivId == null && other.dsivId != null) || (this.dsivId != null && !this.dsivId.equals(other.dsivId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.TipoSignoVital[ dsivId=" + dsivId + " ]";
    }
    
}
