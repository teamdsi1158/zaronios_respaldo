/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import org.codehaus.jackson.map.annotate.JsonView;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"proc_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Procedimiento.findAll", query = "SELECT p FROM Procedimiento p")
    , @NamedQuery(name = "Procedimiento.findByProcId", query = "SELECT p FROM Procedimiento p WHERE p.procId = :procId")
    , @NamedQuery(name = "Procedimiento.findByProcNombre", query = "SELECT p FROM Procedimiento p WHERE p.procNombre = :procNombre")
    , @NamedQuery(name = "Procedimiento.findByProcDescripcion", query = "SELECT p FROM Procedimiento p WHERE p.procDescripcion = :procDescripcion")
    , @NamedQuery(name = "Procedimiento.findByProcNumero", query = "SELECT p FROM Procedimiento p WHERE p.procNumero = :procNumero")
    , @NamedQuery(name = "Procedimiento.findByProcTrataId", query = "SELECT p FROM Procedimiento p WHERE p.trataId = :procTrataId ORDER BY p.procNumero ASC")
    , @NamedQuery(name = "Procedimiento.findByProcTratId", query = "SELECT p FROM Procedimiento p WHERE p.tratId = :procTratId ORDER BY p.procNumero ASC")})
    
public class Procedimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    
    @Basic(optional = false)
    @Column(name = "proc_id", nullable = false)
    private Integer procId;
    @Basic(optional = false)
    @Column(name = "proc_nombre", nullable = false, length = 50)
    private String procNombre;
    @Column(name = "proc_descripcion", length = 500)
    private String procDescripcion;
    @Basic(optional = false)
    @Column(name = "proc_numero", nullable = false)
    private int procNumero;
    
    
    
      @JsonBackReference
    @JoinColumn(name = "trat_id", referencedColumnName = "trat_id", insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.EAGER)
    private TipoTratamiento tratId;
    
    

     @Column(name = "trat_id")
    private Integer trataId;

    public Integer getTrataId() {
        return trataId;
    }

    public void setTrataId(Integer trataId) {
        this.trataId = trataId;
    }
    
    public Procedimiento() {
    }

    public Procedimiento(Integer procId) {
        this.procId = procId;
    }

    public Procedimiento(Integer procId, String procNombre, int procNumero) {
        this.procId = procId;
        this.procNombre = procNombre;
        this.procNumero = procNumero;
    }

    public Integer getProcId() {
        return procId;
    }

    public void setProcId(Integer procId) {
        this.procId = procId;
    }

    public String getProcNombre() {
        return procNombre;
    }

    public void setProcNombre(String procNombre) {
        this.procNombre = procNombre;
    }

    public String getProcDescripcion() {
        return procDescripcion;
    }

    public void setProcDescripcion(String procDescripcion) {
        this.procDescripcion = procDescripcion;
    }

    public int getProcNumero() {
        return procNumero;
    }

    public void setProcNumero(int procNumero) {
        this.procNumero = procNumero;
    }

    public TipoTratamiento getTratId() {
        return tratId;
    }

    public void setTratId(TipoTratamiento tratId) {
        this.tratId = tratId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (procId != null ? procId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Procedimiento)) {
            return false;
        }
        Procedimiento other = (Procedimiento) object;
        if ((this.procId == null && other.procId != null) || (this.procId != null && !this.procId.equals(other.procId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Procedimiento[ procId=" + procId + " ]";
    }
    
}