/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Estado.findAll", query = "SELECT e FROM Estado e")
    , @NamedQuery(name = "Estado.findByEstId", query = "SELECT e FROM Estado e WHERE e.estId = :estId")
    , @NamedQuery(name = "Estado.findByEstNombre", query = "SELECT e FROM Estado e WHERE e.estNombre = :estNombre")
    , @NamedQuery(name = "Estado.findByEstDescripcion", query = "SELECT e FROM Estado e WHERE e.estDescripcion = :estDescripcion")
    , @NamedQuery(name = "Estado.findByEstColor", query = "SELECT e FROM Estado e WHERE e.estColor = :estColor")})
public class Estado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "est_id", nullable = false)
    private Integer estId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "est_nombre", nullable = false, length = 20)
    private String estNombre;
    
    @Size(max = 100)
    @Column(name = "est_descripcion", length = 100)
    private String estDescripcion;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 500)
    @Column(name = "est_color", nullable = false, length = 500)
    private String estColor;

    public Estado() {
    }

    public Estado(Integer estId) {
        this.estId = estId;
    }

    public Estado(Integer estId, String estNombre, String estColor) {
        this.estId = estId;
        this.estNombre = estNombre;
        this.estColor = estColor;
    }

    public Integer getEstId() {
        return estId;
    }

    public void setEstId(Integer estId) {
        this.estId = estId;
    }

    public String getEstNombre() {
        return estNombre;
    }

    public void setEstNombre(String estNombre) {
        this.estNombre = estNombre;
    }

    public String getEstDescripcion() {
        return estDescripcion;
    }

    public void setEstDescripcion(String estDescripcion) {
        this.estDescripcion = estDescripcion;
    }

    public String getEstColor() {
        return estColor;
    }

    public void setEstColor(String estColor) {
        this.estColor = estColor;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (estId != null ? estId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estado)) {
            return false;
        }
        Estado other = (Estado) object;
        if ((this.estId == null && other.estId != null) || (this.estId != null && !this.estId.equals(other.estId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Estado[ estId=" + estId + " ]";
    }
    
}
