/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Contacto.findAll", query = "SELECT c FROM Contacto c")
    , @NamedQuery(name = "Contacto.findByConId", query = "SELECT c FROM Contacto c WHERE c.conId = :conId")
    , @NamedQuery(name = "Contacto.findByContNombre", query = "SELECT c FROM Contacto c WHERE c.contNombre = :contNombre")
    , @NamedQuery(name = "Contacto.findByContApellido", query = "SELECT c FROM Contacto c WHERE c.contApellido = :contApellido")
    , @NamedQuery(name = "Contacto.findByContTelefono", query = "SELECT c FROM Contacto c WHERE c.contTelefono = :contTelefono")})
public class Contacto implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "con_id", nullable = false)
    private Integer conId;
    
    @Column(name = "cont_nombre", length = 50)
    private String contNombre;
    
    @Column(name = "cont_apellido", length = 50)
    private String contApellido;
    
    @Column(name = "cont_telefono")
    private Integer contTelefono;
    
    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "contacto")
    private List<Persona> personaList;

    public Contacto() {
    }

    public Contacto(Integer conId) {
        this.conId = conId;
    }
    
    public String getNombreCompleto(){
        return this.contNombre + " " + this.contApellido;
    }

    public Integer getConId() {
        return conId;
    }

    public void setConId(Integer conId) {
        this.conId = conId;
    }

    public String getContNombre() {
        return contNombre;
    }

    public void setContNombre(String contNombre) {
        this.contNombre = contNombre;
    }

    public String getContApellido() {
        return contApellido;
    }

    public void setContApellido(String contApellido) {
        this.contApellido = contApellido;
    }

    public Integer getContTelefono() {
        return contTelefono;
    }

    public void setContTelefono(Integer contTelefono) {
        this.contTelefono = contTelefono;
    }

    @XmlTransient
    @JsonIgnore
    public List<Persona> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<Persona> personaList) {
        this.personaList = personaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conId != null ? conId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Contacto)) {
            return false;
        }
        Contacto other = (Contacto) object;
        if ((this.conId == null && other.conId != null) || (this.conId != null && !this.conId.equals(other.conId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Contacto[ conId=" + conId + " ]";
    }
    
}
