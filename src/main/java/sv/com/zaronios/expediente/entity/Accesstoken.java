/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Accesstoken.findAll", query = "SELECT a FROM Accesstoken a")
    , @NamedQuery(name = "Accesstoken.findById", query = "SELECT a FROM Accesstoken a WHERE a.id = :id")
    , @NamedQuery(name = "Accesstoken.findByExpiry", query = "SELECT a FROM Accesstoken a WHERE a.expiry = :expiry")
    , @NamedQuery(name = "Accesstoken.findByToken", query = "SELECT a FROM Accesstoken a WHERE a.token = :token")
    , @NamedQuery(name = "Accesstoken.findByUsr", query = "SELECT a FROM Accesstoken a WHERE a.usr = :usr")
    , @NamedQuery(name = "Accesstoken.findByFechaIngreso", query = "SELECT a FROM Accesstoken a WHERE a.fechaIngreso = :fechaIngreso")})
public class Accesstoken implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(nullable = false)
    private Long id;
    
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;
    
    @Basic(optional = false)
    @Column(nullable = false, length = 255)
    private String token;
    
    @Basic(optional = false)
    @Column(nullable = false, length = 255)
    private String usr;
    
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.DATE)
    private Date fechaIngreso;
    
     
    @Column(name = "fecha_ingreso", insertable = false, updatable = false)
    @Temporal(TemporalType.TIME)
    private Date horaIngreso;

    public Accesstoken() {
    }

    public Accesstoken(Long id) {
        this.id = id;
    }

    public Accesstoken(Long id, String token, String usr) {
        this.id = id;
        this.token = token;
        this.usr = usr;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getExpiry() {
        return expiry;
    }

    public void setExpiry(Date expiry) {
        this.expiry = expiry;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsr() {
        return usr;
    }

    public void setUsr(String usr) {
        this.usr = usr;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
    
    public Date getHoraIngreso() {
        return horaIngreso;
    }

  

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Accesstoken)) {
            return false;
        }
        Accesstoken other = (Accesstoken) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Accesstoken[ id=" + id + " ]";
    }
    
}
