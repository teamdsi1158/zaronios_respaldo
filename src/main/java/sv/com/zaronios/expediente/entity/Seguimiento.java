/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seguimiento.findAll", query = "SELECT s FROM Seguimiento s")
    , @NamedQuery(name = "Seguimiento.findBySegId", query = "SELECT s FROM Seguimiento s WHERE s.segId = :segId")
    , @NamedQuery(name = "Seguimiento.findBySegAccion", query = "SELECT s FROM Seguimiento s WHERE s.segAccion = :segAccion")
    , @NamedQuery(name = "Seguimiento.findBySegFecha", query = "SELECT s FROM Seguimiento s WHERE s.segFecha = :segFecha")
    , @NamedQuery(name = "Seguimiento.findBySegUsuario", query = "SELECT s FROM Seguimiento s WHERE s.segUsuario = :segUsuario")})
public class Seguimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "seg_id", nullable = false)
    private Integer segId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 25)
    @Column(name = "seg_accion", nullable = false, length = 25)
    private String segAccion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "seg_fecha", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date segFecha;
    
    @Column(name = "seg_fecha", insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date segDia;
    
    @Column(name = "seg_fecha", insertable = false, updatable = false)
    @Temporal(TemporalType.TIME)
    private Date segHora;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "seg_usuario", nullable = false, length = 30)
    private String segUsuario;
    @JsonIgnore
    @JoinColumn(name = "bit_id", referencedColumnName = "bit_id")
    @ManyToOne
    private Bitacora bitId;

    public Seguimiento() {
    }

    public Seguimiento(Integer segId) {
        this.segId = segId;
    }

    public Seguimiento(Integer segId, String segAccion, Date segFecha, String segUsuario) {
        this.segId = segId;
        this.segAccion = segAccion;
        this.segFecha = segFecha;
        this.segUsuario = segUsuario;
    }

    public Integer getSegId() {
        return segId;
    }

    public void setSegId(Integer segId) {
        this.segId = segId;
    }

    public String getSegAccion() {
        return segAccion;
    }

    public void setSegAccion(String segAccion) {
        this.segAccion = segAccion;
    }

    public Date getSegFecha() {
        return segFecha;
    }

    public void setSegFecha(Date segFecha) {
        this.segFecha = segFecha;
    }
    
     public Date getSegDia() {
        return segDia;
    }

    public Date getSegHora() {
        return segHora;
    }

    public String getSegUsuario() {
        return segUsuario;
    }

    public void setSegUsuario(String segUsuario) {
        this.segUsuario = segUsuario;
    }
    @JsonIgnore
    public Bitacora getBitId() {
        return bitId;
    }

    public void setBitId(Bitacora bitId) {
        this.bitId = bitId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (segId != null ? segId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seguimiento)) {
            return false;
        }
        Seguimiento other = (Seguimiento) object;
        if ((this.segId == null && other.segId != null) || (this.segId != null && !this.segId.equals(other.segId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Seguimiento[ segId=" + segId + " ]";
    }
    
}
