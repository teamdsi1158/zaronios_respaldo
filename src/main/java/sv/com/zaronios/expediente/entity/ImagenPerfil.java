/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "imagen_perfil", catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"ime_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ImagenPerfil.findAll", query = "SELECT i FROM ImagenPerfil i")
    , @NamedQuery(name = "ImagenPerfil.findByImeId", query = "SELECT i FROM ImagenPerfil i WHERE i.imeId = :imeId")})
public class ImagenPerfil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ime_id", nullable = false)
    private Integer imeId;
    
    @JoinColumn(name = "img_id", referencedColumnName = "img_id", nullable = false)
    @ManyToOne(optional = false)
    private Imagen imgId;

    
    @com.fasterxml.jackson.annotation.JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "imagen")
    private List<Empleado> empleadoList;

    
   
    
    public ImagenPerfil() {
    }

    public ImagenPerfil(Integer imeId) {
        this.imeId = imeId;
    }

    public Integer getImeId() {
        return imeId;
    }

    public void setImeId(Integer imeId) {
        this.imeId = imeId;
    }

    public Imagen getImgId() {
        return imgId;
    }

    public void setImgId(Imagen imgId) {
        this.imgId = imgId;
    }
    
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imeId != null ? imeId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ImagenPerfil)) {
            return false;
        }
        ImagenPerfil other = (ImagenPerfil) object;
        if ((this.imeId == null && other.imeId != null) || (this.imeId != null && !this.imeId.equals(other.imeId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.ImagenPerfil[ imeId=" + imeId + " ]";
    }
    
}