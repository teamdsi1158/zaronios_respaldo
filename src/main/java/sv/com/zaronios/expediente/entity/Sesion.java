/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Sesion.findAll", query = "SELECT s FROM Sesion s")
    , @NamedQuery(name = "Sesion.findBySesiId", query = "SELECT s FROM Sesion s WHERE s.sesiId = :sesiId")
    , @NamedQuery(name = "Sesion.findBySesFechaInicio", query = "SELECT s FROM Sesion s WHERE s.sesFechaInicio = :sesFechaInicio")
    , @NamedQuery(name = "Sesion.findBySesiFechaFin", query = "SELECT s FROM Sesion s WHERE s.sesiFechaFin = :sesiFechaFin")
    , @NamedQuery(name = "Sesion.findBySesiObservaciones", query = "SELECT s FROM Sesion s WHERE s.sesiObservaciones = :sesiObservaciones")
    , @NamedQuery(name = "Sesion.findBySesiRecomendaciones", query = "SELECT s FROM Sesion s WHERE s.sesiRecomendaciones = :sesiRecomendaciones")
    , @NamedQuery(name = "Sesion.findBySesiProcedimientos", query = "SELECT s FROM Sesion s WHERE s.sesiProcedimientos = :sesiProcedimientos")
    , @NamedQuery(name = "Sesion.findByCitaId", query = "SELECT s FROM Sesion s WHERE s.citaId = :citaId")
    , @NamedQuery(name = "Sesion.findByEmpId", query = "SELECT s FROM Sesion s WHERE s.empId = :empId")})
public class Sesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sesi_id", nullable = false)
    private Integer sesiId;
    @Column(name = "ses_fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sesFechaInicio;
    @Column(name = "sesi_fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sesiFechaFin;
    @Size(max = 2147483647)
    @Column(name = "sesi_observaciones", length = 2147483647)
    private String sesiObservaciones;
    @Size(max = 500)
    @Column(name = "sesi_recomendaciones", length = 500)
    private String sesiRecomendaciones;
    @Size(max = 500)
    @Column(name = "sesi_procedimientos", length = 500)
    private String sesiProcedimientos;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "cita_id", nullable = false)
    private int citaId;
    
     @JoinColumn(name = "cita_id", referencedColumnName = "cita_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cita cita;

     @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "sesion")
    private Set<SignoVital> signoVitalList;


     @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "sesion")
    private Set<ImagenPaciente> imagenPacienteList;
     
      @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "sesion")
    private Set<Examen> examenPacienteList;
    
    @Column(name = "emp_id")
    private Integer empId;

    @JoinColumn(name = "emp_id", referencedColumnName = "emp_id",insertable = false, updatable = false)
    @ManyToOne
    private Empleado empleado;

     @Basic(optional = false)
    @NotNull
    @Column(name = "asit_id", nullable = false)
    private int asitId;
    
    @JsonBackReference
    @JoinColumn(name = "asit_id", referencedColumnName = "asit_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private AsignacionTratamiento asignacion;


    public Sesion() {
    }

    public Sesion(Integer sesiId) {
        this.sesiId = sesiId;
    }

    public Sesion(Integer sesiId, int citaId, int empId) {
        this.sesiId = sesiId;
        this.citaId = citaId;
        this.empId = empId;
    }

    public Integer getSesiId() {
        return sesiId;
    }

    public void setSesiId(Integer sesiId) {
        this.sesiId = sesiId;
    }

    public Date getSesFechaInicio() {
        return sesFechaInicio;
    }

    public void setSesFechaInicio(Date sesFechaInicio) {
        this.sesFechaInicio = sesFechaInicio;
    }

    public Date getSesiFechaFin() {
        return sesiFechaFin;
    }

    public void setSesiFechaFin(Date sesiFechaFin) {
        this.sesiFechaFin = sesiFechaFin;
    }

    public String getSesiObservaciones() {
        return sesiObservaciones;
    }

    public void setSesiObservaciones(String sesiObservaciones) {
        this.sesiObservaciones = sesiObservaciones;
    }

    public String getSesiRecomendaciones() {
        return sesiRecomendaciones;
    }

    public void setSesiRecomendaciones(String sesiRecomendaciones) {
        this.sesiRecomendaciones = sesiRecomendaciones;
    }

    public String getSesiProcedimientos() {
        return sesiProcedimientos;
    }

    public void setSesiProcedimientos(String sesiProcedimientos) {
        this.sesiProcedimientos = sesiProcedimientos;
    }

    public int getCitaId() {
        return citaId;
    }

    public void setCitaId(int citaId) {
        this.citaId = citaId;
    }

    
    public Cita getCita() {
        return cita;
    }

    public void setCita(Cita cita) {
        this.cita = cita;
    }
    
   
    
    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }
    
    public Set<ImagenPaciente> getImagenPacienteList() {
        return imagenPacienteList;
    }

    public void setImagenPacienteList(Set<ImagenPaciente> imagenPacienteList) {
        this.imagenPacienteList = imagenPacienteList;
    }
     
 public Set<Examen> getExamenPacienteList() {
        return examenPacienteList;
    }

    public void setExamenPacienteList(Set<Examen> examenPacienteList) {
        this.examenPacienteList = examenPacienteList;
    }
    
    public Set<SignoVital> getSignoVitalList() {
        return signoVitalList;
    }

    public void setSignoVitalList(Set<SignoVital> signoVitalList) {
        this.signoVitalList = signoVitalList;
    }
    
    
    public int getAsitId() {
        return asitId;
    }

    public void setAsitId(int asitId) {
        this.asitId = asitId;
    }

    public AsignacionTratamiento getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(AsignacionTratamiento asignacion) {
        this.asignacion = asignacion;
    }
    
    
    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sesiId != null ? sesiId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Sesion)) {
            return false;
        }
        Sesion other = (Sesion) object;
        if ((this.sesiId == null && other.sesiId != null) || (this.sesiId != null && !this.sesiId.equals(other.sesiId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Sesion[ sesiId=" + sesiId + " ]";
    }
    
}
