/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Examen.findAll", query = "SELECT e FROM Examen e")
    , @NamedQuery(name = "Examen.findByExaId", query = "SELECT e FROM Examen e WHERE e.exaId = :exaId")
    , @NamedQuery(name = "Examen.findByExaRuta", query = "SELECT e FROM Examen e WHERE e.exaRuta = :exaRuta")
    , @NamedQuery(name = "Examen.findByExaObservacion", query = "SELECT e FROM Examen e WHERE e.exaObservacion = :exaObservacion")})
public class Examen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @NotNull
    @Column(name = "exa_id", nullable = false)
    private Integer exaId;
    
    @Size(max = 2147483647)
    @Column(name = "exa_ruta", length = 2147483647)
    private String exaRuta;
    @Size(max = 2147483647)
    @Column(name = "exa_observacion", length = 2147483647)
    private String exaObservacion;
    
    @Column(name = "sesi_id")
    private int sesiId;

    @JsonBackReference
    @JoinColumn(name = "sesi_id", referencedColumnName = "sesi_id", nullable = false,  insertable = false, updatable = false)
    @ManyToOne
    private Sesion sesion;

     @Transient 
    private String data;

    public String getData() {
        return data;
    }
    
    public Examen() {
    }

    public Examen(Integer exaId) {
        this.exaId = exaId;
    }

    public Integer getExaId() {
        return exaId;
    }

    public void setExaId(Integer exaId) {
        this.exaId = exaId;
    }

    public String getExaRuta() {
        return exaRuta;
    }

    public void setExaRuta(String exaRuta) {
        this.exaRuta = exaRuta;
    }

    public String getExaObservacion() {
        return exaObservacion;
    }

    public void setExaObservacion(String exaObservacion) {
        this.exaObservacion = exaObservacion;
    }
    
    
    public int getSesiId() {
        return sesiId;
    }

    public void setSesiId(int sesiId) {
        this.sesiId = sesiId;
    }
    
    public Sesion getSesion() {
        return sesion;
    }

    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (exaId != null ? exaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Examen)) {
            return false;
        }
        Examen other = (Examen) object;
        if ((this.exaId == null && other.exaId != null) || (this.exaId != null && !this.exaId.equals(other.exaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Examen[ exaId=" + exaId + " ]";
    }
    
}
