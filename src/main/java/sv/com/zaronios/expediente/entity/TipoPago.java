/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "tipo_pago", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoPago.findAll", query = "SELECT t FROM TipoPago t")
    , @NamedQuery(name = "TipoPago.findByTpagId", query = "SELECT t FROM TipoPago t WHERE t.tpagId = :tpagId")
    , @NamedQuery(name = "TipoPago.findByTpagNombre", query = "SELECT t FROM TipoPago t WHERE t.tpagNombre = :tpagNombre")
    , @NamedQuery(name = "TipoPago.findByTpagDescripcion", query = "SELECT t FROM TipoPago t WHERE t.tpagDescripcion = :tpagDescripcion")})
public class TipoPago implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "tpag_id", nullable = false)
    private Integer tpagId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "tpag_nombre", nullable = false, length = 50)
    private String tpagNombre;
    @Size(max = 100)
    @Column(name = "tpag_descripcion", length = 100)
    private String tpagDescripcion;
    
    @JsonBackReference
     @ElementCollection
    @OneToMany(mappedBy = "tipoPago",fetch=FetchType.EAGER)
    private Set<PagoTratamiento> pagoList;

            
    public TipoPago() {
    }

    public TipoPago(Integer tpagId) {
        this.tpagId = tpagId;
    }

    public TipoPago(Integer tpagId, String tpagNombre) {
        this.tpagId = tpagId;
        this.tpagNombre = tpagNombre;
    }

    public Integer getTpagId() {
        return tpagId;
    }

    public void setTpagId(Integer tpagId) {
        this.tpagId = tpagId;
    }

    public String getTpagNombre() {
        return tpagNombre;
    }

    public void setTpagNombre(String tpagNombre) {
        this.tpagNombre = tpagNombre;
    }

    public String getTpagDescripcion() {
        return tpagDescripcion;
    }

    public void setTpagDescripcion(String tpagDescripcion) {
        this.tpagDescripcion = tpagDescripcion;
    }
    
    
    public Set<PagoTratamiento> getPagoList() {
        return pagoList;
    }

    public void setPagoList(Set<PagoTratamiento> pagoList) {
        this.pagoList = pagoList;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tpagId != null ? tpagId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPago)) {
            return false;
        }
        TipoPago other = (TipoPago) object;
        if ((this.tpagId == null && other.tpagId != null) || (this.tpagId != null && !this.tpagId.equals(other.tpagId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.TipoPago[ tpagId=" + tpagId + " ]";
    }
    
}
