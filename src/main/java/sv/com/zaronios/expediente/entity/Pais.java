/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p")
    , @NamedQuery(name = "Pais.findByPaiCodigo", query = "SELECT p FROM Pais p WHERE p.paiCodigo = :paiCodigo")
    , @NamedQuery(name = "Pais.findByPaiNombre", query = "SELECT p FROM Pais p WHERE p.paiNombre = :paiNombre")})
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "pai_codigo", nullable = false)
    private Integer paiCodigo;
    
    @Column(name = "pai_nombre", length = 50)
    private String paiNombre;
    
    @JsonIgnore
    @OneToMany(mappedBy = "pais")
    private List<Persona> personaList;

    public Pais() {
    }

    public Pais(Integer paiCodigo) {
        this.paiCodigo = paiCodigo;
    }

    public Integer getPaiCodigo() {
        return paiCodigo;
    }

    public void setPaiCodigo(Integer paiCodigo) {
        this.paiCodigo = paiCodigo;
    }

    public String getPaiNombre() {
        return paiNombre;
    }

    public void setPaiNombre(String paiNombre) {
        this.paiNombre = paiNombre;
    }

    @XmlTransient
    @JsonIgnore
    public List<Persona> getPersonaList() {
        return personaList;
    }

    public void setPersonaList(List<Persona> personaList) {
        this.personaList = personaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (paiCodigo != null ? paiCodigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Pais)) {
            return false;
        }
        Pais other = (Pais) object;
        if ((this.paiCodigo == null && other.paiCodigo != null) || (this.paiCodigo != null && !this.paiCodigo.equals(other.paiCodigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Pais[ paiCodigo=" + paiCodigo + " ]";
    }
    
}
