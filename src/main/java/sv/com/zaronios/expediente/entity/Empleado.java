/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"emp_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e")
    , @NamedQuery(name = "Empleado.findByEmpId", query = "SELECT e FROM Empleado e WHERE e.empId = :empId")
    , @NamedQuery(name = "Empleado.findByEmpIsss", query = "SELECT e FROM Empleado e WHERE e.empIsss = :empIsss")
    , @NamedQuery(name = "Empleado.findByEmpEstado", query = "SELECT e FROM Empleado e WHERE e.empEstado = :empEstado")
    , @NamedQuery(name = "Empleado.findByImagen", query = "SELECT e FROM Empleado e WHERE e.imagen = :imagen")
    , @NamedQuery(name = "Empleado.findByPerId", query = "SELECT e FROM Empleado e WHERE e.perId = :perId")})
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "emp_id", nullable = false)
    private Integer empId;
    
    @Column(name = "emp_isss", length = 10)
    private String empIsss;
    
    @Basic(optional = false)
    @Column(name = "emp_estado", nullable = false)
    private boolean empEstado;
    
    @JoinColumn(name = "ime_id", referencedColumnName = "ime_id")
    @ManyToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
    private ImagenPerfil imagen;

   
    
   @JoinColumn(name = "per_id", referencedColumnName = "per_id", insertable = false, updatable = false)
    @ManyToOne(cascade = CascadeType.ALL,optional = false)
    private Persona persona;

    @Column(name = "per_id")
    private Integer perId;

    @JsonBackReference
    @JoinColumn(name = "usr_id", referencedColumnName = "usr_id", insertable = false, updatable = false)
    @OneToOne
    private Usuario usrId;
    
    @Column(name="usr_id")
    private Integer usuario;
    
     @com.fasterxml.jackson.annotation.JsonIgnore
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "empleado")
    private List<Usuario> usuarioList;

   

    public Empleado() {
    }

    public Empleado(Integer empId) {
        this.empId = empId;
    }

    public Empleado(Integer empId, boolean empEstado, int perId) {
        this.empId = empId;
        this.empEstado = empEstado;
        this.perId = perId;
    }

    public Integer getEmpId() {
        return empId;
    }

    public void setEmpId(Integer empId) {
        this.empId = empId;
    }

    public String getEmpIsss() {
        return empIsss;
    }

    public void setEmpIsss(String empIsss) {
        this.empIsss = empIsss;
    }

    public boolean getEmpEstado() {
        return empEstado;
    }

    public void setEmpEstado(boolean empEstado) {
        this.empEstado = empEstado;
    }

    public int getPerId() {
        return perId;
    }

    public void setPerId(int perId) {
        this.perId = perId;
    }

    public Usuario getUsrId() {
        return usrId;
    }

    public void setUsrId(Usuario usrId) {
        this.usrId = usrId;
    }
    
    public Integer getUsuario() {
        return usuario;
    }

    public void setUsuario(Integer usuario) {
        this.usuario = usuario;
    }
    
     public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }    
    
    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
     public ImagenPerfil getImagen() {
        return imagen;
    }

    public void setImagen(ImagenPerfil imagen) {
        this.imagen = imagen;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (empId != null ? empId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.empId == null && other.empId != null) || (this.empId != null && !this.empId.equals(other.empId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Empleado[ empId=" + empId + " ]";
    }
    
}
