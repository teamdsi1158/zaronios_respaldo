/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Expediente.findAll", query = "SELECT e FROM Expediente e")
    , @NamedQuery(name = "Expediente.findByExpId", query = "SELECT e FROM Expediente e WHERE e.expId = :expId")
    , @NamedQuery(name = "Expediente.findByExpCodigo", query = "SELECT e FROM Expediente e WHERE e.expCodigo = :expCodigo")
    , @NamedQuery(name = "Expediente.findByExpFechaCreacion", query = "SELECT e FROM Expediente e WHERE e.expFechaCreacion = :expFechaCreacion")
    , @NamedQuery(name = "Expediente.findByExpEstado", query = "SELECT e FROM Expediente e WHERE e.expEstado = :expEstado")})
public class Expediente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "exp_id", nullable = false)
    private Integer expId;

    @Column(name = "exp_codigo", length = 7)
    private String expCodigo;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "exp_fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expFechaCreacion;

    @Column(name = "exp_estado")
    private Boolean expEstado;

     @Column(name="pac_id")
    private Integer pacId;

   
    public Expediente() {
    }

    public Expediente(Integer expId) {
        this.expId = expId;
    }

    public Integer getExpId() {
        return expId;
    }

    public void setExpId(Integer expId) {
        this.expId = expId;
    }

    public String getExpCodigo() {
        return expCodigo;
    }

    public void setExpCodigo(String expCodigo) {
        this.expCodigo = expCodigo;
    }

    public Date getExpFechaCreacion() {
        return expFechaCreacion;
    }

    public void setExpFechaCreacion(Date expFechaCreacion) {
        this.expFechaCreacion = expFechaCreacion;
    }

    public Boolean getExpEstado() {
        return expEstado;
    }

    public void setExpEstado(Boolean expEstado) {
        this.expEstado = expEstado;
    }

    public Integer getPacId() {
        return pacId;
    }

    public void setPacId(Integer pacId) {
        this.pacId = pacId;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (expId != null ? expId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Expediente)) {
            return false;
        }
        Expediente other = (Expediente) object;
        if ((this.expId == null && other.expId != null) || (this.expId != null && !this.expId.equals(other.expId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Expediente[ expId=" + expId + " ]";
    }

}
