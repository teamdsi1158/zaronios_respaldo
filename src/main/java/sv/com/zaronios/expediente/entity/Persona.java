/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Persona.findAll", query = "SELECT p FROM Persona p")
    , @NamedQuery(name = "Persona.findByPerId", query = "SELECT p FROM Persona p WHERE p.perId = :perId")
    , @NamedQuery(name = "Persona.findByPerTelCelular", query = "SELECT p FROM Persona p WHERE p.perTelCelular = :perTelCelular")
    , @NamedQuery(name = "Persona.findByPerTelFijo", query = "SELECT p FROM Persona p WHERE p.perTelFijo = :perTelFijo")
    , @NamedQuery(name = "Persona.findByPerFechaNacimiento", query = "SELECT p FROM Persona p WHERE p.perFechaNacimiento = :perFechaNacimiento")
    , @NamedQuery(name = "Persona.findByPerSegundoNombre", query = "SELECT p FROM Persona p WHERE p.perSegundoNombre = :perSegundoNombre")
    , @NamedQuery(name = "Persona.findByPerPrimerNombre", query = "SELECT p FROM Persona p WHERE p.perPrimerNombre = :perPrimerNombre")
    , @NamedQuery(name = "Persona.findByPerPrimerApellido", query = "SELECT p FROM Persona p WHERE p.perPrimerApellido = :perPrimerApellido")
    , @NamedQuery(name = "Persona.findByPerSegundoApellido", query = "SELECT p FROM Persona p WHERE p.perSegundoApellido = :perSegundoApellido")
    , @NamedQuery(name = "Persona.findByPerApellidoCasada", query = "SELECT p FROM Persona p WHERE p.perApellidoCasada = :perApellidoCasada")
    , @NamedQuery(name = "Persona.findByPerCorreoElectronico", query = "SELECT p FROM Persona p WHERE p.perCorreoElectronico = :perCorreoElectronico")})
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "per_id", nullable = false)
    private Integer perId;

    @Column(name = "per_tel_celular")
    private Integer perTelCelular;

    @Column(name = "per_tel_fijo")
    private Integer perTelFijo;

    
    @Temporal(javax.persistence.TemporalType.DATE)
    @Column(name = "per_fecha_nacimiento")
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern="yyyy-MM-dd",timezone="America/Phoenix")
    private Date perFechaNacimiento;

    @Column(name = "per_segundo_nombre", length = 20)
    private String perSegundoNombre;

    @Column(name = "per_primer_nombre", length = 20)
    private String perPrimerNombre;

    @Column(name = "per_primer_apellido", length = 20)
    private String perPrimerApellido;

    @Column(name = "per_segundo_apellido", length = 20)
    private String perSegundoApellido;

    @Column(name = "per_apellido_casada", length = 20)
    private String perApellidoCasada;
    
    private String nombreCompleto=this.perPrimerNombre + " " + this.perSegundoNombre + " " + this.perPrimerApellido + " " + this.perSegundoApellido;

    @Column(name = "per_correo_electronico", length = 100)
    private String perCorreoElectronico;

    @Column(name = "per_direccion", length = 100)
    private String perDireccion;

    @Column(name = "per_colonia", length = 100)
    private String perColonia;

    @JoinColumn(name = "con_id", referencedColumnName = "con_id",nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional=false)
    private Contacto contacto;

    @Column(name="con_id")
    private Integer conId;

    @JoinColumn(name = "doc_id", referencedColumnName = "doc_id",nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Documento documento;
    
    @Column(name="doc_id")
    private Integer docId;

    @JoinColumn(name = "est_id", referencedColumnName = "est_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private EstadoCivil estadoCivil;

    @Column(name = "est_id")
    private Integer estId;

    @JoinColumn(name = "mun_codigo", referencedColumnName = "mun_codigo", insertable = false, updatable = false)
    @ManyToOne
    private Municipio municipio;

    @Column(name = "mun_codigo")
    private Integer munCodigo;

    @JoinColumn(name = "pai_codigo", referencedColumnName = "pai_codigo", insertable = false, updatable = false)
    @ManyToOne
    private Pais pais;

    @Column(name = "pai_codigo")
    private Integer paiCodigo;

   
    @JoinColumn(name = "sex_id", referencedColumnName = "sex_id",  insertable = false, updatable = false)
    @ManyToOne
    private Sexo sexo;

    @Column(name = "sex_id")
    private Integer sexId;

    @JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona")
    private List<Empleado> empleadoList;

     @JsonBackReference
     @ElementCollection
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "persona",fetch=FetchType.EAGER)
    private List<Paciente> pacienteList;

    public Persona() {
    }

    public Persona(Integer perId) {
        this.perId = perId;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public String getPerDireccion() {
        return perDireccion;
    }

    public void setPerDireccion(String perDireccion) {
        this.perDireccion = perDireccion;
    }

    public String getPerColonia() {
        return perColonia;
    }

    public void setPerColonia(String perColonia) {
        this.perColonia = perColonia;
    }

    public Integer getPerTelCelular() {
        return perTelCelular;
    }

    public void setPerTelCelular(Integer perTelCelular) {
        this.perTelCelular = perTelCelular;
    }

    public Integer getPerTelFijo() {
        return perTelFijo;
    }

    public void setPerTelFijo(Integer perTelFijo) {
        this.perTelFijo = perTelFijo;
    }

    public Date getPerFechaNacimiento() {
        return perFechaNacimiento;
    }

    public void setPerFechaNacimiento(Date perFechaNacimiento) {
        this.perFechaNacimiento = perFechaNacimiento;
    }

    public String getPerSegundoNombre() {
        return perSegundoNombre;
    }

    public void setPerSegundoNombre(String perSegundoNombre) {
        this.perSegundoNombre = perSegundoNombre;
    }

    public String getPerPrimerNombre() {
        return perPrimerNombre;
    }

    public void setPerPrimerNombre(String perPrimerNombre) {
        this.perPrimerNombre = perPrimerNombre;
    }

    public String getPerPrimerApellido() {
        return perPrimerApellido;
    }

    public void setPerPrimerApellido(String perPrimerApellido) {
        this.perPrimerApellido = perPrimerApellido;
    }

    public String getPerSegundoApellido() {
        return perSegundoApellido;
    }

    public void setPerSegundoApellido(String perSegundoApellido) {
        this.perSegundoApellido = perSegundoApellido;
    }

    public String getPerApellidoCasada() {
        return perApellidoCasada;
    }

    public void setPerApellidoCasada(String perApellidoCasada) {
        this.perApellidoCasada = perApellidoCasada;
    }

    public String getNombreCompleto() {
        return this.perPrimerNombre + " " + this.perSegundoNombre + " " + this.perPrimerApellido + " " + this.perSegundoApellido;
    }

    public String getPerCorreoElectronico() {
        return perCorreoElectronico;
    }

    public void setPerCorreoElectronico(String perCorreoElectronico) {
        this.perCorreoElectronico = perCorreoElectronico;
    }

  public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }
    
    public Integer getConId() {
        return conId;
    }

    public void setConId(Integer conId) {
        this.conId = conId;
    }
    

   public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }
    
     public Integer getDocId() {
        return docId;
    }

    public void setDocId(Integer docId) {
        this.docId = docId;
    }

    public EstadoCivil getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(EstadoCivil estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public Integer getEstId() {
        return estId;
    }

    public void setEstId(Integer estId) {
        this.estId = estId;
    }

    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    public Integer getMunCodigo() {
        return munCodigo;
    }

    public void setMunCodigo(Integer munCodigo) {
        this.munCodigo = munCodigo;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Integer getPaiCodigo() {
        return paiCodigo;
    }

    public void setPaiCodigo(Integer paiCodigo) {
        this.paiCodigo = paiCodigo;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Integer getSexId() {
        return sexId;
    }

    public void setSexId(Integer sexId) {
        this.sexId = sexId;
    }

    @XmlTransient
    @JsonIgnore
    public List<Empleado> getEmpleadoList() {
        return empleadoList;
    }

    public void setEmpleadoList(List<Empleado> empleadoList) {
        this.empleadoList = empleadoList;
    }

    
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (perId != null ? perId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Persona)) {
            return false;
        }
        Persona other = (Persona) object;
        if ((this.perId == null && other.perId != null) || (this.perId != null && !this.perId.equals(other.perId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Persona[ perId=" + perId + " ]";
    }

}