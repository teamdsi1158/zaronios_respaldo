/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "signo_vital", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SignoVital.findAll", query = "SELECT s FROM SignoVital s")
    , @NamedQuery(name = "SignoVital.findBySiviId", query = "SELECT s FROM SignoVital s WHERE s.siviId = :siviId")
    , @NamedQuery(name = "SignoVital.findBySiviValor", query = "SELECT s FROM SignoVital s WHERE s.siviValor = :siviValor")
    , @NamedQuery(name = "SignoVital.findBySiviDescripcion", query = "SELECT s FROM SignoVital s WHERE s.siviDescripcion = :siviDescripcion")
    , @NamedQuery(name = "SignoVital.findBySesiId", query = "SELECT s FROM SignoVital s WHERE s.sesiId = :sesiId")})
public class SignoVital implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sivi_id", nullable = false)
    private Integer siviId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "sivi_valor", nullable = false)
    private BigDecimal siviValor;
    
    @Size(max = 500)
    @Column(name="sivi_descripcion",length = 500)
    private String siviDescripcion;

    
    @Column(name = "sesi_id")
    private Integer sesiId;
    
     @JsonBackReference
    @JoinColumn(name = "sesi_id", referencedColumnName = "sesi_id", nullable = false,  insertable = false, updatable = false)
    @ManyToOne
    private Sesion sesion;

    @JoinColumn(name = "dsiv_id", referencedColumnName = "dsiv_id")
    @ManyToOne
    private TipoSignoVital tipoSignoVital;



    public SignoVital() {
    }

    public SignoVital(Integer siviId) {
        this.siviId = siviId;
    }

    public SignoVital(Integer siviId, BigDecimal siviValor) {
        this.siviId = siviId;
        this.siviValor = siviValor;
    }

    public Integer getSiviId() {
        return siviId;
    }

    public void setSiviId(Integer siviId) {
        this.siviId = siviId;
    }

    public BigDecimal getSiviValor() {
        return siviValor;
    }

    public void setSiviValor(BigDecimal siviValor) {
        this.siviValor = siviValor;
    }

    
    public Integer getSesiId() {
        return sesiId;
    }

    public void setSesiId(Integer sesiId) {
        this.sesiId = sesiId;
    }

    
    public String getSiviDescripcion() {
        return siviDescripcion;
    }

    public void setSiviDescripcion(String siviDescripcion) {
        this.siviDescripcion = siviDescripcion;
    }
    
    
    public Sesion getSesion() {
        return sesion;
    }

    public void setSesion(Sesion sesion) {
        this.sesion = sesion;
    }
    
    
    public TipoSignoVital getTipoSignoVital() {
        return tipoSignoVital;
    }

    public void setTipoSignoVital(TipoSignoVital tipoSignoVital) {
        this.tipoSignoVital = tipoSignoVital;
    }
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siviId != null ? siviId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SignoVital)) {
            return false;
        }
        SignoVital other = (SignoVital) object;
        if ((this.siviId == null && other.siviId != null) || (this.siviId != null && !this.siviId.equals(other.siviId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.SignoVital[ siviId=" + siviId + " ]";
    }
    
}
