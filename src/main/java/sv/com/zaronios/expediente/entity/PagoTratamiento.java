/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "pago_tratamiento", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PagoTratamiento.findAll", query = "SELECT p FROM PagoTratamiento p")
    , @NamedQuery(name = "PagoTratamiento.findByPagId", query = "SELECT p FROM PagoTratamiento p WHERE p.pagId = :pagId")
    , @NamedQuery(name = "PagoTratamiento.findByPagMonto", query = "SELECT p FROM PagoTratamiento p WHERE p.pagMonto = :pagMonto")
    , @NamedQuery(name = "PagoTratamiento.findByPagDescripcion", query = "SELECT p FROM PagoTratamiento p WHERE p.pagDescripcion = :pagDescripcion")
    , @NamedQuery(name = "PagoTratamiento.findByPagFecha", query = "SELECT p FROM PagoTratamiento p WHERE p.pagFecha = :pagFecha")})
public class PagoTratamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pag_id", nullable = false)
    private Integer pagId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "pag_monto", nullable = false)
    private BigDecimal pagMonto;
    
    @Size(max = 500)
    @Column(name = "pag_descripcion", length = 500)
    private String pagDescripcion;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "pag_fecha", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date pagFecha;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "asit_id", nullable = false)
    private int asitId;

    
    @Basic(optional = false)
    @NotNull
    @Column(name = "tpag_id", nullable = false)
    private int tpagId;

    @JoinColumn(name = "tpag_id", referencedColumnName = "tpag_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.EAGER)
    private TipoPago tipoPago;

    
    @JsonBackReference
    @JoinColumn(name = "asit_id", referencedColumnName = "asit_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(fetch=FetchType.EAGER)
    private AsignacionTratamiento asignacion;
    
    @Size(max = 25)
    @Column(name = "usr_nombre", length = 25)
    private String usrNombre;

    @Column(name = "pag_modificacion",columnDefinition= "TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pagModificacion;

    public PagoTratamiento() {
    }

    public PagoTratamiento(Integer pagId) {
        this.pagId = pagId;
    }

    public PagoTratamiento(Integer pagId, BigDecimal pagMonto, Date pagFecha) {
        this.pagId = pagId;
        this.pagMonto = pagMonto;
        this.pagFecha = pagFecha;
    }

    public Integer getPagId() {
        return pagId;
    }

    public void setPagId(Integer pagId) {
        this.pagId = pagId;
    }

    public BigDecimal getPagMonto() {
        return pagMonto;
    }

    public void setPagMonto(BigDecimal pagMonto) {
        this.pagMonto = pagMonto;
    }

    public String getPagDescripcion() {
        return pagDescripcion;
    }

    public void setPagDescripcion(String pagDescripcion) {
        this.pagDescripcion = pagDescripcion;
    }

    public Date getPagFecha() {
        return pagFecha;
    }

    public void setPagFecha(Date pagFecha) {
        this.pagFecha = pagFecha;
    }

    
    public int getAsitId() {
        return asitId;
    }

    public void setAsitId(int asitId) {
        this.asitId = asitId;
    }

    public AsignacionTratamiento getAsignacion() {
        return asignacion;
    }

    public void setAsignacion(AsignacionTratamiento asignacion) {
        this.asignacion = asignacion;
    }
    
    
    public int getTpagId() {
        return tpagId;
    }

    public void setTpagId(int tpagId) {
        this.tpagId = tpagId;
    }
    
    
    public TipoPago getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }
    
    public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }

    public Date getPagModificacion() {
        return pagModificacion;
    }

    public void setPagModificacion(Date pagModificacion) {
        this.pagModificacion = pagModificacion;
    }

    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pagId != null ? pagId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PagoTratamiento)) {
            return false;
        }
        PagoTratamiento other = (PagoTratamiento) object;
        if ((this.pagId == null && other.pagId != null) || (this.pagId != null && !this.pagId.equals(other.pagId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.PagoTratamiento[ pagId=" + pagId + " ]";
    }
    
}
