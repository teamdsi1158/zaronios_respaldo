/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cita.findAll", query = "SELECT c FROM Cita c")
    , @NamedQuery(name = "Cita.findByCitaId", query = "SELECT c FROM Cita c WHERE c.citaId = :citaId")
    , @NamedQuery(name = "Cita.findByCitaFecha", query = "SELECT c FROM Cita c WHERE c.citaFecha = :citaFecha")})
public class Cita implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "cita_id", nullable = false)
    private Integer citaId;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "cita_fecha", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date citaFecha;
    
     @Column(name = "cita_fecha", nullable = false, insertable = false, updatable = false)
    @Temporal(TemporalType.DATE)
    private Date citaDia;

      @Column(name = "cita_fecha", nullable = false, insertable = false, updatable = false)
    @Temporal(TemporalType.TIME)
    private Date citaHora;
        
      @Basic(optional = false)
    @NotNull
    @Column(name = "est_id", nullable = false)
    private int estId;
      
       @JoinColumn(name = "est_id", referencedColumnName = "est_id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estado estado;

      
     @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "citaId")
    private List<Sesion> sesionList;

    public Cita() {
    }

    public Cita(Integer citaId) {
        this.citaId = citaId;
    }

    public Cita(Integer citaId, Date citaFecha) {
        this.citaId = citaId;
        this.citaFecha = citaFecha;
    }

    public Integer getCitaId() {
        return citaId;
    }

    public void setCitaId(Integer citaId) {
        this.citaId = citaId;
    }

    public Date getCitaFecha() {
        return citaFecha;
    }

    public void setCitaFecha(Date citaFecha) {
        this.citaFecha = citaFecha;
    }

    
    public Date getCitaDia() {
        return citaDia;
    }

    public void setCitaDia(Date citaDia) {
        this.citaDia = citaDia;
    }

    public Date getCitaHora() {
        return citaHora;
    }

    public void setCitaHora(Date citaHora) {
        this.citaHora = citaHora;
    }
    
    
    public int getEstId() {
        return estId;
    }

    public void setEstId(int estId) {
        this.estId = estId;
    }
    
    
    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    

    @XmlTransient
    @JsonIgnore
    public List<Sesion> getSesionList() {
        return sesionList;
    }

    public void setSesionList(List<Sesion> sesionList) {
        this.sesionList = sesionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (citaId != null ? citaId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cita)) {
            return false;
        }
        Cita other = (Cita) object;
        if ((this.citaId == null && other.citaId != null) || (this.citaId != null && !this.citaId.equals(other.citaId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Cita[ citaId=" + citaId + " ]";
    }
    
}
