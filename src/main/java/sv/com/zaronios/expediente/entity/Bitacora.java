/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Bitacora.findAll", query = "SELECT b FROM Bitacora b")
    , @NamedQuery(name = "Bitacora.findByBitId", query = "SELECT b FROM Bitacora b WHERE b.bitId = :bitId")
    , @NamedQuery(name = "Bitacora.findByBitTabla", query = "SELECT b FROM Bitacora b WHERE b.bitTabla = :bitTabla")
    , @NamedQuery(name = "Bitacora.findByBitCreacion", query = "SELECT b FROM Bitacora b WHERE b.bitCreacion = :bitCreacion")
    , @NamedQuery(name = "Bitacora.findByIdEntidad", query = "SELECT b FROM Bitacora b WHERE b.idEntidad = :idEntidad")})
public class Bitacora implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "bit_id", nullable = false)
    private Integer bitId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "bit_tabla", nullable = false, length = 20)
    private String bitTabla;
    @Basic(optional = false)
    @NotNull
    @Column(name = "bit_creacion", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date bitCreacion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "id_entidad", nullable = false)
    private int idEntidad;
    @OneToMany(mappedBy = "bitId",fetch=FetchType.EAGER)
    private Set<Seguimiento> seguimientoSet;

    public Bitacora() {
    }

    public Bitacora(Integer bitId) {
        this.bitId = bitId;
    }

    public Bitacora(Integer bitId, String bitTabla, Date bitCreacion, int idEntidad) {
        this.bitId = bitId;
        this.bitTabla = bitTabla;
        this.bitCreacion = bitCreacion;
        this.idEntidad = idEntidad;
    }

    public Integer getBitId() {
        return bitId;
    }

    public void setBitId(Integer bitId) {
        this.bitId = bitId;
    }

    public String getBitTabla() {
        return bitTabla;
    }

    public void setBitTabla(String bitTabla) {
        this.bitTabla = bitTabla;
    }

    public Date getBitCreacion() {
        return bitCreacion;
    }

    public void setBitCreacion(Date bitCreacion) {
        this.bitCreacion = bitCreacion;
    }

    public int getIdEntidad() {
        return idEntidad;
    }

    public void setIdEntidad(int idEntidad) {
        this.idEntidad = idEntidad;
    }

    @XmlTransient
    @JsonIgnore
    public Set<Seguimiento> getSeguimientoSet() {
        return seguimientoSet;
    }

    public void setSeguimientoSet(Set<Seguimiento> seguimientoSet) {
        this.seguimientoSet = seguimientoSet;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (bitId != null ? bitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Bitacora)) {
            return false;
        }
        Bitacora other = (Bitacora) object;
        if ((this.bitId == null && other.bitId != null) || (this.bitId != null && !this.bitId.equals(other.bitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Bitacora[ bitId=" + bitId + " ]";
    }
    
}
