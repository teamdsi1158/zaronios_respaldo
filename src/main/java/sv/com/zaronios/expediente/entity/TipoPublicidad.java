/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.hibernate.Query;

/**
 *
 * @author marlon
 */
@Entity
@Table(name = "tipo_publicidad", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoPublicidad.findAll", query = "SELECT t FROM TipoPublicidad t")
    , @NamedQuery(name = "TipoPublicidad.findByPubId", query = "SELECT t FROM TipoPublicidad t WHERE t.pubId = :pubId")
    , @NamedQuery(name = "TipoPublicidad.findByPubNombre", query = "SELECT t FROM TipoPublicidad t WHERE t.pubNombre = :pubNombre")
    , @NamedQuery(name = "TipoPublicidad.findByPubDescripcion", query = "SELECT t FROM TipoPublicidad t WHERE t.pubDescripcion = :pubDescripcion")
    , @NamedQuery(name = "TipoPublicidad.findByPubFechaCreacion", query = "SELECT t FROM TipoPublicidad t WHERE t.pubFechaCreacion = :pubFechaCreacion")
    , @NamedQuery(name = "TipoPublicidad.findByPubEstado", query = "SELECT t FROM TipoPublicidad t WHERE t.pubEstado = :pubEstado")})
    
public class TipoPublicidad implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pub_id", nullable = false)
    private Integer pubId;
    
    @Column(name = "pub_nombre", length = 50)
    private String pubNombre;
    
    @Column(name = "pub_descripcion", length = 100)
    private String pubDescripcion;
    
    @JsonFormat(pattern = "dd/MM/yyyy") 
    @Column(name = "pub_fecha_creacion")
    private Date pubFechaCreacion;
    
    @Column(name = "pub_estado")
    private Boolean pubEstado;
    
     @JsonBackReference
     @ElementCollection
    @OneToMany(mappedBy = "pubId",fetch=FetchType.EAGER)
    private List<Paciente> pacienteList;
    
    @Transient
    private Integer count;

    public TipoPublicidad() {
    }

    public TipoPublicidad(Integer pubId) {
        this.pubId = pubId;
    }

    public Integer getPubId() {
        return pubId;
    }

    public void setPubId(Integer pubId) {
        this.pubId = pubId;
    }

    public String getPubNombre() {
        return pubNombre;
    }

    public void setPubNombre(String pubNombre) {
        this.pubNombre = pubNombre;
    }

    public String getPubDescripcion() {
        return pubDescripcion;
    }

    public void setPubDescripcion(String pubDescripcion) {
        this.pubDescripcion = pubDescripcion;
    }

    public Date getPubFechaCreacion() {
        return pubFechaCreacion;
    }

    public void setPubFechaCreacion(Date pubFechaCreacion) {
        this.pubFechaCreacion = pubFechaCreacion;
    }

    public Boolean getPubEstado() {
        return pubEstado;
    }

    public void setPubEstado(Boolean pubEstado) {
        this.pubEstado = pubEstado;
    }
   
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pubId != null ? pubId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoPublicidad)) {
            return false;
        }
        TipoPublicidad other = (TipoPublicidad) object;
        if ((this.pubId == null && other.pubId != null) || (this.pubId != null && !this.pubId.equals(other.pubId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.TipoPublicidad[ pubId=" + pubId + " ]";
    }

}
