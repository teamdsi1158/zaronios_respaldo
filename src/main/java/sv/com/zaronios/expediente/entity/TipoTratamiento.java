/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "tipo_tratamiento", catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"trat_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoTratamiento.findAll", query = "SELECT t FROM TipoTratamiento t")
    , @NamedQuery(name = "TipoTratamiento.findByTratId", query = "SELECT t FROM TipoTratamiento t WHERE t.tratId = :tratId")
    , @NamedQuery(name = "TipoTratamiento.findByTratNombre", query = "SELECT t FROM TipoTratamiento t WHERE t.tratNombre = :tratNombre")
    , @NamedQuery(name = "TipoTratamiento.findByTratDescripcion", query = "SELECT t FROM TipoTratamiento t WHERE t.tratDescripcion = :tratDescripcion")
    , @NamedQuery(name = "TipoTratamiento.findByTratEstado", query = "SELECT t FROM TipoTratamiento t WHERE t.tratEstado = :tratEstado")})
public class TipoTratamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "trat_id", nullable = false)
    private Integer tratId;
    @Basic(optional = false)
    @Column(name = "trat_nombre", nullable = false, length = 50)
    private String tratNombre;
    @Basic(optional = false)
    @Column(name = "trat_descripcion", nullable = false, length = 500)
    private String tratDescripcion;
    @Basic(optional = false)
    @Column(name = "trat_estado", nullable = false)
    private boolean tratEstado;
    
    
    @Column(name = "trat_cirugia", nullable = false)
    private boolean tratCirugia;

    
    @JsonBackReference
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tratamiento",fetch=FetchType.EAGER)
    private Set<AsignacionTratamiento> asignacionTratamientoList;

    
    @JsonManagedReference
    @ElementCollection
    @JoinColumn(name="trat_id")
    @OneToMany(fetch=FetchType.EAGER)
    @OrderBy("procNumero ASC")
    private Set<Procedimiento> procedimientoList;

    public TipoTratamiento() {
    }

    public TipoTratamiento(Integer tratId) {
        this.tratId = tratId;
    }

    public TipoTratamiento(Integer tratId, String tratNombre, String tratDescripcion, boolean tratEstado) {
        this.tratId = tratId;
        this.tratNombre = tratNombre;
        this.tratDescripcion = tratDescripcion;
        this.tratEstado = tratEstado;
    }

    public Integer getTratId() {
        return tratId;
    }

    public void setTratId(Integer tratId) {
        this.tratId = tratId;
    }

    public String getTratNombre() {
        return tratNombre;
    }

    public void setTratNombre(String tratNombre) {
        this.tratNombre = tratNombre;
    }

    public String getTratDescripcion() {
        return tratDescripcion;
    }

    public void setTratDescripcion(String tratDescripcion) {
        this.tratDescripcion = tratDescripcion;
    }

    public boolean getTratEstado() {
        return tratEstado;
    }

    public void setTratEstado(boolean tratEstado) {
        this.tratEstado = tratEstado;
    }
    
    
    public boolean getTratCirugia() {
        return tratCirugia;
    }

    public void setTratCirugia(boolean tratCirugia) {
        this.tratCirugia = tratCirugia;
    }
    
    
    public Set<AsignacionTratamiento> getAsignacionTratamientoList() {
        return asignacionTratamientoList;
    }

    public void setAsignacionTratamientoList(Set<AsignacionTratamiento> asignacionTratamientoList) {
        this.asignacionTratamientoList = asignacionTratamientoList;
    }

    
    public Set<Procedimiento> getProcedimientoList() {
        return procedimientoList;
    }

    public void setProcedimientoList(Set<Procedimiento> procedimientoList) {
        this.procedimientoList = procedimientoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (tratId != null ? tratId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoTratamiento)) {
            return false;
        }
        TipoTratamiento other = (TipoTratamiento) object;
        if ((this.tratId == null && other.tratId != null) || (this.tratId != null && !this.tratId.equals(other.tratId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.TipoTratamiento[ tratId=" + tratId + " ]";
    }
    
}