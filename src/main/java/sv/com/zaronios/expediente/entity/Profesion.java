/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Profesion.findAll", query = "SELECT p FROM Profesion p")
    , @NamedQuery(name = "Profesion.findByProfId", query = "SELECT p FROM Profesion p WHERE p.profId = :profId")
    , @NamedQuery(name = "Profesion.findByProfNombre", query = "SELECT p FROM Profesion p WHERE p.profNombre = :profNombre")
    , @NamedQuery(name = "Profesion.findByProfLugarDeTrabajo", query = "SELECT p FROM Profesion p WHERE p.profLugarDeTrabajo = :profLugarDeTrabajo")})
public class Profesion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "prof_id", nullable = false)
    private Integer profId;
    
    @Column(name = "prof_nombre", length = 50)
    private String profNombre;
    
    @Column(name = "prof_lugar_de_trabajo", length = 50)
    private String profLugarDeTrabajo;
    
    @com.fasterxml.jackson.annotation.JsonIgnore
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "profesion")
    private List<Paciente> pacienteList;

    public Profesion() {
    }

    public Profesion(Integer profId) {
        this.profId = profId;
    }

    public Integer getProfId() {
        return profId;
    }

    public void setProfId(Integer profId) {
        this.profId = profId;
    }

    public String getProfNombre() {
        return profNombre;
    }

    public void setProfNombre(String profNombre) {
        this.profNombre = profNombre;
    }

    public String getProfLugarDeTrabajo() {
        return profLugarDeTrabajo;
    }

    public void setProfLugarDeTrabajo(String profLugarDeTrabajo) {
        this.profLugarDeTrabajo = profLugarDeTrabajo;
    }

    @XmlTransient
    @JsonIgnore
    public List<Paciente> getPacienteList() {
        return pacienteList;
    }

    public void setPacienteList(List<Paciente> pacienteList) {
        this.pacienteList = pacienteList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (profId != null ? profId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Profesion)) {
            return false;
        }
        Profesion other = (Profesion) object;
        if ((this.profId == null && other.profId != null) || (this.profId != null && !this.profId.equals(other.profId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Profesion[ profId=" + profId + " ]";
    }
    
}
