/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Menu.findAll", query = "SELECT m FROM Menu m")
    , @NamedQuery(name = "Menu.findByMenId", query = "SELECT m FROM Menu m WHERE m.menId = :menId")
    , @NamedQuery(name = "Menu.findByMenNombre", query = "SELECT m FROM Menu m WHERE m.menNombre = :menNombre")
    , @NamedQuery(name = "Menu.findByMenRuta", query = "SELECT m FROM Menu m WHERE m.menRuta = :menRuta")
    , @NamedQuery(name = "Menu.findByMenIcono", query = "SELECT m FROM Menu m WHERE m.menIcono = :menIcono")
    , @NamedQuery(name = "Menu.findByOrden", query = "SELECT m FROM Menu m WHERE m.orden = :orden")})
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "men_id", nullable = false)
    private Integer menId;

    @Column(name = "men_nombre", length = 50)
    private String menNombre;

    @Column(name = "men_ruta", length = 100)
    private String menRuta;

    @Column(name = "men_icono")
    private String menIcono;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "nivel")
    private Integer nivel;

    @Column(name = "men_id_padre")
    private Integer menIdPadre;

    @JsonBackReference
    @ManyToMany(mappedBy = "menuSet")
    private Set<Rol> rolSet;
    
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "idPadre",fetch = FetchType.EAGER)
    @OrderBy("orden")
    private List<Menu> menuList;

    @JoinColumn(name = "men_id_padre", referencedColumnName = "men_id",  insertable = false, updatable = false)
    @com.fasterxml.jackson.annotation.JsonIgnore
    @ManyToOne
    private Menu idPadre;
    
    @OneToMany(cascade = CascadeType.ALL,mappedBy = "menu",fetch = FetchType.EAGER)
    private List<Permiso> permisoList;


    
    public Menu() {
    }

    public Menu(Integer menId) {
        this.menId = menId;
    }

    public Integer getMenId() {
        return menId;
    }

    public void setMenId(Integer menId) {
        this.menId = menId;
    }

    public String getMenNombre() {
        return menNombre;
    }

    public void setMenNombre(String menNombre) {
        this.menNombre = menNombre;
    }

    public String getMenRuta() {
        return menRuta;
    }

    public void setMenRuta(String menRuta) {
        this.menRuta = menRuta;
    }

    public Set<Rol> getRolSet() {
        return rolSet;
    }

    public void setRolSet(Set<Rol> rolSet) {
        this.rolSet = rolSet;
    }

    public Integer getMenIdPadre() {
        return menIdPadre;
    }

    public void setMenIdPadre(Integer menIdPadre) {
        this.menIdPadre = menIdPadre;
    }

    public String getMenIcono() {
        return menIcono;
    }

    public void setMenIcono(String menIcono) {
        this.menIcono = menIcono;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }
    
    
    public List<Menu> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<Menu> menuList) {
        this.menuList = menuList;
    }

    public Menu getIdPadre() {
        return idPadre;
    }

    public void setIdPadre(Menu idPadre) {
        this.idPadre = idPadre;
    }
    
    public List<Permiso> getPermisoList() {
        return permisoList;
    }

    public void setPermisoList(List<Permiso> permisoList) {
        this.permisoList = permisoList;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (menId != null ? menId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Menu)) {
            return false;
        }
        Menu other = (Menu) object;
        if ((this.menId == null && other.menId != null) || (this.menId != null && !this.menId.equals(other.menId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Menu[ menId=" + menId + " ]";
    }

}
