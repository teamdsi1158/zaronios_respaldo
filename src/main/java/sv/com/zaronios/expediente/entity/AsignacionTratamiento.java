/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import org.codehaus.jackson.annotate.JsonIgnore;

/**
 *
 * @author mata
 */
@Entity
@Table(name = "asignacion_tratamiento", catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "AsignacionTratamiento.findAll", query = "SELECT a FROM AsignacionTratamiento a")
    , @NamedQuery(name = "AsignacionTratamiento.findByAsitId", query = "SELECT a FROM AsignacionTratamiento a WHERE a.asitId = :asitId")
    , @NamedQuery(name = "AsignacionTratamiento.findByAsitPlan", query = "SELECT a FROM AsignacionTratamiento a WHERE a.asitPlan = :asitPlan")
    , @NamedQuery(name = "AsignacionTratamiento.findByAsitFinalizado", query = "SELECT a FROM AsignacionTratamiento a WHERE a.asitFinalizado = :asitFinalizado")
    , @NamedQuery(name = "AsignacionTratamiento.findByAsitPrecio", query = "SELECT a FROM AsignacionTratamiento a WHERE a.asitPrecio = :asitPrecio")
    , @NamedQuery(name = "AsignacionTratamiento.findByAsitRecomendaciones", query = "SELECT a FROM AsignacionTratamiento a WHERE a.asitRecomendaciones = :asitRecomendaciones")
    , @NamedQuery(name = "AsignacionTratamiento.findByExpId", query = "SELECT a FROM AsignacionTratamiento a WHERE a.expId = :expId")})
public class AsignacionTratamiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "asit_id", nullable = false)
    private Integer asitId;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "asit_plan", nullable = false, length = 2147483647)
    private String asitPlan;
    
    @Column(name="asit_estimacion")
    private Integer asitEstimacion;

    
    @Column(name = "asit_finalizado")
    private Boolean asitFinalizado;
    
    @Column(name = "asit_precio")
    private BigDecimal asitPrecio;
    
    @Size(max = 500)
    @Column(name = "asit_recomendaciones", length = 500)
    private String asitRecomendaciones;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "exp_id", nullable = false)
    private int expId;
    
     @Basic(optional = false)
    @NotNull
    @Column(name = "trat_id", nullable = false)
    private int tratId;

    @JoinColumn(name = "exp_id", referencedColumnName = "exp_id",   insertable = false, updatable = false)
    @ManyToOne
    private Expediente expediente;

    
    @JsonManagedReference
    @OneToMany(fetch=FetchType.EAGER,mappedBy = "asignacion")
    private Set<PagoTratamiento> pagoTratamientoList;
    
    @JsonManagedReference
    @OrderBy("cita ASC")
    @OneToMany(fetch=FetchType.EAGER,cascade = CascadeType.ALL, mappedBy = "asignacion")
    private Set<Sesion> sesionList;

   
    
    @JoinColumn(name = "trat_id", referencedColumnName = "trat_id", insertable = false, updatable = false)
    @ManyToOne
    private TipoTratamiento tratamiento;

     
    
    public AsignacionTratamiento() {
    }

    public AsignacionTratamiento(Integer asitId) {
        this.asitId = asitId;
    }

    public AsignacionTratamiento(Integer asitId, String asitPlan, int expId) {
        this.asitId = asitId;
        this.asitPlan = asitPlan;
        this.expId = expId;
    }

    public Integer getAsitId() {
        return asitId;
    }

    public void setAsitId(Integer asitId) {
        this.asitId = asitId;
    }

    public String getAsitPlan() {
        return asitPlan;
    }
    
    public void setAsitPlan(String asitPlan) {
        this.asitPlan = asitPlan;
    }

    public Boolean getAsitFinalizado() {
        return asitFinalizado;
    }

    public void setAsitFinalizado(Boolean asitFinalizado) {
        this.asitFinalizado = asitFinalizado;
    }

    public BigDecimal getAsitPrecio() {
        return asitPrecio;
    }

    public void setAsitPrecio(BigDecimal asitPrecio) {
        this.asitPrecio = asitPrecio;
    }
    
    
    public Integer getAsitEstimacion() {
        return asitEstimacion;
    }

    public void setAsitEstimacion(Integer asitEstimacion) {
        this.asitEstimacion = asitEstimacion;
    }

    
    public TipoTratamiento getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(TipoTratamiento tratamiento) {
        this.tratamiento = tratamiento;
    }


    public String getAsitRecomendaciones() {
        return asitRecomendaciones;
    }

    public void setAsitRecomendaciones(String asitRecomendaciones) {
        this.asitRecomendaciones = asitRecomendaciones;
    }

    public int getExpId() {
        return expId;
    }

    public void setExpId(int expId) {
        this.expId = expId;
    }
    
    
    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    
    public Set<PagoTratamiento> getPagoTratamientoList() {
        return pagoTratamientoList;
    }

    public void setPagoTratamientoList(Set<PagoTratamiento> pagoTratamientoList) {
        this.pagoTratamientoList = pagoTratamientoList;
    }

    
    public int getTratId() {
        return tratId;
    }

    public void setTratId(int tratId) {
        this.tratId = tratId;
    }
    

    @XmlTransient
    @JsonIgnore
    public Set<Sesion> getSesionList() {
        return sesionList;
    }

    public void setSesionList(Set<Sesion> sesionList) {
        this.sesionList = sesionList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (asitId != null ? asitId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AsignacionTratamiento)) {
            return false;
        }
        AsignacionTratamiento other = (AsignacionTratamiento) object;
        if ((this.asitId == null && other.asitId != null) || (this.asitId != null && !this.asitId.equals(other.asitId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.AsignacionTratamiento[ asitId=" + asitId + " ]";
    }
    
}