/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author mata
 */
@Entity
@Table(catalog = "expediente", schema = "public", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"img_id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Imagen.findAll", query = "SELECT i FROM Imagen i")
    , @NamedQuery(name = "Imagen.findByImgId", query = "SELECT i FROM Imagen i WHERE i.imgId = :imgId")
    , @NamedQuery(name = "Imagen.findByImgRuta", query = "SELECT i FROM Imagen i WHERE i.imgRuta = :imgRuta")
    , @NamedQuery(name = "Imagen.findByImgNombre", query = "SELECT i FROM Imagen i WHERE i.imgNombre = :imgNombre")})
public class Imagen implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "img_id", nullable = false)
    private Integer imgId;
    @Basic(optional = false)
    @Column(name = "img_ruta", nullable = false, length = 500)
    private String imgRuta;
    @Basic(optional = false)
    @Column(name = "img_nombre", nullable = false, length = 100)
    private String imgNombre;

    @Transient 
    private String data;

    public String getData() {
        return data;
    }
    
    public Imagen() {
    }

    public Imagen(Integer imgId) {
        this.imgId = imgId;
    }

    public Imagen(Integer imgId, String imgRuta, String imgNombre) {
        this.imgId = imgId;
        this.imgRuta = imgRuta;
        this.imgNombre = imgNombre;
    }

    public Integer getImgId() {
        return imgId;
    }

    public void setImgId(Integer imgId) {
        this.imgId = imgId;
    }

    public String getImgRuta() {
        return imgRuta;
    }

    public void setImgRuta(String imgRuta) {
        this.imgRuta = imgRuta;
    }

    public String getImgNombre() {
        return imgNombre;
    }

    public void setImgNombre(String imgNombre) {
        this.imgNombre = imgNombre;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (imgId != null ? imgId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Imagen)) {
            return false;
        }
        Imagen other = (Imagen) object;
        if ((this.imgId == null && other.imgId != null) || (this.imgId != null && !this.imgId.equals(other.imgId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "sv.com.zaronios.expediente.entity.Imagen[ imgId=" + imgId + " ]";
    }
    
}
