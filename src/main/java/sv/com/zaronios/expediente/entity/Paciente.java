/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author marlon
 */
@Entity
@Table(catalog = "expediente", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Paciente.findAll", query = "SELECT p FROM Paciente p")
    , @NamedQuery(name = "Paciente.findByPacId", query = "SELECT p FROM Paciente p WHERE p.pacId = :pacId")
    , @NamedQuery(name = "Paciente.findByPacAntecedentesPersonales", query = "SELECT p FROM Paciente p WHERE p.pacAntecedentesPersonales = :pacAntecedentesPersonales")
    , @NamedQuery(name = "Paciente.findByPacAntecedentesMedicos", query = "SELECT p FROM Paciente p WHERE p.pacAntecedentesMedicos = :pacAntecedentesMedicos")
    , @NamedQuery(name = "Paciente.findByPacEstado", query = "SELECT p FROM Paciente p WHERE p.pacEstado = :pacEstado")
    , @NamedQuery(name = "Paciente.findByPacModificacion", query = "SELECT p FROM Paciente p WHERE p.pacModificacion = :pacModificacion")
    , @NamedQuery(name = "Paciente.findByUsrNombre", query = "SELECT p FROM Paciente p WHERE p.usrNombre = :usrNombre")
    , @NamedQuery(name = "Paciente.findByExpId", query = "SELECT p FROM Paciente p WHERE p.expId = :expId")
    , @NamedQuery(name = "Paciente.findByPerId", query = "SELECT p FROM Paciente p WHERE p.perId = :perId")
    , @NamedQuery(name = "Paciente.findByProfId", query = "SELECT p FROM Paciente p WHERE p.profId = :profId")
    , @NamedQuery(name = "Paciente.findByPubId", query = "SELECT p FROM Paciente p WHERE p.pubId = :pubId")
    , @NamedQuery(name = "Paciente.findBySanId", query = "SELECT p FROM Paciente p WHERE p.sanId = :sanId")})
public class Paciente implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "pac_id", nullable = false)
    private Integer pacId;

    @Column(name = "pac_antecedentes_personales", length = 500)
    private String pacAntecedentesPersonales;
    
    @Column(name = "pac_antecedentes_medicos", length = 500)
    private String pacAntecedentesMedicos;

    @Column(name = "pac_estado")
    private Boolean pacEstado;

    @JoinColumn(name = "exp_id", referencedColumnName = "exp_id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Expediente expediente;

    @Column(name = "exp_id")
    private Integer expId;

    @JoinColumn(name = "per_id", referencedColumnName = "per_id", insertable = false, updatable = false)
    @ManyToOne(cascade = CascadeType.ALL,optional = false)
    private Persona persona;

    @Column(name = "per_id")
    private Integer perId;

    @JoinColumn(name = "prof_id", referencedColumnName = "prof_id", insertable = false, updatable = false)
    @ManyToOne
    private Profesion profesion;

    @Column(name = "prof_id")
    private Integer profId;

  
    @JoinColumn(name = "pub_id", referencedColumnName = "pub_id", insertable = false, updatable = false)
    @ManyToOne
    private TipoPublicidad publicidad;

    @Column(name = "pub_id")
    private Integer pubId;

    @JoinColumn(name = "san_id", referencedColumnName = "san_id", insertable = false, updatable = false)
    @ManyToOne
    private TipoSangre sangre;

    @Column(name = "san_id")
    private Integer sanId;
    
    @Size(max = 25)
    @Column(name = "usr_nombre", length = 25)
    private String usrNombre;

    @Column(name = "pac_modificacion",columnDefinition= "TIMESTAMP WITH TIME ZONE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date pacModificacion;
   
    public Paciente() {
    }

    public Paciente(Integer pacId) {
        this.pacId = pacId;
    }

    public Integer getPacId() {
        return pacId;
    }

    public void setPacId(Integer pacId) {
        this.pacId = pacId;
    }

    public String getPacAntecedentesPersonales() {
        return pacAntecedentesPersonales;
    }

    public void setPacAntecedentesPersonales(String pacAntecedentesPersonales) {
        this.pacAntecedentesPersonales = pacAntecedentesPersonales;
    }

    public String getPacAntecedentesMedicos() {
        return pacAntecedentesMedicos;
    }

    public void setPacAntecedentesMedicos(String pacAntecedentesMedicos) {
        this.pacAntecedentesMedicos = pacAntecedentesMedicos;
    }

    public Boolean getPacEstado() {
        return pacEstado;
    }

    public void setPacEstado(Boolean pacEstado) {
        this.pacEstado = pacEstado;
    }

    public Expediente getExpediente() {
        return expediente;
    }

    public void setExpediente(Expediente expediente) {
        this.expediente = expediente;
    }

    public Integer getExpId() {
        return expId;
    }

    public void setExpId(Integer expId) {
        this.expId = expId;
    }

    public Persona getPersona() {
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }

    public Integer getPerId() {
        return perId;
    }

    public void setPerId(Integer perId) {
        this.perId = perId;
    }

    public Profesion getProfesion() {
        return profesion;
    }

    public void setProfesion(Profesion profesion) {
        this.profesion = profesion;
    }

    public Integer getProfId() {
        return profId;
    }

    public void setProfId(Integer profId) {
        this.profId = profId;
    }

    public TipoPublicidad getPublicidad() {
        return publicidad;
    }

    public void setPublicidad(TipoPublicidad publicidad) {
        this.publicidad = publicidad;
    }

    public Integer getPubId() {
        return pubId;
    }

    public void setPubId(Integer pubId) {
        this.pubId = pubId;
    }

    public TipoSangre getSangre() {
        return sangre;
    }

    public void setSangre(TipoSangre sangre) {
        this.sangre = sangre;
    }

    public Integer getSanId() {
        return sanId;
    }

    public void setSanId(Integer sanId) {
        this.sanId = sanId;
    }
    
    public Date getPacModificacion() {
        return pacModificacion;
    }

    public void setPacModificacion(Date pacModificacion) {
        this.pacModificacion = pacModificacion;
    }
    
     public String getUsrNombre() {
        return usrNombre;
    }

    public void setUsrNombre(String usrNombre) {
        this.usrNombre = usrNombre;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pacId != null ? pacId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Paciente)) {
            return false;
        }
        Paciente other = (Paciente) object;
        if ((this.pacId == null && other.pacId != null) || (this.pacId != null && !this.pacId.equals(other.pacId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Paciente{" + "pacId=" + pacId + ", pacAntecedentesPersonales=" + pacAntecedentesPersonales + ", pacAntecedentesMedicos=" + pacAntecedentesMedicos + ", pacEstado=" + pacEstado + ", expediente=" + expediente + ", persona=" + persona + ", profesion=" + profesion + ", publicidad=" + publicidad + ", pubId=" + pubId + ", sangre=" + sangre + ", sanId=" + sanId + '}';
    }

}
