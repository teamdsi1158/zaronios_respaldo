/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.configuration;

/**
 *
 * @author sscruz
 */
import javax.servlet.Filter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
import sv.com.zaronios.expediente.rest.CORSFilter;

public class SpringMvcInitializer extends AbstractAnnotationConfigDispatcherServletInitializer implements org.springframework.web.WebApplicationInitializer {

    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{SpringRootConfig.class};
    }

    @Override
    protected Class<?>[] getServletConfigClasses() {
        return null;
    }

    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }

    @Override
    protected Filter[] getServletFilters() {
        return new Filter[]{new CORSFilter()};
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);

        //CONFIGURANDO SERVLET PARA  WEBSERVICES USANDO JERSEY
        /*Dynamic dynamic = servletContext.addServlet("jersey-serlvet",  com.sun.jersey.spi.spring.container.servlet.SpringServlet.class );  
        //servletContext.addServlet("jersey-serlvet",  com.sun.jersey.spi.spring.container.servlet.SpringServlet.class );  
        dynamic.setAsyncSupported(true);
        dynamic.setInitParameter("com.sun.jersey.config.property.packages","sv.com.scotiaseguros.genie.rest");
        //dynamic.setInitParameter(ResourceConfig.PROPERTY_CONTAINER_RESPONSE_FILTERS,"sv.com.scotiaseguros.genie.rest.CORSFilter");
        dynamic.setInitParameter("com.sun.jersey.api.json.POJOMappingFeature","true");
        dynamic.addMapping("/rest/*");  
        dynamic.setLoadOnStartup(1);  */
    }

}
