/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.configuration;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;
import sv.com.zaronios.expediente.security.dao.accesstoken.AccessTokenDao;
import sv.com.zaronios.expediente.security.dao.accesstoken.JpaAccessTokenDao;
import sv.com.zaronios.expediente.security.dao.user.GroupDao;
import sv.com.zaronios.expediente.security.dao.user.JpaGroupDao;
import sv.com.zaronios.expediente.security.dao.user.JpaUserDao;
import sv.com.zaronios.expediente.security.dao.user.UserDao;

/**
 *
 * @author sscruz
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "sv.com.zaronios.expediente")
@EnableTransactionManagement
public class SpringRootConfig {

    @Bean
    public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    @Bean(name = "dataSource", destroyMethod = "")
    public DataSource dataSource() {
        final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
        dsLookup.setResourceRef(true);
        DataSource dataSource = dsLookup.getDataSource("jdbc/ExpedienteDS");
        return dataSource;
    }

    @Autowired
    @Bean(name = "entityManagerFactory")
    public EntityManagerFactory entityManagerFactory(DataSource dataSource) {
        final org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean em = new org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPersistenceUnitName("expedientePU");
        org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter hva = new org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter();
        hva.setShowSql(true);
        hva.setGenerateDdl(true);
        em.setJpaVendorAdapter(hva);
        em.afterPropertiesSet();
        return em.getObject();
    }

    @Bean(name = "objectMapper")
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Autowired
    @Bean(name = "transactionManager")
    public PlatformTransactionManager transactionManager(DataSource dataSource) {
        org.springframework.orm.jpa.JpaTransactionManager tm = new org.springframework.orm.jpa.JpaTransactionManager(entityManagerFactory(dataSource));

        return tm;
    }

    @Bean(name = "accessTokenDao")
    public AccessTokenDao accessTokenDao() {
        JpaAccessTokenDao accessTokenDao = new JpaAccessTokenDao();
        return accessTokenDao;
    }

    @Bean(name = "userDao")
    public UserDao userDao() {
        JpaUserDao accessTokenDao = new JpaUserDao();
        return accessTokenDao;
    }

    @Bean(name = "groupDao")
    public GroupDao groupDao() {
        JpaGroupDao accessTokenDao = new JpaGroupDao();
        return accessTokenDao;
    }

    @Bean
    public ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
    
    

}
