package sv.com.zaronios.expediente.configuration;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import sv.com.zaronios.expediente.rest.AuthenticationTokenProcessingFilter;
import sv.com.zaronios.expediente.rest.UnauthorizedEntryPoint;
import sv.com.zaronios.expediente.security.service.TokenService;
import sv.com.zaronios.expediente.Temporizador.ControllerTemporizador;
/**
 *
 * @author Marlon
 */
@Configuration
@EnableWebSecurity
public class AppConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private TokenService tokenService;

    @Autowired
    DataSource dataSource;

    @Autowired
    public void configureGlobalSecurity(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .passwordEncoder(passwordEncoder())
                .usersByUsernameQuery("select usr_username,usr_password, activo from usuario where usr_username=?")
                .authoritiesByUsernameQuery("select usr_username, rol_id, activo from usuario where usr_username=?");
    }

    @Bean
    public ShaPasswordEncoder passwordEncoder() {
        ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
        return encoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/index").permitAll()
                .antMatchers("/partials/login.html").permitAll()
                .antMatchers("/partials/**").authenticated()
                .antMatchers("/reports/**").authenticated()
                .antMatchers("/rest/user/authenticate").permitAll()
                .antMatchers("/rest/user/**").authenticated()
                .antMatchers("/rest/groups/**").authenticated()
                .antMatchers("/rest/**").authenticated()
                .and().formLogin().failureUrl("/index")
                .and().exceptionHandling().authenticationEntryPoint(new UnauthorizedEntryPoint()).accessDeniedPage("/index")
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilterBefore(new AuthenticationTokenProcessingFilter(tokenService), UsernamePasswordAuthenticationFilter.class);
        //Temporizador para Backup
        ControllerTemporizador temporizador=new ControllerTemporizador();
        
    }
    

    @Bean(name = "authenticationManager")
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
