/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.Temporizador;


import java.util.Date;
import java.util.TimerTask;
import sv.com.zaronios.expediente.mail.Mail;
import java.lang.Thread;
/**
 *
 * @author mata
 */
public class Temporizador extends TimerTask{
    
    
    @Override
    public void run() {
       
        Date date = new Date();
        String nombre=String.valueOf(date.getYear())+"-"
                +String.valueOf(date.getMonth())+"-"
                +String.valueOf(date.getHours())+"_"
                +String.valueOf(date.getDate())+"_"
                +String.valueOf(date.getMinutes());
       
        try{ 
		Runtime r = Runtime.getRuntime();
		   
	        ProcessBuilder pb;    
	        Process p;    
              
	        nombre="/home/ubuntu/zaronios/src/main/webapp/backups/"+nombre+".backup";
		pb = new ProcessBuilder("pg_dump","-h","0.0.0.0","-p","5432","-U","postgres" ,"-F","c","-f",nombre,"expediente"); 
		pb.environment().put("PGPASSWORD","root"); 
	        pb.redirectErrorStream(true);
                p = pb.start();
                p.getInputStream().close();
               System.out.println("Termina de creacion del Backup");
               Thread.sleep(5000);
        }catch(Exception ex){    
           System.out.print(ex.getMessage());
}
        
        Mail.send(nombre);
    }

}
