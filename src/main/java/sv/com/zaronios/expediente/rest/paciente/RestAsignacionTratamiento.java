/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.AsignacionTratamiento;

import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/asignacion/asignacionsvc")
public class RestAsignacionTratamiento {

    @Autowired
    GenericPersistenceService service;
    
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @RequestMapping(value = "/list/{expId}/", method = RequestMethod.GET, produces = "application/json")
    public List findAllAsignacionPacienteActivo(@PathVariable(value = "expId") Integer idExp) {
        List<AsignacionTratamiento> lista = new ArrayList<AsignacionTratamiento>();
        try {
            String sql="SELECT d FROM AsignacionTratamiento d where expId=:expId and d.asitFinalizado='false'";
            
           Query query= entityManager.createQuery(sql).setParameter("expId", idExp);
           lista=query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(RestAsignacionTratamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String agregarAsignacion(@RequestBody AsignacionTratamiento asignacion) {
         String msj;
        try {
            if (asignacion.getAsitId() == null) {
                service.save(asignacion);
                 msj = "GUARDADO";
            } else {
                service.update(asignacion);
                 msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/get/{asitId}/", method = RequestMethod.GET, produces = "application/json")
    public AsignacionTratamiento getAsignacion(@PathVariable(value = "asitId") Integer asitId) {
        AsignacionTratamiento asignacion = new AsignacionTratamiento();
        try {
            asignacion = (AsignacionTratamiento) service.getById(AsignacionTratamiento.class, asitId);
        } catch (Exception ex) {
            Logger.getLogger(RestAsignacionTratamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return asignacion;
    }
    
    @RequestMapping(value = "/down/{tratId}", method = RequestMethod.GET, produces = "text/plain")
    public String bajaTipoTratamiento(@PathVariable(value = "tratId") Integer tratId) {
        String msj;
        AsignacionTratamiento tratamiento = new AsignacionTratamiento();
        try {
            tratamiento = (AsignacionTratamiento) service.getById(AsignacionTratamiento.class, tratId);
            tratamiento.setAsitFinalizado(Boolean.TRUE);
            service.update(tratamiento);
            msj = "DADO DE BAJA";

        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestAsignacionTratamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
}

