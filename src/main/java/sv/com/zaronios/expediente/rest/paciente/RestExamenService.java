/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.Encoders.Encoders;
import sv.com.zaronios.expediente.entity.Examen;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/examen/examensvc")
public class RestExamenService {
    
    @Autowired
    GenericPersistenceService service;
    
    @PersistenceContext
    private EntityManager entityManager;
    
     @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
     public List findAllExamenPaciente(Integer sesiId) {
        List<Examen> lista = new ArrayList<Examen>();
        try {
            String sql="SELECT d FROM Examen d where sesiId=:sesiId";
            Query query= entityManager.createQuery(sql).setParameter("sesiId", sesiId);
             lista=query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(RestMenuService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST,consumes = "application/json",produces = "text/plain")
    public String addAndUpExamen(@RequestBody Examen examen,HttpServletRequest request) throws IOException {
    String imagen=examen.getData();
    String ruta;
    String ruta1;
    String ruta2;
    String nombre;
    String msj;
         try{
          if(imagen!=null){
          String imageDataBytes = imagen.substring(imagen.indexOf(",")+1);
            
            Base64.Decoder deco = Base64.getDecoder();
            //Decodifica el String de base 64 a un array de bytes
            byte[] decodedBytes =deco.decode(imageDataBytes);
            InputStream stream = new ByteArrayInputStream(decodedBytes);
            BufferedImage image = ImageIO.read(stream);
            stream.close();
            nombre=String.valueOf(Encoders.generateRamdomNumber(1, 1000));
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
            String fecha=df.format(date);
           System.out.print(fecha);
           nombre=Encoders.encrypt(nombre+fecha);
           nombre="/"+nombre+".jpg";
           examen.setExaRuta(nombre);
           ruta= request.getSession().getServletContext().getRealPath("/");
           ruta1=ruta+"../../src/main/webapp/imagenes/examenes"+nombre; 
	   ruta2=ruta+"imagenes/examenes"+nombre;
	   System.out.println("Ruta de src ::"+ruta1);
	   System.out.println("Ruta de target ::"+ruta2);
	   //Direccion donde se guardara el archivo, Pero luego se guardara en un almacen en la nube
            File outputfile = new File(ruta1);
	    File outputfile1 = new File(ruta2);
            //Guardado de la los demas atributos
           
           
            //////////////////////////////////////
	    ImageIO.write(image,"png", outputfile1);
            ImageIO.write(image,"png", outputfile);
         }
            if(examen.getExaId()==null){
               
                service.save(examen);
           msj="GUARDADO";
            }else{
                
                service.update(examen);
                msj="ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj="ERROR";
             Logger.getLogger(RestExamenService.class.getName()).log(Level.SEVERE, null, ex);
         }
        return msj; 
         
    }
}
