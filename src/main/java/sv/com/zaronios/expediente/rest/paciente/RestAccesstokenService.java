/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Accesstoken;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/accessToken/accessTokensvc")
public class RestAccesstokenService {
    
    @Autowired
    GenericPersistenceService service;
    
    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllAccessToken() {
        List<Accesstoken> lista = new ArrayList<Accesstoken>();
        try {
            lista = service.select("SELECT a FROM Accesstoken a ORDER BY id DESC");
        } catch (Exception ex) {
            Logger.getLogger(RestAccesstokenService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
}
