/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.TipoPublicidad;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/publicidad/publicidadsvc")
public class RestPublicidadService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoPublicidad() {
        List<TipoPublicidad> lista = new ArrayList<TipoPublicidad>();
        try {
            lista = service.select("SELECT d FROM TipoPublicidad d where d.pubEstado=true ORDER BY d.pubNombre ASC");
        } catch (Exception ex) {
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/listDesactivated/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoPublicidadDesactivated() {
        List<TipoPublicidad> lista = new ArrayList<TipoPublicidad>();
        try {
            lista = service.select("SELECT d FROM TipoPublicidad d where d.pubEstado=false");
        } catch (Exception ex) {
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpTipoPublicidad(@RequestBody TipoPublicidad publicidad) {
        String msj;
        try {
            this.logger.info(publicidad.toString());
            publicidad.setPubFechaCreacion(Date.from(Instant.now()));
            publicidad.setPubEstado(Boolean.TRUE);
            if (publicidad.getPubId() == null) {
                service.save(publicidad);
                msj = "GUARDADO";
            } else {
                service.update(publicidad);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{pubId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delTipoPublicidad(@PathVariable(value = "pubId") Integer pubId) {
        String msj;
        try {
            this.logger.info("ID: " + pubId);
            service.delete(TipoPublicidad.class, pubId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{pubId}/", method = RequestMethod.GET, produces = "application/json")
    public TipoPublicidad getTipoPublicidad(@PathVariable(value = "pubId") Integer pubId) {
        TipoPublicidad publicidad = new TipoPublicidad();
        try {
            publicidad = (TipoPublicidad) service.getById(TipoPublicidad.class, pubId);
            
        } catch (Exception ex) {
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return publicidad;
    }

    @RequestMapping(value = "/down/{pubId}/", method = RequestMethod.GET, produces = "text/plain")
    public String bajaTipoPublicidad(@PathVariable(value = "pubId") Integer pubId) {
        String msj;
        TipoPublicidad publicidad = new TipoPublicidad();
        try {
            this.logger.info("ID: " + pubId);
            publicidad = (TipoPublicidad) service.getById(TipoPublicidad.class, pubId);
            publicidad.setPubEstado(Boolean.FALSE);
            service.update(publicidad);
            msj = "DADO DE BAJA";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/up/{pubId}/", method = RequestMethod.GET, produces = "text/plain")
    public String activarPublicidad(@PathVariable(value = "pubId") Integer pubId) {
        String msj;
        TipoPublicidad publicidad = new TipoPublicidad();
        try {
            this.logger.info("ID: " + pubId);
            publicidad = (TipoPublicidad) service.getById(TipoPublicidad.class, pubId);
            publicidad.setPubEstado(Boolean.TRUE);
            service.update(publicidad);
            msj = "ACTIVADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPublicidadService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/statistics/", method = RequestMethod.GET, produces = "application/json")
   public List getStatistic(){
       List estadistica=new ArrayList();
       try{
            estadistica = service.select("SELECT p.pubId,p.pubNombre,COUNT(p.pubId) FROM TipoPublicidad p ,Paciente AS e where p.pubId=e.pubId and p.pubEstado=true GROUP by p.pubId ORDER BY COUNT(p.pubId) DESC");
       } catch(Exception ex){
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
       }
       return estadistica;
   }

}
