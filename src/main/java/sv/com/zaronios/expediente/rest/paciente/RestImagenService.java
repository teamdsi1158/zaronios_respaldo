/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Base64.Decoder;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.Encoders.Encoders;
import sv.com.zaronios.expediente.entity.Imagen;
import sv.com.zaronios.expediente.entity.ImagenPerfil;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/image/imagensvc")
public class RestImagenService {

    @Autowired
    GenericPersistenceService service;
    
 @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllPerfiles() {
        List<ImagenPerfil> lista = new ArrayList<ImagenPerfil>();
        try {
            lista = service.select("SELECT i FROM ImagenPerfil i");
        } catch (Exception ex) {
            Logger.getLogger(RestImagenService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
    public Imagen addAndUpImagen(@RequestBody Imagen imagenP, HttpServletRequest request) throws IOException {
       
        //Parte el base 64 en 2 ;
        String imagen=imagenP.getData();
        String ruta;
	String ruta1;
        String ruta2;
	String nombre;

        try{
            if(imagen!=null){
             String imageDataBytes = imagen.substring(imagen.indexOf(",")+1);
             
            Decoder deco = Base64.getDecoder();
            //Decodifica el String de base 64 a un array de bytes
            byte[] decodedBytes =deco.decode(imageDataBytes);
            InputStream stream = new ByteArrayInputStream(decodedBytes);
            BufferedImage image = ImageIO.read(stream);
            stream.close();
            nombre=String.valueOf(Encoders.generateRamdomNumber(1, 1000));
            Date date = new Date();
            DateFormat df = new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
            String fecha=df.format(date);
            System.out.print(fecha);
            nombre=Encoders.encrypt(nombre+fecha);
            nombre="/"+nombre+".jpg";
            imagenP.setImgNombre(nombre);
	    ruta=request.getSession().getServletContext().getRealPath("/");
            ruta1= ruta+"../../src/main/webapp/imagenes/perfiles"+nombre;
	    ruta2= ruta+"imagenes/perfiles"+nombre;
            //Direccion donde se guardara el archivo, Pero luego se guardara en un almacen en la nube
            File outputfile = new File(ruta1);
	    File outputfile1 = new File(ruta2);
            //Guardado de la los demas atributos
            imagenP.setImgRuta(nombre);
           System.out.println("Ruta raiz ::::" + ruta);
	   System.out.println("Ruta de resource ::"+ruta1);
	   System.out.println("Ruta de target ::"+ruta2);
            //////////////////////////////////////
            
	    ImageIO.write(image,"png",outputfile1);
	    ImageIO.write(image,"png",outputfile);
            }
           
           
            if(imagenP.getImgId()==null){
               
                service.save(imagenP);
           
            }else{
                
                service.update(imagenP);
            }
            
       } catch (Exception ex) {
         }
        return imagenP;
    }
    
    @RequestMapping(value = "/savePerfil/", method = RequestMethod.POST,consumes = "application/json",produces = "application/json")
        public ImagenPerfil addAndUpPerfil(@RequestBody ImagenPerfil imagen)throws IOException {
         String msj;
        try {
            if (imagen.getImeId() == null) {
                service.save(imagen);
                msj = "GUARDADO";
            } else {
                service.update(imagen);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestImagenService.class.getName()).log(Level.SEVERE, null, ex);
        }
            return imagen;
        }
}
