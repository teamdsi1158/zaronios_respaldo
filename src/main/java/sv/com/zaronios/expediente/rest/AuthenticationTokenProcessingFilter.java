package sv.com.zaronios.expediente.rest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import sv.com.zaronios.expediente.security.entity.User;
import sv.com.zaronios.expediente.security.service.TokenService;

public class AuthenticationTokenProcessingFilter extends GenericFilterBean {

    private final TokenService tokenService;

    public AuthenticationTokenProcessingFilter(TokenService userService) {
        this.tokenService = userService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = this.getAsHttpRequest(request);

        String accessToken = this.extractAuthTokenFromRequest(httpRequest);
        if (null != accessToken) {
            String user = this.tokenService.findUserByAccessToken(accessToken);
            if (null != user) {
                List groups = this.tokenService.findUserGroups(user);
                User usr = new User(user, groups);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(usr, null, groups);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }

    private HttpServletRequest getAsHttpRequest(ServletRequest request) {
        if (!(request instanceof HttpServletRequest)) {
            throw new RuntimeException("Expecting an HTTP request");
        }
        return (HttpServletRequest) request;
    }

    private String extractAuthTokenFromRequest(HttpServletRequest httpRequest) {
        String authToken = httpRequest.getHeader("X-Access-Token");
        if (authToken == null) {
            authToken = httpRequest.getParameter("token");
        }
        return authToken;
    }
}
