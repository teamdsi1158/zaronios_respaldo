/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Bitacora;
import sv.com.zaronios.expediente.service.GenericPersistenceService;
import sv.com.zaronios.expediente.entity.Paciente;
/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/bitacora/bitacorasvc")
public class RestBitacoraService {
    
    @Autowired
    GenericPersistenceService service;
    
    @PersistenceContext
    private EntityManager entityManager;
    
    @RequestMapping(value = "/pacientes/{pacId}/", method = RequestMethod.GET, produces = "application/json")
    public List fillAllBitacoraPacientes(@PathVariable(value = "pacId") Integer pacId){
        List<Bitacora> bitacora=new ArrayList<Bitacora>();
        try {
        String sql="SELECT d FROM Bitacora d where bitTabla='paciente' and d.idEntidad=:pacId";
        Query query= entityManager.createQuery(sql).setParameter("pacId", pacId);
        bitacora=query.getResultList();
         } catch (Exception ex) {
            Logger.getLogger(RestBitacoraService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bitacora;
    }
    
     @RequestMapping(value = "/pago/{pagId}/", method = RequestMethod.GET, produces = "application/json")
    public List fillAllBitacoraPagoTratamiento(@PathVariable(value = "pagId") Integer pagId){
        List<Bitacora> bitacora=new ArrayList<Bitacora>();
        try {
        String sql="SELECT d FROM Bitacora d where bitTabla='pago_tratamiento' and d.idEntidad=:pagId";
        Query query= entityManager.createQuery(sql).setParameter("pagId", pagId);
        bitacora=query.getResultList();
         } catch (Exception ex) {
            Logger.getLogger(RestBitacoraService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return bitacora;
    }
     
     
    
}
