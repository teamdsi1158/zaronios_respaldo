/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.TipoTratamiento;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/tratamiento/tratamientosvc")
public class RestTratamientoService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoTratamiento() {
        List<TipoTratamiento> lista = new ArrayList<TipoTratamiento>();
        try {
            lista = service.select("SELECT t FROM TipoTratamiento t where t.tratEstado='true'");
        } catch (Exception ex) {
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
      @RequestMapping(value = "/listDesactivated/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoTratamientoDesactivated() {
        List<TipoTratamiento> lista = new ArrayList<TipoTratamiento>();
        try {
            lista = service.select("SELECT t FROM TipoTratamiento t where t.tratEstado='false'");
        } catch (Exception ex) {
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/addAndUp/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public TipoTratamiento addAndUpTipoTratamiento(@RequestBody TipoTratamiento tratamiento) {
        String msj;
        try {

            tratamiento.setTratEstado(Boolean.TRUE);
            if (tratamiento.getTratId() == null) {
                service.save(tratamiento);
                msj = "GUARDADO";
            } else {
                service.update(tratamiento);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tratamiento;
    }

    @RequestMapping(value = "/del/{tratId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delTipoPublicidad(@PathVariable(value = "tratId") Integer tratId) {
        String msj;
        try {
            this.logger.info("ID: " + tratId);
            service.delete(TipoTratamiento.class, tratId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{tratId}/", method = RequestMethod.GET,produces="application/json")
    public TipoTratamiento getTipoTratamiento(@PathVariable(value = "tratId") Integer tratId) {
        TipoTratamiento tratamiento = new TipoTratamiento();
        try {
            tratamiento = (TipoTratamiento) service.getById(TipoTratamiento.class, tratId);
        } catch (Exception ex) {
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tratamiento;
    }

    @RequestMapping(value = "/down/{tratId}", method = RequestMethod.GET, produces = "text/plain")
    public String bajaTipoTratamiento(@PathVariable(value = "tratId") Integer tratId) {
        String msj;
        TipoTratamiento tratamiento = new TipoTratamiento();
        this.logger.info("ID: " + tratId);
        try {
            tratamiento = (TipoTratamiento) service.getById(TipoTratamiento.class, tratId);
            tratamiento.setTratEstado(Boolean.FALSE);
            service.update(tratamiento);
            msj = "ELIMINADO";

        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/up/{tratId}", method = RequestMethod.GET, produces = "text/plain")
    public String activarTratamiento(@PathVariable(value = "tratId") Integer tratId) {
        String msj;
        TipoTratamiento tratamiento = new TipoTratamiento();
        this.logger.info("ID: " + tratId);
        try {
            tratamiento = (TipoTratamiento) service.getById(TipoTratamiento.class, tratId);
            tratamiento.setTratEstado(Boolean.TRUE);
            service.update(tratamiento);
            msj = "ACTIVADO";

        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/statistics/", method = RequestMethod.GET, produces = "application/json")
   public List getStatistic(){
       List estadistica=new ArrayList();
       try{
            estadistica = service.select("SELECT t.tratId,t.tratNombre,COUNT(t.tratId) FROM TipoTratamiento t ,AsignacionTratamiento AS a  where t.tratId=a.tratId GROUP by t.tratId ORDER by COUNT(t.tratId) DESC ");
       } catch(Exception ex){
            Logger.getLogger(RestTratamientoService.class.getName()).log(Level.SEVERE, null, ex);
       }
       return estadistica;
   }
}