package sv.com.zaronios.expediente.rest.paciente;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.util.JRLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author Marlon
 */
@RestController
@RequestMapping("/rest/reportsvc")
public class RestReportes {

    @Autowired
    GenericPersistenceService service;

    @Autowired
    DataSource dataSource;

    @RequestMapping(value = "/report_accesos/")
    public void generateReportAccess(@RequestHeader("X-Access-Token") String token, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            JasperReport report;
            JasperPrint print;
            Connection cn;
            byte[] binario = null;
            if (token == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            Map parameters = new HashMap();
            cn = dataSource.getConnection();
            File fileReport=getFile("ingreporte",request);
            parameters.put("path_file",fileReport.getAbsolutePath().split("/zaronios")[0]);
            report = getCompiledFile("ingreporte", request,fileReport);
            binario = JasperRunManager.runReportToPdf(report, parameters, cn);
            print = JasperFillManager.fillReport(report, parameters, cn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=accesos.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, outStream);
	    
                  
            response.setContentLength(binario.length);
            response.setCharacterEncoding("utf-8");
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(binario, 0, binario.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (JRException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @RequestMapping(value="/report_pacientes/")
    public void generateReportPacientes(@RequestHeader("X-Access-Token") String token, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try{
            JasperReport report;
            JasperPrint print;
            Connection cn;
            byte[] binario = null;
            if (token == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            Map parameters = new HashMap();
            cn = dataSource.getConnection();
            File fileReport=getFile("pacientes",request);
            parameters.put("path_file",fileReport.getAbsolutePath().split("/zaronios")[0]);
            report = getCompiledFile("pacientes", request,fileReport);
            binario = JasperRunManager.runReportToPdf(report, parameters, cn);
            print = JasperFillManager.fillReport(report, parameters, cn);
             response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=pacientes.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, outStream);
            
            
            response.setContentLength(binario.length);
            response.setCharacterEncoding("utf-8");
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(binario, 0, binario.length);
            ouputStream.flush();
            ouputStream.close();
       } catch (JRException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
    @RequestMapping(value = "/report_empleado/{id}/")
    public void generateReportEmpleado(@PathVariable Long id,@RequestHeader("X-Access-Token") String token, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            JasperReport report;
            JasperPrint print;
            Connection cn;
            byte[] binario = null;
            if (token == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            Map parameters = new HashMap();
            parameters.put("id_empleado", id);
            cn = dataSource.getConnection();
            File fileReport=getFile("empleado",request);
            
            parameters.put("path_file",fileReport.getAbsolutePath().split("/zaronios")[0]);
            report = getCompiledFile("empleado", request,fileReport);
            
            binario = JasperRunManager.runReportToPdf(report, parameters, cn);
            print = JasperFillManager.fillReport(report, parameters, cn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=empleado.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, outStream);
            
            
	    response.setContentLength(binario.length);
            response.setCharacterEncoding("utf-8");
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(binario, 0, binario.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (JRException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @RequestMapping(value = "/report_expediente/{id}/")
    public void generateReportExpediente(@PathVariable Long id,@RequestHeader("X-Access-Token") String token, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            JasperReport report;
            JasperPrint print;
            Connection cn;
            byte[] binario = null;
            if (token == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            Map parameters = new HashMap();
            parameters.put("exp_id", id);
            cn = dataSource.getConnection();
            File fileReport=getFile("expediente",request);
	    System.out.print(" Ruta : "+ fileReport.getAbsolutePath().split("/zaronios")[0]+" -l- ");            
            parameters.put("path_file",fileReport.getAbsolutePath().split("/zaronios")[0]);
            report = getCompiledFile("expediente", request,fileReport);
            
            binario = JasperRunManager.runReportToPdf(report, parameters, cn);
            print = JasperFillManager.fillReport(report, parameters, cn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=expediente.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, outStream);
            
            response.setContentLength(binario.length);
            response.setCharacterEncoding("utf-8");
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(binario, 0, binario.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (JRException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
        @RequestMapping(value = "/report_paciente/{id}/")
    public void generateReportPaciente(@PathVariable Long id,@RequestHeader("X-Access-Token") String token, HttpServletRequest request, HttpServletResponse response) throws SQLException {
        try {
            JasperReport report;
            JasperPrint print;
            Connection cn;
            byte[] binario = null;
            if (token == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
            Map parameters = new HashMap();
            parameters.put("pac_id", id);
            cn = dataSource.getConnection();
            File fileReport=getFile("datosPaciente",request);
            
            parameters.put("path_file",fileReport.getAbsolutePath().split("/zaronios")[0]);
            report = getCompiledFile("datosPaciente", request,fileReport);
            
            binario = JasperRunManager.runReportToPdf(report, parameters, cn);
            print = JasperFillManager.fillReport(report, parameters, cn);
            response.setContentType("application/x-pdf");
            response.setHeader("Content-disposition", "inline; filename=datosPaciente.pdf");
            final OutputStream outStream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(print, outStream);
            
            
	    response.setContentLength(binario.length);
            response.setCharacterEncoding("utf-8");
            ServletOutputStream ouputStream = response.getOutputStream();
            ouputStream.write(binario, 0, binario.length);
            ouputStream.flush();
            ouputStream.close();
        } catch (JRException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(RestReportes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JasperReport getCompiledFile(String fileName, HttpServletRequest request,File reportFile) throws JRException {
        if (!reportFile.exists()) {
            JasperCompileManager.compileReportToFile(request.getSession().getServletContext().getRealPath("/reports/" + fileName + ".jrxml"), request.getSession().getServletContext().getRealPath("/reports/" + fileName + ".jasper"));
        }
        JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(reportFile.getPath());
        return jasperReport;
    }
    
    private File getFile(String fileName,HttpServletRequest request){
        System.out.println("path " + request.getSession().getServletContext().getRealPath("/reports/" + fileName + ".jasper"));
        File reportFile = new File(request.getSession().getServletContext().getRealPath("/reports/" + fileName + ".jasper"));
        return reportFile;
    }

}
