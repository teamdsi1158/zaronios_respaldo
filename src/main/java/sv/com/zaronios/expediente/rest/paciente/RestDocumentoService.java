/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Documento;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/documento/documentosvc")
public class RestDocumentoService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllDocumento() {
        List<Documento> lista = new ArrayList<Documento>();
        try {
            lista = service.select("SELECT d FROM Documento d");
        } catch (Exception ex) {
            Logger.getLogger(RestDocumentoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Documento addAndUpDocumento(@RequestBody Documento documento) {
        try {
            if (documento.getDocId() == null) {
                service.save(documento);
            } else {
                service.update(documento);
            }
        } catch (Exception ex) {
            Logger.getLogger(RestDocumentoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documento;
    }

    @RequestMapping(value = "/del/{docId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delDocumento(@PathVariable(value = "docId") Integer docId) {
        String msj;
        try {
            service.delete(Documento.class, docId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestDocumentoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{docId}/", method = RequestMethod.GET, produces = "text/plain")
    public Documento getDocumento(@PathVariable(value = "docId") Integer docId) {
        Documento documento = new Documento();
        try {
            documento = (Documento) service.getById(Documento.class, docId);
        } catch (Exception ex) {
            Logger.getLogger(RestDocumentoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return documento;
    }
}
