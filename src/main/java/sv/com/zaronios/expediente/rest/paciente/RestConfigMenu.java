/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Menu;
import sv.com.zaronios.expediente.entity.Usuario;
import sv.com.zaronios.expediente.service.GenericPersistenceService;
import sv.com.zaronios.expediente.service.MenuService;

/**
 *
 * @author Marlon
 */
@RestController
@RequestMapping("/rest/menuconf/menusvc")
public class RestConfigMenu {

    @Autowired
    MenuService menuService;

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List listaCoberturas() {
        List<Menu> lista = new ArrayList<Menu>();
        try {
            lista = menuService.getMenu();
        } catch (Exception ex) {
            Logger.getLogger(RestConfigMenu.class.getName()).log(Level.SEVERE, null, ex);
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return lista;
    }

    @RequestMapping(value = "/usuarios/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Usuario getUsuario(@RequestBody Map map) {
        Usuario usuario = new Usuario();
        System.out.println(map.get("username").toString());
        String param = map.get("username").toString();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(" select u from Usuario u ");
            sb.append(" join fetch u.rol r ");
            sb.append(" where ");
            sb.append(" 1=1 ");
            sb.append(" and u.usrUsername = '" + param + "'");
            usuario = (Usuario) service.select(sb.toString()).get(0);
        } catch (Exception ex) {
            Logger.getLogger(RestConfigMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }
}
