/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Expediente;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/expediente/expedientesvc")
public class RestExpedienteService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllExpediente() {
        List<Expediente> lista = new ArrayList<Expediente>();
        try {
            lista = service.select("SELECT e FROM Expediente e");
        } catch (Exception ex) {
            Logger.getLogger(RestExpedienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpExpediente(@RequestBody Expediente expediente) {
        String msj;
        try {
            if (expediente.getExpId() == null) {
                service.save(expediente);
                msj = "GUARDADO";
            } else {
                service.update(expediente);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestExpedienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{expId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delExpediente(@PathVariable(value = "expId") Integer expId) {
        String msj;
        try {
            service.delete(Expediente.class, expId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestExpedienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{expId}/", method = RequestMethod.GET, produces = "text/plain")
    public Expediente getExpediente(@PathVariable(value = "expId") Integer expId) {
        Expediente expediente = new Expediente();
        try {
            expediente = (Expediente) service.getById(Expediente.class, expId);
        } catch (Exception ex) {
            Logger.getLogger(RestExpedienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return expediente;
    }

    @RequestMapping(value = "/down/{empId}/", method = RequestMethod.GET, produces = "text/plain")
    public String bajarExpediente(@PathVariable(value = "expId") Integer expId) {
        String msj;
        try {
            Expediente expediente = (Expediente) service.getById(Expediente.class, expId);
            expediente.setExpEstado(Boolean.FALSE);
            service.update(expediente);
            msj = "DADO DE BAJA";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestExpedienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

}
