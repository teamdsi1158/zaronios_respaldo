/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.PagoTratamiento;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/pago/pagosvc")
public class RestPagoService {
     @Autowired
    GenericPersistenceService service;

     @PersistenceContext
    private EntityManager entityManager;
     
    @RequestMapping(value = "/list/{asitId}", method = RequestMethod.GET, produces = "application/json")
    public List findAllPago(@PathVariable(value = "asitId") Integer asitId) {
        List<PagoTratamiento> lista = new ArrayList<PagoTratamiento>();
        try {
            String sql="SELECT d FROM PagoTratamiento d where asitId=:asitId";
            
           Query query= entityManager.createQuery(sql).setParameter("asitId", asitId);
           lista=query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(RestAsignacionTratamiento.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String savePago(@RequestBody PagoTratamiento pago) {
        String msj;
        try {
            if (pago.getPagId() == null) {
                service.save(pago);
                msj = "GUARDADO";
            } else {
                service.update(pago);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    
    @RequestMapping(value = "/del/{pagId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delSexo(@PathVariable(value = "pagId") Integer pagId) {
        String msj;
        try {
            service.delete(PagoTratamiento.class, pagId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
     @RequestMapping(value = "/get/{pagId}/", method = RequestMethod.GET, produces = "application/json")
    public PagoTratamiento getPago(@PathVariable(value = "pagId") Integer pagId) {
        PagoTratamiento pago = new PagoTratamiento();
        try {
            pago = (PagoTratamiento) service.getById(PagoTratamiento.class, pagId);
        } catch (Exception ex) {
            Logger.getLogger(RestPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pago;
    }
}
