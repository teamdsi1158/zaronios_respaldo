/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.EstadoCivil;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/estadoc/estadocsvc")
public class RestEstadoCivilService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllEstadoCivil() {
        List<EstadoCivil> lista = new ArrayList<EstadoCivil>();
        try {
            lista = service.select("SELECT e FROM EstadoCivil e");
        } catch (Exception ex) {
            Logger.getLogger(RestEstadoCivilService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpEstadoCivil(@RequestBody EstadoCivil estado) {
        String msj;
        try {
            if (estado.getEstId() == null) {
                service.save(estado);
                msj = "GUARDADO";
            } else {
                service.update(estado);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEstadoCivilService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{estId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delEstadoCivil(@PathVariable(value = "estId") Integer estId) {
        String msj;
        try {
            service.delete(EstadoCivil.class, estId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "Error posiblemente hay personas con este estado civil";
            Logger.getLogger(RestEstadoCivilService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{estId}/", method = RequestMethod.GET, produces = "application/json")
    public EstadoCivil getEstadoCivil(@PathVariable(value = "estId") Integer estId) {
        EstadoCivil estado = new EstadoCivil();
        try {
            estado = (EstadoCivil) service.getById(EstadoCivil.class, estId);
        } catch (Exception ex) {
            Logger.getLogger(RestEstadoCivilService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estado;
    }
    
    @RequestMapping(value = "/statistics/", method = RequestMethod.GET, produces = "application/json")
    public List getStatistic(){
       List estadistica=new ArrayList();
       try{
            estadistica = service.select("SELECT d.estId,d.estNombre,COUNT(d.estId) FROM EstadoCivil d ,Persona AS p,Paciente AS e  where p.estId=d.estId AND p.perId=e.perId  GROUP by d.estId ORDER by COUNT(d.estId) DESC");
       } catch(Exception ex){
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
       }
       return estadistica;
   }
}
