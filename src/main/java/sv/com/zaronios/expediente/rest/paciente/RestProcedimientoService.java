/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Procedimiento;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/procedimiento/procedimientosvc")
public class RestProcedimientoService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllProcedimiento() {
        List<Procedimiento> lista = new ArrayList<Procedimiento>();
        try {
            lista = service.select("SELECT p FROM Procedimiento p ");
        } catch (Exception ex) {
            Logger.getLogger(RestProcedimientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/addAndUp/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpProcedimiento(@RequestBody Procedimiento procedimiento) {
        String msj;
        try {

            
            if (procedimiento.getProcId() == null) {
                service.save(procedimiento);
                msj = "GUARDADO";
            } else {
                service.update(procedimiento);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestProcedimientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{ProcId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delTipoProcedimiento(@PathVariable(value = "ProcId") Integer ProcId) {
        String msj;
        try {
            this.logger.info("ID: " + ProcId);
            service.delete(Procedimiento.class, ProcId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestProcedimientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{procId}/", method = RequestMethod.GET,produces = "application/json")
    public Procedimiento getProcedimiento(@PathVariable(value = "procId") Integer procId) {
        Procedimiento procedimiento = new Procedimiento();
        try {
            procedimiento = (Procedimiento) service.getById(Procedimiento.class, procId);
        } catch (Exception ex) {
            Logger.getLogger(RestProcedimientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return procedimiento;
    }

    @RequestMapping(value = "/down/{ProcId}", method = RequestMethod.DELETE, produces = "text/plain")
    public String bajaProcedimiento(@PathVariable(value = "ProcId") Integer procId) {
        String msj;
        Procedimiento procedimiento = new Procedimiento();
        this.logger.info("ID: " + procId);
        try {
            procedimiento = (Procedimiento) service.getById(Procedimiento.class, procId);
            service.delete(Procedimiento.class, procId);
            service.update(procedimiento);
            msj = "Eliminado";

        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestProcedimientoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
}
