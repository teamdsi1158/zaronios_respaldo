/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.FormParam;
import javax.ws.rs.WebApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.security.entity.AccessToken;
import sv.com.zaronios.expediente.security.entity.Rol;
import sv.com.zaronios.expediente.security.service.TokenService;
import sv.com.zaronios.expediente.transfer.UserTransfer;

/**
 *
 * @author sscruz
 */
@RestController
@RequestMapping("/rest/user")
public class UserRestController {

    @Autowired
    private TokenService tokenService;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authManager;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public UserTransfer getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
       
        System.out.println("class: " + principal.getClass().getSimpleName());
        if (!(principal instanceof UserDetails)) {
            throw new WebApplicationException(401);
        }
        UserDetails userDetails = (UserDetails) principal;
        return new UserTransfer(userDetails.getUsername(), this.createRoleMap(userDetails));
    }

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = "application/json")
    public AccessToken authenticate(@FormParam("username") String username, @FormParam("password") String password) {
        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, password);
        Authentication authentication = this.authManager.authenticate(authenticationToken);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        System.out.println("auth: " + authentication.isAuthenticated());
        Object principal = authentication.getPrincipal();
        Object auth= authentication.getAuthorities().toArray()[0];
        System.out.print("class: " + auth);
        
        if (!(principal instanceof UserDetails)) {
            throw new WebApplicationException(401);
        }
        return this.tokenService.createAccessToken((UserDetails) principal);
    }

    private Map<String, Boolean> createRoleMap(UserDetails userDetails) {
        Map<String, Boolean> roles = new HashMap<String, Boolean>();
        List<GrantedAuthority> groups = tokenService.findUserGroups(userDetails.getUsername());
        for (GrantedAuthority authority : groups) {
            roles.put(authority.getAuthority(), Boolean.TRUE);
        }
        return roles;
    }

}
