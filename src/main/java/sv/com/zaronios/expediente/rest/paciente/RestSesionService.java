/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Sesion;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/sesion/sesionsvc")
public class RestSesionService {
     @Autowired
    GenericPersistenceService service;
    
     @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces="application/json")
    public Integer agregarSesion(@RequestBody Sesion sesion) {
        String msj;
        try {
            if (sesion.getSesiId()== null) {
                service.save(sesion);
                 msj = "GUARDADO";
            } else {
                service.update(sesion);
                 msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestSesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sesion.getSesiId();
    }
    
    @RequestMapping(value = "/get/{sesiId}/", method = RequestMethod.GET, produces = "application/json")
    public Sesion getSexo(@PathVariable(value = "sesiId") Integer sesiId) {
        Sesion sexo = new Sesion();
        try {
            sexo = (Sesion) service.getById(Sesion.class, sesiId);
        } catch (Exception ex) {
            Logger.getLogger(RestSesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sexo;
    }
    
}
