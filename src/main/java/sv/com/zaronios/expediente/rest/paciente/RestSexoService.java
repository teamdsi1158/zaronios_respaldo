/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Sexo;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/sexo/sexosvc")
public class RestSexoService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllSexo() {
        List<Sexo> lista = new ArrayList<Sexo>();
        try {
            lista = service.select("SELECT d FROM Sexo d");
        } catch (Exception ex) {
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpSexo(@RequestBody Sexo sexo) {
        String msj;
        try {
            if (sexo.getSexId() == null) {
                service.save(sexo);
                msj = "GUARDADO";
            } else {
                service.update(sexo);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{sexId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delSexo(@PathVariable(value = "sexId") Integer sexId) {
        String msj;
        try {
            service.delete(Sexo.class, sexId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR posiblemente hay personas registradas con este genero";
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{sexId}/", method = RequestMethod.GET, produces = "application/json")
    public Sexo getSexo(@PathVariable(value = "sexId") Integer sexId) {
        Sexo sexo = new Sexo();
        try {
            sexo = (Sexo) service.getById(Sexo.class, sexId);
        } catch (Exception ex) {
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sexo;
    }
    
   @RequestMapping(value = "/statistics/", method = RequestMethod.GET, produces = "application/json")
   public List getStatistic(){
       List estadistica=new ArrayList();
       try{
            estadistica = service.select("SELECT d.sexId,d.sexNombre,COUNT(d.sexId) FROM Sexo d ,Persona AS p,Paciente AS e  where p.sexId=d.sexId AND p.perId=e.perId  GROUP by d.sexId");
       } catch(Exception ex){
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
       }
       return estadistica;
   }
}