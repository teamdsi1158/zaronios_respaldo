/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Contacto;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/contacto/contactosvc")
public class RestContactoService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllContacto() {
        List<Contacto> lista = new ArrayList<Contacto>();
        try {
            lista = service.select("SELECT d FROM Contacto d");
        } catch (Exception ex) {
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Contacto agregarContacto(@RequestBody Contacto contacto) {
        try {
            if (contacto.getConId() == null) {
                service.save(contacto);
            } else {
                service.update(contacto);
            }
        } catch (Exception ex) {
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contacto;
    }

    @RequestMapping(value = "/del/{conId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delContacto(@PathVariable(value = "conId") Integer conId) {
        String msj;
        try {
            msj = "ELIMINADO";
            service.delete(Contacto.class, conId);
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{conId}/", method = RequestMethod.GET, produces = "application/json")
    public Contacto getContacto(@PathVariable(value = "conId") Integer conId) {
        Contacto contacto = new Contacto();
        try {
            contacto = (Contacto) service.getById(Contacto.class, conId);
        } catch (Exception ex) {
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contacto;
    }
}
