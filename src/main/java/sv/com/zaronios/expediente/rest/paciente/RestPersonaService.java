/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Persona;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping(value = "/rest/persona/personasvc")
public class RestPersonaService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List listarPersona() {
        List<Persona> lista = new ArrayList<Persona>();
        try {
            lista = service.select("select p Persona p");
        } catch (Exception e) {
            Logger.getLogger(RestPersonaService.class.getName()).log(Level.SEVERE, null, e);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Persona addAndUpersona(@RequestBody Persona persona) {
        try {
            if (persona.getPerId() == null) {
                service.save(persona);
            } else {
                service.update(persona);
            }
        } catch (Exception ex) {
            Logger.getLogger(RestPersonaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return persona;
    }

    @RequestMapping(value = "/del/{perId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delPersona(@PathVariable(value = "perId") Integer perId) {
        String msj;
        try {
            service.delete(Persona.class, perId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPersonaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{perId}/", method = RequestMethod.GET, produces = "application/json")
    public Persona getPersona(@PathVariable(value = "perId") Integer perId) {
        Persona persona = new Persona();
        try {
            persona = (Persona) service.getById(Persona.class, perId);
        } catch (Exception ex) {
            Logger.getLogger(RestPersonaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return persona;
    }

}