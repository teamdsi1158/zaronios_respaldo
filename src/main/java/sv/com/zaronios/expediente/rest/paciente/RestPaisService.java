/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Pais;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/pais/paissvc")
public class RestPaisService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllPais() {
        List<Pais> lista = new ArrayList<Pais>();
        try {
            lista = service.select("SELECT p FROM Pais p");
        } catch (Exception ex) {
            Logger.getLogger(RestPaisService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/addAndUp/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpPais(@RequestBody Pais pais) {
        String msj;
        try {
            if (pais.getPaiCodigo() == null) {
                service.save(pais);
                msj = "GUARDADO";
            } else {
                service.update(pais);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPaisService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{paiCodigo}/", method = RequestMethod.GET, produces = "text/plain")
    public String delPais(@PathVariable(value = "paiCodigo") Integer paiCodigo) {
        String msj;
        try {
            service.delete(Pais.class, paiCodigo);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPaisService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{paiCodigo}/", method = RequestMethod.GET, produces = "text/plain")
    public Pais getPais(@PathVariable(value = "paiCodigo") Integer paiCodigo) {
        Pais pais = new Pais();
        try {
            pais = (Pais) service.getById(Pais.class, paiCodigo);
        } catch (Exception ex) {
            Logger.getLogger(RestPaisService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pais;
    }
}
