/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.TipoSangre;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/tiposan/tiposansvc")
public class RestTipoSangreService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoSangre() {
        List<TipoSangre> lista = new ArrayList<TipoSangre>();
        try {
            lista = service.select("SELECT d FROM TipoSangre d");
        } catch (Exception ex) {
            Logger.getLogger(RestTipoSangreService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpTipoSangre(@RequestBody TipoSangre tipo) {
        String msj;
        try {
            if (tipo.getSanId() == null) {
                service.save(tipo);
                msj = "GUARDADO";
            } else {
                service.update(tipo);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTipoSangreService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{sanId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delTipoSangre(@PathVariable(value = "sanId") Integer sanId) {
        String msj;
        try {
            service.delete(TipoSangre.class, sanId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTipoSangreService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{sanId}/", method = RequestMethod.GET, produces = "text/plain")
    public TipoSangre getTipoSangre(@PathVariable(value = "sanId") Integer sanId) {
        TipoSangre tipo = new TipoSangre();
        try {
            tipo = (TipoSangre) service.getById(TipoSangre.class, sanId);
        } catch (Exception ex) {
            Logger.getLogger(RestTipoSangreService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipo;
    }
}