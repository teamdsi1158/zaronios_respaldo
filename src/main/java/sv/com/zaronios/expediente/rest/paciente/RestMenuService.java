/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;


import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Menu;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/menu/menusvc")
public class RestMenuService {

    @Autowired
    GenericPersistenceService service;   
    
    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List<Menu> cargarMenu() {
        List<Menu> lista = null;
        try {
            String query = "select m from Menu m where m.menIdPadre is null";
            lista = service.select(query);
          
        } catch (Exception ex) {
            Logger.getLogger(RestContactoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpMenu(@RequestBody Menu menu) {
        String msj;
        try {
            if (menu.getMenId() == null) {
                service.save(menu);
                msj = "GUARDADO";
            } else {
                service.update(menu);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestMenuService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{menId}/", method = RequestMethod.GET, produces = "text/plain")
    public String delMenu(@PathVariable(value = "menId") Integer menId) {
        String msj;
        try {
            service.delete(Menu.class, menId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestMenuService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{menId}/", method = RequestMethod.GET, produces = "text/plain")
    public Menu getMenu(@PathVariable(value = "menId") Integer menId) {
        Menu menu = new Menu();
        try {
            menu = (Menu) service.getById(Menu.class, menId);
        } catch (Exception ex) {
            Logger.getLogger(RestMenuService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return menu;
    }

}
