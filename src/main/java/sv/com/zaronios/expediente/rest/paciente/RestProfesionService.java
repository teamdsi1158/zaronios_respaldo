/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Profesion;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/profesion/profesionsvc")
public class RestProfesionService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllProfesion() {
        List<Profesion> lista = new ArrayList<Profesion>();
        try {
            lista = service.select("SELECT d FROM Profesion d ORDER BY d.profNombre ASC");
        } catch (Exception ex) {
            Logger.getLogger(RestProfesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Profesion addAndUpProfesion(@RequestBody Profesion profesion) {
        try {
            if (profesion.getProfId() == null) {
                service.save(profesion);
            } else {
                service.update(profesion);
            }
        } catch (Exception ex) {
            Logger.getLogger(RestProfesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return profesion;
    }

    @RequestMapping(value = "/del/{profId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delProfesion(@PathVariable(value = "profId") Integer profId) {
        String msj;
        try {
            service.delete(Profesion.class, profId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestProfesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{profId}/", method = RequestMethod.GET, produces = "application/json")
    public Profesion getProfesion(@PathVariable(value = "profId") Integer profId) {
        Profesion profesion = new Profesion();
        try {
            profesion = (Profesion) service.getById(Profesion.class, profId);
        } catch (Exception ex) {
            Logger.getLogger(RestProfesionService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return profesion;
    }
    
    @RequestMapping(value = "/statistics/", method = RequestMethod.GET, produces = "application/json")
   public List getStatistic(){
       List estadistica=new ArrayList();
       try{
            estadistica = service.select("SELECT p.profId,p.profNombre,COUNT(p.profId) FROM Profesion p ,Paciente AS e where p.profId=e.profId GROUP by p.profId ORDER BY COUNT(p.profId) DESC");
       } catch(Exception ex){
            Logger.getLogger(RestSexoService.class.getName()).log(Level.SEVERE, null, ex);
       }
       return estadistica;
   }
}
