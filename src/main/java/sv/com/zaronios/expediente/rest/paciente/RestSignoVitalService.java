/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.SignoVital;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/signoVital/signoVitalsvc")
public class RestSignoVitalService {
     @Autowired
    GenericPersistenceService service;
     
     @PersistenceContext
    private EntityManager entityManager;
     
     @RequestMapping(value = "/list/{sesiId}/", method = RequestMethod.GET, produces = "application/json")
    public List findAllSignoVitalSesion(@PathVariable(value = "sesiId") Integer sesiId) {
        List<SignoVital> lista = new ArrayList<SignoVital>();
        try {
             String sql="SELECT d FROM SignoVital d where sesiId=:sesiId";
            
           Query query= entityManager.createQuery(sql).setParameter("sesiId", sesiId);
           lista=query.getResultList();
        } catch (Exception ex) {
            Logger.getLogger(RestSignoVitalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
     
      @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpSignoVital(@RequestBody SignoVital signo) {
        String msj;
        try {
            if (signo.getSiviId() == null) {
                service.save(signo);
                msj = "GUARDADO";
            } else {
                service.update(signo);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR"+ex.toString();
            Logger.getLogger(RestSignoVitalService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
}
