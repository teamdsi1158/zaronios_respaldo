/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Cita;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/cita/citasvc")
public class RestCitaService {
    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllCita() {
        List<Cita> lista = new ArrayList<Cita>();
        try {
            lista = service.select("SELECT d FROM Cita d");
        } catch (Exception ex) {
            Logger.getLogger(RestCitaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Integer addAndUpCita(@RequestBody Cita cita) {
        String msj;
        try {
            if (cita.getCitaId() == null) {
                service.save(cita);
                msj = "GUARDADO";
            } else {
                service.update(cita);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestCitaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cita.getCitaId();
    }

    @RequestMapping(value = "/del/{citaId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delCita(@PathVariable(value = "citaId") Integer citaId) {
        String msj;
        try {
            service.delete(Cita.class, citaId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR posiblemente hay personas registradas con este genero";
            Logger.getLogger(RestCitaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{citaId}/", method = RequestMethod.GET, produces = "application/json")
    public Cita getCita(@PathVariable(value = "citaId") Integer citaId) {
        Cita cita = new Cita();
        try {
            cita = (Cita) service.getById(Cita.class, citaId);
        } catch (Exception ex) {
            Logger.getLogger(RestCitaService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cita;
    }
    
 
}
