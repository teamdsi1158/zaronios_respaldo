/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Rol;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/rol/rolsvc")
public class RestRolService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllRol() {
        List<Rol> lista = new ArrayList<Rol>();
        try {
            lista = service.select("SELECT r FROM Rol r");
        } catch (Exception ex) {
            Logger.getLogger(RestRolService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String agregarRol(@RequestBody Rol rol) {
         String msj;
        try {
            if (rol.getRolId() == null) {
                service.save(rol);
                 msj = "GUARDADO";
            } else {
                service.update(rol);
                  msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestRolService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{rolId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delRol(@PathVariable(value = "rolId") Integer rolId) {
        String msj;
        try {
            msj = "ELIMINADO";
            service.delete(Rol.class, rolId);
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestRolService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{rolId}/", method = RequestMethod.GET, produces = "application/json")
    public Rol getRol(@PathVariable(value = "rolId") Integer rolId) {
        Rol contacto = new Rol();
        try {
            contacto = (Rol) service.getById(Rol.class, rolId);
        } catch (Exception ex) {
            Logger.getLogger(RestRolService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contacto;
    }

}
