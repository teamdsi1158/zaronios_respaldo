/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.TipoPago;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/tipoPago/tipoPagosvc")
public class RestTipoPagoService {
     @Autowired
    GenericPersistenceService service;
     
     @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoPago() {
        List <TipoPago>lista = new ArrayList<TipoPago>();
        try {
            lista = service.select("SELECT p FROM TipoPago p  ORDER BY p.tpagNombre ASC");
        } catch (Exception ex) {
            Logger.getLogger(RestTipoPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUptipoPago(@RequestBody TipoPago tipoPago) {
          String msj;
        try {

            
            if (tipoPago.getTpagId() == null) {
                service.save(tipoPago);
                msj = "GUARDADO";
            } else {
                service.update(tipoPago);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTipoPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    
    @RequestMapping(value = "/del/{tpagId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String deltipoPago(@PathVariable(value = "tpagId") Integer tpagId) {
        String msj;
        try {
            service.delete(TipoPago.class, tpagId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR POSIBLEMENTE HAY PERSONAS QUE PAGARON CON ESTA MODALIDAD";
            Logger.getLogger(RestTipoPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/get/{tpagId}/", method = RequestMethod.GET, produces = "application/json")
    public TipoPago getTipoPago(@PathVariable(value = "tpagId") Integer tpagId) {
        TipoPago sexo = new TipoPago();
        try {
            sexo = (TipoPago) service.getById(TipoPago.class, tpagId);
        } catch (Exception ex) {
            Logger.getLogger(RestTipoPagoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return sexo;
    }
    
}
