/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ws.rs.WebApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.security.entity.Group;
import sv.com.zaronios.expediente.security.entity.Role;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author sscruz
 */
@RestController
@RequestMapping("/rest/groups")

public class GroupRestController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private GenericPersistenceService genericService;

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    public List list() throws IOException {
        try {
            this.logger.info("list()");
            List<Group> allEntries = genericService.select(Group.class, "id");
            return allEntries;
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GroupRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public Group read(@PathVariable("id") Long id) {
        this.logger.info("read(id)");
        Group blogPost = null;
        try {
            blogPost = (Group) this.genericService.getById(Group.class, id);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GroupRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (blogPost == null) {
            throw new WebApplicationException(404);
        }
        return blogPost;
    }

    @RequestMapping(method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public Group create(Group group) {
        this.logger.info("create(): " + group);
        this.logger.info("Grupo: " + group.toString());
        try {
            return (Group) this.genericService.save(group);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GroupRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    public Group update(@PathVariable("id") Long id, Group group) {
        this.logger.info("update(): " + group);

        try {
            return (Group) this.genericService.update(group);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GroupRestController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "application/json")
    public void delete(@PathVariable("id") Long id) {
        this.logger.info("delete(id)");

        try {
            this.genericService.delete(Group.class, id);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GroupRestController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private boolean isAdmin() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Object principal = authentication.getPrincipal();
        if ((principal instanceof String) && ((String) principal).equals("anonymousUser")) {
            return false;
        }
        UserDetails userDetails = (UserDetails) principal;
        return userDetails.getAuthorities().contains(Role.ADMIN);
    }
}
