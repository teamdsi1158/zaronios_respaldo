/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Paciente;


import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/paciente/pacientesvc")
public class RestPacienteService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllPacientes() {
        List <Paciente>lista = new ArrayList<Paciente>();
        try {
            lista = service.select("SELECT p FROM Paciente p  where p.pacEstado='true' ORDER BY p.expediente.expFechaCreacion DESC");
        } catch (Exception ex) {
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/listDesactivated/", method = RequestMethod.GET, produces = "application/json")
    public List findAllPacientesDesactivated() {
        List <Paciente>lista = new ArrayList<Paciente>();
        try {
            lista = service.select("SELECT p FROM Paciente p  where p.pacEstado='false' ORDER BY p.expediente.expFechaCreacion DESC");
        } catch (Exception ex) {
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpPacientes(@RequestBody Paciente paciente) {
        String msj;
        System.out.print("---- fechaParaPrueba --"+paciente.getPacModificacion());
        try {
             paciente.setPacEstado(Boolean.TRUE);
            this.logger.info("Paciente " + paciente.toString());
            if (paciente.getPacId() == null) {
                service.save(paciente);
                msj = "GUARDADO";
            } else {
                service.update(paciente);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{pacId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delPaciente(@PathVariable(value = "pacId") Integer pacId) {
        String msj;
        try {
            this.logger.info("ID: " + pacId);
            service.delete(Paciente.class, pacId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{pacId}/", method = RequestMethod.GET, produces = "application/json")
    public Paciente getPaciente(@PathVariable(value = "pacId") Integer pacId) {
        Paciente paciente = new Paciente();
        try {
            paciente = (Paciente) service.getById(Paciente.class, pacId);
        } catch (Exception ex) {
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return paciente;
    }

    @RequestMapping(value = "/down/{pacId}/", method = RequestMethod.GET, produces = "text/plain")
    public String bajarPaciente(@PathVariable(value = "pacId") Integer pacId) {
        String msj;
        Paciente paciente = new Paciente();
        try {
            paciente = (Paciente) service.getById(Paciente.class, pacId);
            this.logger.info("ID: " + pacId);
            paciente.setPacEstado(Boolean.FALSE);
            service.update(paciente);
            msj = "DADO DE BAJA";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
       @RequestMapping(value = "/up/{pacId}/", method = RequestMethod.GET, produces = "text/plain")
    public String activarPaciente(@PathVariable(value = "pacId") Integer pacId) {
        String msj;
        Paciente paciente = new Paciente();
        try {
            paciente = (Paciente) service.getById(Paciente.class, pacId);
            this.logger.info("ID: " + pacId);
            paciente.setPacEstado(Boolean.TRUE);
            service.update(paciente);
            msj = "ACTIVADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestPacienteService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
    @RequestMapping(value = "/report/", method = RequestMethod.GET)
   
  public void verPDF() throws Exception{
		InputStream employeeReportStream = getClass().getResourceAsStream("C:\\Users\\matae\\Desktop\\zaronios\\src\\main\\webapp\\ReportePaciente.jrxml");
        JasperReport jasperReport= JasperCompileManager.compileReport(employeeReportStream);
	}

}
