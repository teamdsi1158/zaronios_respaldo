/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.TipoSignoVital;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/tipoSigno/tipoSignosvc")
public class RestTipoSignoService {
    
      @Autowired
    GenericPersistenceService service;
      
 @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllTipoSignoVital() {
        List<TipoSignoVital> lista = new ArrayList<TipoSignoVital>();
        try {
            lista = service.select("SELECT d FROM TipoSignoVital d");
        } catch (Exception ex) {
            Logger.getLogger(RestTipoSignoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/get/{dsivId}/", method = RequestMethod.GET, produces = "application/json")
    public TipoSignoVital getTipoSignoVital(@PathVariable(value = "dsivId") Integer dsivId) {
        TipoSignoVital tipoSignoVital = new TipoSignoVital();
        try {
            tipoSignoVital = (TipoSignoVital) service.getById(TipoSignoVital.class, dsivId);
        } catch (Exception ex) {
            Logger.getLogger(RestTipoSignoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return tipoSignoVital;
    }
    
    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public String addAndUpTipoSignoVital(@RequestBody TipoSignoVital tipo) {
        String msj;
        try {
            if (tipo.getDsivId() == null) {
                service.save(tipo);
                msj = "GUARDADO";
            } else {
                service.update(tipo);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestTipoSignoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
     @RequestMapping(value = "/del/{dsivId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delTipoSignoVital(@PathVariable(value = "dsivId") Integer dsivId) {
        String msj;
        try {
            service.delete(TipoSignoVital.class, dsivId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR posiblemente hay personas registradas con este signo vital";
            Logger.getLogger(RestTipoSignoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

}
