/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import sv.com.zaronios.expediente.Encoders.Encoders;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Usuario;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/usuario/usuariosvc")
public class RestUsuarioService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllUsuario() {
        List<Usuario> lista = new ArrayList<Usuario>();
        try {
            lista = service.select("SELECT r FROM Usuario r");
        } catch (Exception ex) {
            Logger.getLogger(RestUsuarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public Usuario agregarUsuario(@RequestBody Usuario usuario) {
        String msj;
        Encoders encoder=new Encoders();
        String hashPass = encoder.encrypt(usuario.getUsrPassword());
        
        usuario.setUsrPassword(hashPass);
        usuario.setActivo(Boolean.TRUE);
        try {
            if (usuario.getUsrId() == null) {
                service.save(usuario);
            } else {
                service.update(usuario);
            }
        } catch (Exception ex) {
            Logger.getLogger(RestUsuarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return usuario;
    }

    @RequestMapping(value = "/del/{usrId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delUsuario(@PathVariable(value = "usrId") Integer usrId) {
        String msj;
        try {
            msj = "ELIMINADO";
            service.delete(Usuario.class, usrId);
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestUsuarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{usrId}/", method = RequestMethod.GET, produces = "application/json")
    public Usuario getUsuario(@PathVariable(value = "usrId") Integer usrId) {
        Usuario contacto = new Usuario();
        try {
            contacto = (Usuario) service.getById(Usuario.class, usrId);
        } catch (Exception ex) {
            Logger.getLogger(RestUsuarioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return contacto;
    }
   
}