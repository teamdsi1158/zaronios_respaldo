/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Municipio;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/municipio/municipiosvc")
public class RestMunicipioService {

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/{depCodigo}/", method = RequestMethod.GET, produces = "application/json")
    public List findAllMunicipios(@PathVariable(value = "depCodigo") Integer depCodigo) {
        List<Municipio> lista = new ArrayList<Municipio>();
        try {
            lista = service.select("SELECT m FROM Municipio m where m.depCodigo.depCodigo =" + depCodigo);
        } catch (Exception ex) {
            Logger.getLogger(RestMunicipioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/addAndUp/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpMunicipio(@RequestBody Municipio municipio) {
        String msj;
        try {
            if (municipio.getMunCodigo() == null) {
                service.save(municipio);
                msj = "GUARDADO";
            } else {
                service.update(municipio);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestMunicipioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{munCodigo}/", method = RequestMethod.GET, produces = "text/plain")
    public String delMunicipio(@PathVariable(value = "munCodigo") Integer munCodigo) {
        String msj;
        try {
            this.logger.info("ID: " + munCodigo);
            service.delete(Municipio.class, munCodigo);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestMunicipioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{munCodigo}/", method = RequestMethod.GET, produces = "text/plain")
    public Municipio getMunicipio(@PathVariable(value = "munCodigo") Integer munCodigo) {
        Municipio municipio = new Municipio();
        try {
            municipio = (Municipio) service.getById(Municipio.class, munCodigo);
        } catch (Exception ex) {
            Logger.getLogger(RestMunicipioService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return municipio;
    }
}