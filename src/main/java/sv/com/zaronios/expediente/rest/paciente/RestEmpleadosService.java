/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

/**
 *
 * @author marlon
 */

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Empleado;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author marlon
 */
@RestController
@RequestMapping("/rest/empleado/empleadosvc")
public class RestEmpleadosService {

    @Autowired
    GenericPersistenceService service;

    @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllEmpleados() {
        List<Empleado> lista = new ArrayList<Empleado>();
        try {
            lista = service.select("SELECT e FROM Empleado e where e.empEstado=true");
        } catch (Exception ex) {
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
      @RequestMapping(value = "/listDesactivated/", method = RequestMethod.GET, produces = "application/json")
    public List findAllPacientesDesactivated() {
        List <Empleado>lista = new ArrayList<Empleado>();
        try {
            lista = service.select("SELECT p FROM Empleado p  where p.empEstado='false' ORDER BY p.persona.perPrimerNombre DESC");
        } catch (Exception ex) {
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
    @RequestMapping(value = "/listNotUser/", method = RequestMethod.GET, produces = "application/json")
    public List findAllEmpleadosNotUser() {
        List<Empleado> lista = new ArrayList<Empleado>();
        try {
            lista = service.select("SELECT e FROM Empleado e where e.usrId=null");
        } catch (Exception ex) {
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    @RequestMapping(value = "/save/", method = RequestMethod.POST,  consumes = "application/json", produces = "text/plain")
    public String addAndUpEmpleados(@RequestBody Empleado empleado) {
        String msj;
        try {
       
            if (empleado.getEmpId() == null) {
                service.save(empleado);
                msj = "GUARDADO";
            } else {
                service.update(empleado);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/del/{empId}/", method = RequestMethod.DELETE, produces = "text/plain")
    public String delEmpleado(@PathVariable(value = "empId") Integer empId) {
        String msj;
        try {
            service.delete(Empleado.class, empId);
            msj = "ELIMINADO";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }

    @RequestMapping(value = "/get/{empId}/", method = RequestMethod.GET, produces = "application/json")
    public Empleado getEmpleado(@PathVariable(value = "empId") Integer empId) {
        Empleado empleado = new Empleado();
        try {
            empleado = (Empleado) service.getById(Empleado.class, empId);
        } catch (Exception ex) {
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return empleado;
    }

    @RequestMapping(value = "/down/{empId}/", method = RequestMethod.GET, produces = "text/plain")
    public String bajarEmpleado(@PathVariable(value = "empId") Integer empId) {
        String msj;
        try {
            Empleado empleado = (Empleado) service.getById(Empleado.class, empId);
            empleado.setEmpEstado(Boolean.FALSE);
            service.update(empleado);
            msj = "DADO DE BAJA";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
     @RequestMapping(value = "/up/{empId}/", method = RequestMethod.GET, produces = "text/plain")
    public String activarPaciente(@PathVariable(value = "empId") Integer pacId) {
        String msj;
        Empleado empleado = new Empleado();
        try {
            empleado = (Empleado) service.getById(Empleado.class, pacId);
            empleado.setEmpEstado(Boolean.TRUE);
            service.update(empleado);
            msj = "Activado";
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEmpleadosService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
}
