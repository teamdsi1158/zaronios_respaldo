/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.rest.paciente;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import sv.com.zaronios.expediente.entity.Estado;
import sv.com.zaronios.expediente.service.GenericPersistenceService;

/**
 *
 * @author mata
 */
@RestController
@RequestMapping("/rest/estado/estadosvc")
public class RestEstadoService {
     @Autowired
    GenericPersistenceService service;
     
      @RequestMapping(value = "/list/", method = RequestMethod.GET, produces = "application/json")
    public List findAllEstado() {
        List<Estado> lista = new ArrayList<Estado>();
        try {
            lista = service.select("SELECT e FROM Estado e");
        } catch (Exception ex) {
            Logger.getLogger(RestEstadoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
       @RequestMapping(value = "/save/", method = RequestMethod.POST, consumes = "application/json", produces = "text/plain")
    public String addAndUpEstadoCivil(@RequestBody Estado estado) {
        String msj;
        try {
            if (estado.getEstId() == null) {
                service.save(estado);
                msj = "GUARDADO";
            } else {
                service.update(estado);
                msj = "ACTUALIZADO";
            }
        } catch (Exception ex) {
            msj = "ERROR";
            Logger.getLogger(RestEstadoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return msj;
    }
    
     @RequestMapping(value = "/get/{estId}/", method = RequestMethod.GET, produces = "application/json")
    public Estado getEstado(@PathVariable(value = "estId") Integer estId) {
        Estado estado = new Estado();
        try {
            estado = (Estado) service.getById(Estado.class, estId);
        } catch (Exception ex) {
            Logger.getLogger(RestEstadoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return estado;
    }
}
