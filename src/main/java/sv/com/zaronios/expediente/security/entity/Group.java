/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.security.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.security.core.GrantedAuthority;

/**
 *
 * @author Marlon
 */
@javax.persistence.Entity
@Table(name = "GROUPS")
public class Group implements Entity, GrantedAuthority {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true, length = 16, nullable = false)
    private String name;

    @Column(name = "usr", unique = true, length = 16, nullable = false)
    private String user;

    @Override
    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String getAuthority() {
        return this.name;
    }

    @Override
    public String toString() {
        return "Group{" + "id=" + id + ", name=" + name + ", user=" + user + '}';
    }

}
