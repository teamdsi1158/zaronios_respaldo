package sv.com.zaronios.expediente.security.dao.user;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import sv.com.zaronios.expediente.dao.Dao;
import sv.com.zaronios.expediente.security.entity.User;

/**
 * @author Marlon
 */
public interface UserDao extends Dao<User, Long> {

    User loadUserByUsername(String username) throws UsernameNotFoundException;

    User findByName(String name);
}
