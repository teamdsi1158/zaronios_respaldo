package sv.com.zaronios.expediente.security.service;

import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;
import sv.com.zaronios.expediente.security.entity.AccessToken;

/**
 * @author Marlon
 */
public interface TokenService {

    String findUserByAccessToken(String accessToken);

    List findUserGroups(String user);

    AccessToken createAccessToken(UserDetails user);

}
