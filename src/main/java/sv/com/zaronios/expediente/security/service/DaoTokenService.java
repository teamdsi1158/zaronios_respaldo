package sv.com.zaronios.expediente.security.service;

import java.time.Instant;
import java.util.Date;
import java.util.List;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sv.com.zaronios.expediente.security.dao.accesstoken.AccessTokenDao;
import sv.com.zaronios.expediente.security.dao.user.GroupDao;
import sv.com.zaronios.expediente.security.entity.AccessToken;

/**
 * @author Marlon
 */
@Service(value = "tokenService")
public class DaoTokenService implements TokenService {

    @Autowired
    private AccessTokenDao accessTokenDao;
    @Autowired
    private GroupDao groupDao;

    protected DaoTokenService() {

    }

    public DaoTokenService(AccessTokenDao accessTokenDao) {
        this.accessTokenDao = accessTokenDao;
    }

    @Override
    @Transactional
    public String findUserByAccessToken(String accessTokenString) {
        AccessToken accessToken = this.accessTokenDao.findByToken(accessTokenString);
        if (null == accessToken) {
            return null;
        }
        if (accessToken.isExpired()) {
            this.accessTokenDao.delete(accessToken);
            return null;
        }
        return accessToken.getUser();
    }

    @Override
    @Transactional
    public AccessToken createAccessToken(UserDetails user) {
        AccessToken accessToken = new AccessToken(user.getUsername(), UUID.randomUUID().toString(), Date.from(Instant.now()));
        return this.accessTokenDao.save(accessToken);
    }

    @Override
    public List findUserGroups(String user) {
        return this.groupDao.findUserGroups(user);
    }

}
