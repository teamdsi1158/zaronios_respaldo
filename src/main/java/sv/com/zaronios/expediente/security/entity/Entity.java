package sv.com.zaronios.expediente.security.entity;

import java.io.Serializable;

public interface Entity extends Serializable {

    Long getId();

}
