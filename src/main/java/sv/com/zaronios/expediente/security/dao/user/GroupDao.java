package sv.com.zaronios.expediente.security.dao.user;

import java.util.List;
import sv.com.zaronios.expediente.dao.Dao;
import sv.com.zaronios.expediente.security.entity.Group;

/**
 * @author Marlon
 */
public interface GroupDao extends Dao<Group, Long> {

    List findUserGroups(String user);

    List findUserInGroup(Group group);
}
