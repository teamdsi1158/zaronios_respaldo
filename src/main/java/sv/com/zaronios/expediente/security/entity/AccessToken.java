package sv.com.zaronios.expediente.security.entity;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Marlon
 */
@javax.persistence.Entity
@Table(name = "ACCESSTOKEN")
@SequenceGenerator(name = "ACCESSTOKEN_SEQ", sequenceName = "ACCESSTOKEN_SEQ", initialValue = 1, allocationSize = 1)
public class AccessToken implements Entity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ACCESSTOKEN_SEQ")
    private Long id;

    @Column(nullable = false)
    private String token;

    @Column(name = "usr", nullable = false)
    private String user;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiry;

    @Column(name = "fecha_ingreso")
    private Date fechaIngreso;

    protected AccessToken() {

    }

    public AccessToken(String user, String token) {
        this.user = user;
        this.token = token;
    }

    public AccessToken(String user, String token, Date fechaIngreso) {
        this(user, token);
        this.fechaIngreso = fechaIngreso;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    public String getToken() {
        return this.token;
    }

    public String getUser() {
        return this.user;
    }

    public Date getExpiry() {
        return this.expiry;
    }

    public boolean isExpired() {
        if (null == this.expiry) {
            return false;
        }

        return this.expiry.getTime() > System.currentTimeMillis();
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @Override
    public String toString() {
        return "AccessToken{" + "id=" + id + ", token=" + token + ", user=" + user + ", expiry=" + expiry + ", fechaIngreso=" + fechaIngreso + '}';
    }

}
