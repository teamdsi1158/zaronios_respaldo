package sv.com.zaronios.expediente.security.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@javax.persistence.Entity
@Table(name = "User")
public class User implements Entity, UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true, length = 16, nullable = false)
    private String name;
    
    @Column(nullable=false)
    private Integer idRol;

    @Column(length = 80, nullable = false)
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    private Set<Group> roles = new HashSet<Group>();

    protected User() {
        /* Reflection instantiation */
    }

    public User(Long id, String name) {
        this.id = id;
        this.name = name;

    }

    public User(String name, List roles,Integer idRol) {
        this.idRol=idRol;
        this.name = name;
        this.roles.addAll(roles);
    }
    
     public User(String name, List roles) {
        
        this.name = name;
        this.roles.addAll(roles);
    }

    public User(String name, String passwordHash) {
        this.name = name;
        this.password = passwordHash;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Group> getRoles() {
        return this.roles;
    }

    public void setRoles(Set<Group> roles) {
        this.roles = roles;
    }

    public void addRole(Group role) {
        this.roles.add(role);
    }
    
    
    public Integer getIdRol() {
        return idRol;
    }

    public void setIdRol(Integer idRol) {
        this.idRol = idRol;
    }


    @Override
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.getRoles();
    }

    @Override
    public String getUsername() {
        return this.name;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
