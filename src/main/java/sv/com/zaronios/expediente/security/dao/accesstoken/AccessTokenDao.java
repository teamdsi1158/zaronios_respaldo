package sv.com.zaronios.expediente.security.dao.accesstoken;

import sv.com.zaronios.expediente.dao.Dao;
import sv.com.zaronios.expediente.security.entity.AccessToken;

/**
 * @author Marlon
 */
public interface AccessTokenDao extends Dao<AccessToken, Long> {

    AccessToken findByToken(String accessTokenString);

}
