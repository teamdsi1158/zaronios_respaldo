/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sv.com.zaronios.expediente.security.dao.user;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.springframework.transaction.annotation.Transactional;
import sv.com.zaronios.expediente.dao.JpaDao;
import sv.com.zaronios.expediente.security.entity.Group;

/**
 *
 * @author Marlon
 */
public class JpaGroupDao extends JpaDao<Group, Long> implements GroupDao {

    public JpaGroupDao() {
        super(Group.class);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Group> findAll() {
        final CriteriaBuilder builder = this.getEntityManager().getCriteriaBuilder();
        final CriteriaQuery<Group> criteriaQuery = builder.createQuery(Group.class);

        Root<Group> root = criteriaQuery.from(Group.class);
        criteriaQuery.orderBy(builder.desc(root.get("name")));

        TypedQuery<Group> typedQuery = this.getEntityManager().createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public List findUserGroups(String user) {
        if (user == null) {
            return new ArrayList();
        }
        return this.getEntityManager().createQuery("from Group where user=:usr").setParameter("usr", user).getResultList();
    }

    @Override
    public List findUserInGroup(Group group) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
