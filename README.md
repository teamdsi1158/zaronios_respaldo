# README #

Este Proyecto es para respaldo del otro que llego al limite

### ¿que proposito tiene este repositorio? ###

* Tener un control sobre las versiones desarrolladas del sistema informatico de la clinica estetica Zaronios
* Version	3.0.0.

## Estado del sistema informatico ##
* En construcción

### Configuraciones ###

* Desarrollo con Spring 
* Servidor Apache TomCat
* Dependencias:
** Spring Security.
** Angular JS.
* Configuracion de base de datos:
** Postgresql.
** Base de datos Expediente

### Contribuyentes del proyecto ###

* Luis Alejandro Alas González
* Daniel Alejandro Diaz Chica
* Marlon Ernesto Figueroa Fuentes
* Carlos Antonio Mata Meza
* Jorge Otoniel Rodríguez Avalos

